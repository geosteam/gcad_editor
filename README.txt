Установка
bower install
npm install

Запуск
Продакшен: gulp prod
Разработка ( после запуска команды приложение будет доступно по адресу http://localhost:1337/ ) : gulp dev

//==============================================================//
Краткое описание структуры GCAD

Проект состоит из двух глобальных объектов: GCAD и EDITOR
В GCAD хранятся классы объектов:

Фигуры
Circle
LIne
Path
Point
Polyline
Rect
Ruler
Spline
Text

Объекты
Core: что-то типа ядра приложения
Store: Хранилище всех нарисованных объектов
Layer: Слой
SelectedObjectStore: Хранилище всех выделенных объектов

Метод getUniqueId генерирует уникальный идентификатор.

В объекте EDITOR находятся основные методы по работе с объектами и обработчики событий.
Методы:
-------
gcad
objectSnap
handlersStore
helpers
showContextMenu
demoJSON
draw
redraw
Initialization
getUniqueId
enabledContextMenu
disabledContextMenu
setDefaultLayer
addLayer
deleteLayer
renameLayer
getObjectById
redrawObjectsOfLayer
drawDemo
getJsonStore
getStore
getSelectedObject
zoomOn
zoomOff
initDefs
removeAll
redrawAll
load
applyStyleFromGlobal
setDefaultKeyBinds
disabledKeyBinds
removeObject
groupingLayers
selectionWithRectangle
translateObjectsOfBasePoint
resetGcadSelections
setHandlersDefault
linePainter
pathPainter
pointPainter
snap
resetSnap
snapMousemove
selectSnapCircle
selectSnapLine
selectSnapPoint
circlePainter
drawCircle
translateCircle
enabledDrawPolyline
mergePolylines
polylinePainter
translatePolyline
drawRuler
rulerPainter
drawRect
rectPainter
translateRect
enabledDrawSpline
mergeSplines
splinePainter
translateSpline
textPainter
containerSelector
-------

Работа приложения начинается с метода initialization() в котором
также активируются обработчики событий по умолчанию.
//==============================================================//