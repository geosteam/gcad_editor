var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    connect = require('gulp-connect'),
    minifyCss = require('gulp-minify-css'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    notify = require("gulp-notify"),
    sprite = require('gulp.spritesmith'),
    gutil = require('gulp-util'),
    mainBowerFiles = require('main-bower-files'),
    clean = require('gulp-clean');

var pathForJs = [
    // Файлы проекта
    'src/js/global.js',
    'src/js/gcad/**/*.js',
    'src/js/editor/**/*.js',
    'src/js/ui/**/*.js'
];

var pathForLibraryJs = [
    // Библиотеки
    'src/js/include/jquery.js',
    'src/js/include/keymaster.js',
    'src/js/include/d3.js',
    'src/js/include/d3-context-menu.js',
    'src/js/include/angular.js',
    'src/js/include/opentip-jquery.js',
    'src/js/include/remodal.js',
    'src/js/include/undomanager.js'
];

/**
 * Слияние и сжатие js файлов
 */
gulp.task('scripts-for-production', function() {
  return gulp.src(pathForJs)
      .pipe(concat('production.min.js'))
      .pipe(uglify().on('error', gutil.log))
      .pipe(gulp.dest('dist/js'))
      .pipe(notify("Сборка говнокода завершена... "));
});

gulp.task('scripts-for-dev', function() {
    return gulp.src(pathForJs)
        .pipe(plumber()) // plumber
        .pipe(concat('developer.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(connect.reload());
});

gulp.task('scripts-library', function() {
    return gulp.src(pathForLibraryJs)
        .pipe(concat('lib.min.js'))
        //.pipe(uglify().on('error', gutil.log))
        .pipe(gulp.dest('dist/js'))
});

/**
 * Спрайты
 */

gulp.task('sprite', function() {
    var spriteData =
        gulp.src('src/img/icons/*.*') // путь, откуда берем картинки для спрайта
            .pipe(sprite({
                imgName: 'sprite.png',
                cssName: 'sprite.scss',
                cssFormat: 'scss',
                cssOpts: 'functions'
            }));

    spriteData.img.pipe(gulp.dest('dist/img/')); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest('src/sass/')); // путь, куда сохраняем стили
});

/**
 * Очистка папки js на production
 */
gulp.task('clean', function () {
    return gulp.src('dist/js', {read: false})
        .pipe(clean());
});

/**
 * Live-reload
 */

gulp.task('connect', function() {
    connect.server({
        port: 1337,
        root: '',
        livereload: true
    });
});

/**
 * Файлы стилей
 */

gulp.task('css', function() {
    return gulp.src('src/sass/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(plumber()) // plumber
        .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('dist/css/'))
        .pipe(connect.reload());
});


/**
 * Таск для bower
 */
gulp.task('bower', function() {
    return gulp.src(mainBowerFiles())
        .pipe(gulp.dest('src/js/include'));
});


/**
 * Мониторинг изменений
 */
gulp.task('watch', function() {
    gulp.watch([ 'src/sass/*.scss', 'src/css/*.css' ], [ 'css' ]);
    gulp.watch(['src/js/**/*.js'], ['scripts-for-dev']);
    gulp.watch([ 'bower_components' ], [ 'bower' ]);
    gulp.watch(['src/img/icons/*.*'], [ 'sprite' ]);
});

gulp.task('prod', ['clean', 'scripts-library', 'scripts-for-production', 'css', 'bower']);
gulp.task('dev', ['clean', 'scripts-library', 'scripts-for-dev', 'sprite','css', 'bower', 'connect', 'watch']);
