angular.module("GcadApp").controller("LayersCtrl", [ '$scope', 'StoreFactory',
    function($scope, StoreFactory) {
        $scope.StoreFactory = new StoreFactory();
        $scope.showLayerList = "";
        $scope.layersList = $scope.StoreFactory.getLayers();
        $scope.newNameOfLayer = "";
        $scope.defaultLayerId = $scope.StoreFactory.getDefaultLayer().id;
        $scope.selectedLayerName = "";
        $scope.selectedLayerId = null;
        $scope.selectedLayers = [];
        $scope.objectSnap = false;
        $scope.settingsOfLayer = [{
            color: '#ffffff',
            typeOfLines: '',
            opacity: '',
            widthOfLines: ''
        }];

        $scope.typesOfLine = [
            {
                name: 'line',
                style: '0'
            },
            {
                name: 'dotted line',
                style: '5, 10'
            },
            {
                name: 'super dotted line',
                style: '15, 10, 5, 10, 15'
            }
        ];

        $scope.show = function() {
            if ($scope.showLayerList === "") {
                $scope.showLayerList = 'show';
            } else {
                $scope.showLayerList = "";
            }
        };

        $scope.deleteLayer = function(id) {
            if (id === $scope.defaultLayerId) {
                alert('Нельзя вот так вот просто взять, и удалить текущий слой...');
            } else {
                EDITOR.deleteLayer($scope.StoreFactory.store.layers[id]);
            }
        };

        $scope.setDefaultLayer = function(id) {
            $scope.StoreFactory.setDefaultLayer(id);
            $scope.defaultLayerId = $scope.StoreFactory.getDefaultLayer().id;
        };

        $scope.addLayer = function() {
            if ($scope.newNameOfLayer.length === 0) {
                return false;
            }
            var layer = new GCAD.Layer();
            layer.name = $scope.newNameOfLayer;
            $scope.newNameOfLayer = "";
            EDITOR.addLayer(layer);

            $scope.settingsOfLayer.push({
                color: '#ffffff',
                typeOfLines: '',
                opacity: '',
                widthOfLines: ''
            });
        };

        $scope.changeLayer = function(id) {
            var layer = $scope.StoreFactory.store.layers[id];
            if (layer.visible) {
                layer.show(); // layer.visible уже был изменен, поэтому используем метод show()
            } else {
                layer.hide();
            }
        };

        $scope.selectLayer = function(layerId, $event) {
            var layerListElement = angular.element('.layer-list ul li');

            $scope.selectedLayer = $scope.StoreFactory.getLayer(layerId);
            $scope.selectedLayerName = $scope.selectedLayer.name;
            $scope.selectedLayerId = $scope.selectedLayer.id;

            if (key.ctrl) {
                $scope.selectedLayers.push($scope.selectedLayerId);
                angular.element($event.currentTarget).addClass('selected');
            } else {
                angular.forEach(layerListElement, function(value){
                    var data = angular.element(value);
                    data.removeClass('selected');
                });
                $scope.selectedLayers = [];
                $scope.selectedLayers.push($scope.selectedLayerId);
                angular.element($event.currentTarget).addClass('selected');
            }
        };

        $scope.renameLayer = function(id, name) {
            $scope.StoreFactory.setNameLayer(id, name);
        };

        $scope.changeColorOfLayer = function(layerId, index) {
            var layer = $scope.StoreFactory.getLayer(layerId);
            layer.color = $scope.settingsOfLayer[index].color;
            EDITOR.redrawObjectsOfLayer(layerId);
        };

        $scope.changeOpacityOfLayer = function(layerId, index) {
            var layer = $scope.StoreFactory.getLayer(layerId);
            layer.opacity = $scope.settingsOfLayer[index].opacity;
            EDITOR.redrawObjectsOfLayer(layerId);
        };

        $scope.changeWidthOfLayer = function(layerId, index) {
            var layer = $scope.StoreFactory.getLayer(layerId);
            layer.widthOfLines = $scope.settingsOfLayer[index].widthOfLines;
            EDITOR.redrawObjectsOfLayer(layerId);
        };

        $scope.changeStrokeDasharray = function(layerId, index) {
            var layer = $scope.StoreFactory.getLayer(layerId);
            $scope.typesOfLine.forEach(function(item) {
                if ($scope.settingsOfLayer[index].typeOfLines === item.name) {
                    layer.strokeDasharrayOfLines = item.style;
                }
            });
            EDITOR.redrawObjectsOfLayer(layerId);
        };

        $scope.groupingLayers = function() {
            var thereIsDefaultLayer = false;

            if ($scope.selectedLayers.length === 0) {
                return false;
            }

            $scope.selectedLayers.forEach(function(idLayer) {
                var layer = $scope.StoreFactory.getLayer(idLayer);

                if (layer.id === $scope.defaultLayerId) {
                    thereIsDefaultLayer = true;
                }
            });

            if (thereIsDefaultLayer) {
                return false;
            }

            EDITOR.runDialogForGrouping($scope.selectedLayers, function() {
                $scope.$apply();
            });
        };

        $scope.onOpen = function() {
            //$scope.$apply();
            $scope.layersList = $scope.StoreFactory.getLayers();
        };
    }
]);
