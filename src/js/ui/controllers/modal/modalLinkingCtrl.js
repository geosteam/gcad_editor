angular.module("GcadApp").controller("ModalCtrl", [ '$scope', 'StoreFactory',
    function($scope, StoreFactory) {
        $scope.StoreFactory = new StoreFactory();
        $scope.snapPoint = false;
        $scope.snapLine = false;

        $scope.changeSnapPoint = function() {
            if ($scope.snapPoint) {
                EDITOR.snap.enabledSnapPoint = true;
                $scope.snapPoint = true;
            } else {
                EDITOR.snap.enabledSnapPoint = false;
                $scope.snapPoint = false;
            }

            EDITOR.setHandlersDefault();
            EDITOR.zoomOn(); // Иначе не будет отключаться zoom
        };

        $scope.changeSnapLine = function() {
            if ($scope.snapLine) {
                EDITOR.snap.enabledSnapLine = true;
                $scope.snapLine = true;
            } else {
                EDITOR.snap.enabledSnapLine = false;
                $scope.snapLine = false;
            }

            EDITOR.setHandlersDefault();
            EDITOR.zoomOn(); // Иначе не будет отключаться zoom
        };
    }
]);
