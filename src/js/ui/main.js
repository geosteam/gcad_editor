(function() {
    /**
     * Файл с которого все начинается...
     */

    angular.module("GcadApp", []);

    var drawPolylineButton = $('#draw-pol'),
        drawSplineButton = $('#draw-spline'),
        mergePolylineButton = $('#merge-pol'),
        mergeSplineButton = $('#merge-spline'),
        translateOfBasePointButton = $('#translate-of-base-point'),
        drawRulerButton = $('#draw-ruler'),
        drawParallelButton = $('#draw-parallel-ruler'),
        drawRectangleButton = $('#draw-rectangle'),
        drawCircleButton = $('#draw-circle');

    var tooltipsOptionsForLeftButtons = {
        delay: 1,
        tipJoint: 'right middle',
        targetJoint: 'right middle',
        target: true,
        offset: [10, 0]
    };

    EDITOR.Initialization('#svg');
    //EDITOR.drawDemo();

    /**
     * Tooltips
     */

    drawPolylineButton.opentip('Полилиния', tooltipsOptionsForLeftButtons);
    drawSplineButton.opentip('Сплайн', tooltipsOptionsForLeftButtons);
    mergePolylineButton.opentip('Объединить полилинии', tooltipsOptionsForLeftButtons);
    mergeSplineButton.opentip('Объединить сплайны', tooltipsOptionsForLeftButtons);
    translateOfBasePointButton.opentip('Переместить относительно базовой точки', tooltipsOptionsForLeftButtons);
    drawRulerButton.opentip('Линейка', tooltipsOptionsForLeftButtons);
    drawParallelButton.opentip('Параллельная линейка', tooltipsOptionsForLeftButtons);
    drawRectangleButton.opentip('Прямоугольник', tooltipsOptionsForLeftButtons);
    drawCircleButton.opentip('Окружность', tooltipsOptionsForLeftButtons);

    /**
     * Button handlers
     */

    drawPolylineButton.on('click', function() {
        var self = $(this);

        self.toggleClass("active");
        EDITOR.enabledDrawPolyline(function() {
            self.toggleClass("active");
        });
    });

    drawSplineButton.on('click', function() {
        var self = $(this);

        self.toggleClass("active");
        EDITOR.enabledDrawSpline(function() {
            self.toggleClass("active");
        });
    });

    mergePolylineButton.on('click', function() {
        var self = $(this);

        self.toggleClass("active");
        EDITOR.mergePolylines(function() {
            self.toggleClass("active");
        });
    });

    mergeSplineButton.on('click', function() {
        var self = $(this);

        self.toggleClass("active");
        EDITOR.mergeSplines(function() {
            self.toggleClass("active");
        });
    });

    translateOfBasePointButton.on('click', function() {
        var self = $(this);

        self.toggleClass("active");
        EDITOR.translateObjectsOfBasePoint(function() {
            self.toggleClass("active");
        });
    });

    drawRulerButton.on('click', function() {
        var self = $(this);

        self.toggleClass("active");
        EDITOR.drawRuler({parallel: false}, function() {
            self.toggleClass("active");
        });
    });

    drawParallelButton.on('click', function() {
        var self = $(this);

        self.toggleClass("active");
        EDITOR.drawRuler({parallel: true}, function() {
            self.toggleClass("active");
        });
    });

    drawRectangleButton.on('click', function() {
        var self = $(this);

        self.toggleClass("active");
        EDITOR.drawRect(function() {
            self.toggleClass("active");
        });
    });

    drawCircleButton.on('click', function() {
        var self = $(this);

        self.toggleClass("active");
        EDITOR.drawCircle(function() {
            self.toggleClass("active");
        });
    });

    $("#getStore").on('click', function() {
        console.log(EDITOR.getStore());
        //console.log(EDITOR.getJsonStore());
    });

    $('#getSelected').on('click', function() {
        console.log(EDITOR.getSelectedObject());
    });

    $("#demo").on('click', function() {
        EDITOR.load(EDITOR.demoJSON);
    });

    $('#get-snap-object').on('click', function() {
        console.log(EDITOR.snap);
    });

    $('#getSvgDom').on('click', function() {
        console.log($('svg g#general')[0]);
    });

    $('#layer-list').on('click', function() {
        var $modal = $('[data-remodal-id=layer-manager]');
        var inst = $modal.remodal({});
        var $but = $modal.find('.put');

        inst.open();

        $but.on('click', function() {
            var $modal = $('[data-remodal-id=layer-manager2]');
            var inst = $modal.remodal({});

            inst.open();
        });
    });

    $('#object-snap-button').on('click', function() {
        EDITOR.runDialogSnapSettings();
    });

    $('#header-undo').on('click', function() {
        EDITOR.gcad.UndoManager.undo();
    });

    $('#header-redo').on('click', function() {
        EDITOR.gcad.UndoManager.redo();
    });
})();
