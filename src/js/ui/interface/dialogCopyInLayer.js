/**
 * Копировать объект в слой
 * @param idObject Идентификатор объекта
 */
EDITOR.runDialogCopyInLayer = function(idObject) {
    var $modal = $('[data-remodal-id=copyObjectInLayer]');
    var inst = $modal.remodal({});
    var $selectLayer = $modal.find('select.layers');
    var layers = EDITOR.gcad.store.layers;
    var applyButton = $modal.find('#apply-copyInLayer');

    inst.open();

    $selectLayer.find('option').remove();

    for (var key in EDITOR.gcad.store.layers) {
        var $option = $('<option></option>');

        $option.text(EDITOR.gcad.store.layers[key].name);
        $option.attr('data-layer-id', key);
        $selectLayer.append($option);
    }

    $modal.on('closing', function () {
        $modal.off('closing');
        applyButton.off('click');
    });

    applyButton.on('click', function() {
        var object = EDITOR.getObjectById(idObject);
        var selectedLayerId = $selectLayer.find(':selected').attr('data-layer-id');

        EDITOR.resetGcadSelections();

        if (object.type === 'circle') {
            var newCircle = EDITOR.helpers.cloneCircle(object, selectedLayerId);
            EDITOR.draw(newCircle);
        }
        if (object.type === 'polyline') {
            var newPolyline = EDITOR.helpers.clonePolyline(object, selectedLayerId);
            EDITOR.draw(newPolyline);
        }
        if (object.type === 'rect') {
            var newRect = EDITOR.helpers.cloneRect(object, selectedLayerId);
            EDITOR.draw(newRect);
        }
        if (object.type === 'ruler') {
            var newRuler = EDITOR.helpers.cloneRuler(object, selectedLayerId);
            EDITOR.draw(newRuler);
        }

        inst.close();
    });
};
