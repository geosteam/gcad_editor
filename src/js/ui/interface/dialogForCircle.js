EDITOR.runDialogForCircle = function(circle) {
    var $modal = $('[data-remodal-id=preferenceForCircle]');
    var inst = $modal.remodal({});
    var $inputForPointX = $modal.find('input[name=pointX]');
    var $inputForPointY = $modal.find('input[name=pointY]');
    var $inputColor = $modal.find('input[type=color]');
    var $widthLine = $modal.find('input[name=width]');
    var $selectLayer = $modal.find('.select-layers');
    var layers = EDITOR.gcad.store.layers;


    inst.open();

    validateInput($inputForPointX);
    validateInput($inputForPointY);

    for (var key in EDITOR.gcad.store.layers) {
        var $option = $('<option></option>');

        $option.text(EDITOR.gcad.store.layers[key].name);
        $option.attr('data-layer-id', key);
        $selectLayer.append($option);
    }

    $inputForPointX.val(circle.centerX);
    $inputForPointY.val(circle.centerY);
    $inputColor.val(GCAD.Style.circle.color);
    $widthLine.val(circle.strokeWidth);
    $selectLayer.val(layers[circle.layerID].name);

    $modal.on('closing', function () {
        $modal.off('closing');

        $inputForPointX.off('input');
        $inputForPointY.off('input');
        $inputColor.off('input');
        $widthLine.off('input');
        $selectLayer.off('change');

        $selectLayer.find('option').remove();
    });

    $inputForPointX.on('input', function() {
        changeInputCoordinate();
    });

    $inputForPointY.on('input', function() {
        changeInputCoordinate();
    });

    $inputColor.on('input', function() {
        circle.newColor = $(this).val();
        EDITOR.redraw(circle);
    });

    $selectLayer.on('change', function() {
        var id = $(this).find('option:selected').attr('data-layer-id');
        EDITOR.removeObject(circle);
        circle.layerID = id;
        EDITOR.draw(circle);
        EDITOR.gcad.selectedObjectsStore.add(circle);
    });

    $widthLine.on('input', function() {
        circle.strokeWidth = $(this).val();
        EDITOR.redraw(circle);
        circle.newStrokeWidth = $(this).val();
    });

    function changeInputCoordinate() {
        var x = +$modal.find('input[name=pointX]').val();
        var y = +$modal.find('input[name=pointY]').val();

        if (x.length === 0 || y.length === 0) {
            return false;
        } else {
            setNewCoordinate(circle, x, y);
        }
        EDITOR.redraw(circle);
    }

    function setNewCoordinate(circle, newX, newY) {
        circle.centerX = newX;
        circle.centerY = newY;

        circle.points.center.cx = newX;
        circle.points.center.cy = newY;

        circle.points.right.cx = circle.radius + circle.centerX;
        circle.points.right.cy = circle.centerY;

        circle.points.upper.cx = circle.centerX;
        circle.points.upper.cy = circle.radius + circle.centerY;

        circle.points.left.cx = -circle.radius + circle.centerX;
        circle.points.left.cy = circle.centerY;

        circle.points.lower.cx = circle.centerX;
        circle.points.lower.cy = -circle.radius + circle.centerY;
    }

    function validateInput(input) {
        input.on('input', function(){
            this.value = this.value.replace(/^\.|[^\d\.]|\.(?=.*\.)|^0+(?=\d)/g, '');
        });
    }
};
