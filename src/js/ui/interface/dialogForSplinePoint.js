EDITOR.runDialogForSplinePoint  = function(spline, point) {
    var $modal = $('[data-remodal-id=preferenceForSplinePoint]');
    var inst = $modal.remodal({});
    var $inputX = $modal.find('input[name=pointX]');
    var $inputY = $modal.find('input[name=pointY]');

    function validateInput(dom) {
        dom.on('input', function(){
            this.value = this.value.replace(/^\.|[^\d\.]|\.(?=.*\.)|^0+(?=\d)/g, '');
        });
    }

    $modal.find('input[name=pointX]').val(point.pointSpline.cx);
    $modal.find('input[name=pointY]').val(point.pointSpline.cy);

    inst.open();

    $modal.on('closing', function () {
        $modal.off('closing');
        $inputX.off('input');
        $inputY.off('input');
    });

    validateInput($inputX);
    validateInput($inputY);

    $inputX.on('input', function() {
        redrawPointOfSpline(+$inputX.val(), +$inputY.val());
    });

    $inputY.on('input', function() {
        redrawPointOfSpline(+$inputX.val(), +$inputY.val());
    });

    function redrawPointOfSpline(x, y) {
        if (x.length === 0 || y.length === 0) {
            return false;
        } else {
            setNewCoordinate(spline, point, x, y);
        }

        EDITOR.redraw(spline);
    }

    function setNewCoordinate(spline, point, newX, newY) {
        point.pointSpline.cx = newX;
        point.pointSpline.cy = newY;
        point.pointPath.x = newX;
        point.pointPath.y = newY;
    }
};

