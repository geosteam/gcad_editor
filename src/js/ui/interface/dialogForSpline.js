EDITOR.runDialogForSpline  = function(spline) {
    var $modal = $('[data-remodal-id=preferenceForSpline]');
    var inst = $modal.remodal({});
    var $selectLayer = $modal.find('.select-layers');
    var layers = EDITOR.gcad.store.layers;

    inst.open();

    for (var key in EDITOR.gcad.store.layers) {
        var $option = $('<option></option>');

        $option.text(EDITOR.gcad.store.layers[key].name);
        $option.attr('data-layer-id', key);
        $selectLayer.append($option);
    }

    $selectLayer.val(layers[spline.layerID].name);

    $modal.on('closing', function () {
        $modal.off('closing');

        $selectLayer.off('change');

        $selectLayer.find('option').remove();
    });

    $selectLayer.on('change', function() {
        var id = $(this).find('option:selected').attr('data-layer-id');
        EDITOR.removeObject(spline);
        spline.layerID = id;
        EDITOR.draw(spline);
        EDITOR.gcad.selectedObjectsStore.add(spline);
    });
};

