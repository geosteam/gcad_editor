EDITOR.runDialogForRect = function(rect) {
    var $modal = $('[data-remodal-id=preferenceForRect]');
    var inst = $modal.remodal({});
    var $inputForPointX = $modal.find('input[name=pointX]');
    var $inputForPointY = $modal.find('input[name=pointY]');
    var $selectAngle = $modal.find('#selectAngle');
    var $inputColor = $modal.find('input[type=color]');
    var pointPainter = EDITOR.pointPainter;
    var selectedPoint = rect.points.upperLeft;
    var $widthLine = $modal.find('input[name=width]');
    var $selectLayer = $modal.find('.select-layers');
    var layers = EDITOR.gcad.store.layers;

    inst.open();

    rect.points.upperLeft.color = GCAD.Style.point.selectedColor;
    pointPainter.redraw(rect.points.upperLeft);

    for (var key in EDITOR.gcad.store.layers) {
        var $option = $('<option></option>');

        $option.text(EDITOR.gcad.store.layers[key].name);
        $option.attr('data-layer-id', key);
        $selectLayer.append($option);
    }

    $inputForPointX.val(rect.points.upperLeft.cx);
    $inputForPointY.val(rect.points.upperLeft.cy);
    $inputColor.val(GCAD.Style.rect.color);
    $widthLine.val(rect.strokeWidth);
    $selectAngle.val('1');
    $selectLayer.val(layers[rect.layerID].name);

    validateInput($inputForPointX);
    validateInput($inputForPointY);

    $inputForPointX.on('input', function() {
        changeInputCoordinate();
    });

    $inputForPointY.on('input', function() {
        changeInputCoordinate();
    });

    $inputColor.on('input', function() {
        rect.newColor = $(this).val();
        EDITOR.redraw(rect);
    });

    $widthLine.on('input', function() {
        rect.strokeWidth = $(this).val();
        EDITOR.redraw(rect);
    });

    $selectLayer.on('change', function() {
        var id = $(this).find('option:selected').attr('data-layer-id');
        EDITOR.removeObject(rect);
        rect.layerID = id;
        EDITOR.draw(rect);
        EDITOR.gcad.selectedObjectsStore.add(rect);
        selectedPoint = rect.points.upperLeft;
    });

    $selectAngle.on('change', function() {
        var optionSelectedValue = $(this).find('option:selected').val();

        resetSelectPointRect();

        if (optionSelectedValue == 1) {
            rect.points.upperLeft.color = GCAD.Style.point.selectedColor;
            pointPainter.redraw(rect.points.upperLeft);
            selectedPoint = rect.points.upperLeft;

            $inputForPointX.val(rect.points.upperLeft.cx);
            $inputForPointY.val(rect.points.upperLeft.cy);
        }
        if (optionSelectedValue == 2) {
            rect.points.upperRight.color = GCAD.Style.point.selectedColor;
            pointPainter.redraw(rect.points.upperRight);
            selectedPoint = rect.points.upperRight;

            $inputForPointX.val(rect.points.upperRight.cx);
            $inputForPointY.val(rect.points.upperRight.cy);
        }
        if (optionSelectedValue == 3) {
            rect.points.lowerLeft.color = GCAD.Style.point.selectedColor;
            pointPainter.redraw(rect.points.lowerLeft);
            selectedPoint = rect.points.lowerLeft;

            $inputForPointX.val(rect.points.lowerLeft.cx);
            $inputForPointY.val(rect.points.lowerLeft.cy);
        }
        if (optionSelectedValue == 4) {
            rect.points.lowerRight.color = GCAD.Style.point.selectedColor;
            pointPainter.redraw(rect.points.lowerRight);
            selectedPoint = rect.points.lowerRight;

            $inputForPointX.val(rect.points.lowerRight.cx);
            $inputForPointY.val(rect.points.lowerRight.cy);
        }

        function resetSelectPointRect() {
            rect.points.upperLeft.color = GCAD.Style.point.color;
            pointPainter.redraw(rect.points.upperLeft);

            rect.points.upperRight.color = GCAD.Style.point.color;
            pointPainter.redraw(rect.points.upperRight);

            rect.points.lowerLeft.color = GCAD.Style.point.color;
            pointPainter.redraw(rect.points.lowerLeft);

            rect.points.lowerRight.color = GCAD.Style.point.color;
            pointPainter.redraw(rect.points.lowerRight);
        }

    });

    $modal.on('closing', function () {
        $modal.off('closing');

        $inputForPointX.off('input');
        $inputForPointY.off('input');
        $selectAngle.off('change');
        $selectLayer.off('change');
        $inputColor.off('input');
        $widthLine.off('input');

        $selectLayer.find('option').remove();
        selectedPoint.color = GCAD.Style.point.color;
        pointPainter.redraw(selectedPoint);
    });

    function changeInputCoordinate() {

        console.log('wff');
        var x = +$modal.find('input[name=pointX]').val();
        var y = +$modal.find('input[name=pointY]').val();

        if (x.length === 0 || y.length === 0) {
            return false;
        } else {
            setNewCoordinate(rect, selectedPoint, x, y);
        }
        EDITOR.redraw(rect);
    }

    function validateInput(input) {
        input.on('input', function(){
            this.value = this.value.replace(/^\.|[^\d\.]|\.(?=.*\.)|^0+(?=\d)/g, '');
        });
    }

    function setNewCoordinate(rect, point, newX, newY) {
        point.cx = newX;
        point.cy = newY;

        if (point.positionOnRect === 'upperLeft') {
            rect.coordOfAngle.upperLeft.x = newX;
            rect.coordOfAngle.upperLeft.y = newY;
            rect.points.upperMiddle.cx = (rect.points.upperRight.cx + point.cx) / 2;
            rect.points.upperMiddle.cy = (rect.points.upperRight.cy + point.cy) / 2;
            rect.points.left.cx = (rect.points.lowerLeft.cx + point.cx) / 2;
            rect.points.left.cy = (rect.points.lowerLeft.cy + point.cy) / 2;
        }
        if (point.positionOnRect === 'upperRight') {
            rect.coordOfAngle.upperRight.x = newX;
            rect.coordOfAngle.upperRight.y = newY;
            rect.points.upperMiddle.cx = (rect.points.upperLeft.cx + point.cx) / 2;
            rect.points.upperMiddle.cy = (rect.points.upperLeft.cy + point.cy) / 2;
            rect.points.right.cx = (rect.points.lowerRight.cx + point.cx) / 2;
            rect.points.right.cy = (rect.points.lowerRight.cy + point.cy) / 2;
        }
        if (point.positionOnRect === 'lowerLeft') {
            rect.coordOfAngle.lowerLeft.x = newX;
            rect.coordOfAngle.lowerLeft.y = newY;
            rect.points.lowerMiddle.cx = (rect.points.lowerRight.cx + point.cx) / 2;
            rect.points.lowerMiddle.cy = (rect.points.lowerRight.cy + point.cy) / 2;
            rect.points.left.cx = (rect.points.upperLeft.cx + point.cx) / 2;
            rect.points.left.cy = (rect.points.upperLeft.cy + point.cy) / 2;
        }
        if (point.positionOnRect === 'lowerRight') {
            rect.coordOfAngle.lowerRight.x = newX;
            rect.coordOfAngle.lowerRight.y = newY;
            rect.points.lowerMiddle.cx = (rect.points.lowerLeft.cx + point.cx) / 2;
            rect.points.lowerMiddle.cy = (rect.points.lowerLeft.cy + point.cy) / 2;
            rect.points.right.cx = (rect.points.upperRight.cx + point.cx) / 2;
            rect.points.right.cy = (rect.points.upperRight.cy + point.cy) / 2;
        }
    }
};
