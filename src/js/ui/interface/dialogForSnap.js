EDITOR.runDialogSnapSettings = function() {
    var $modal = $('[data-remodal-id=snapSettings]');
    var inst = $modal.remodal({});
    var $selectLayer = $modal.find('select.layers');
    var applyButton = $modal.find('#apply-snapSettings');

    inst.open();

    $selectLayer.find('option').remove();

    $modal.on('closing', function () {
        $modal.off('closing');
        applyButton.off('click');
    });

    applyButton.on('click', function() {
        EDITOR.setHandlersDefault();
        EDITOR.zoomOn(); // Иначе не будет отключаться zoom

        inst.close();
    });
};
