EDITOR.runDialogForGrouping = function(selectedLayers, callback) {
    var $modal = $('[data-remodal-id=chooseLayerForGrouping]');
    var inst = $modal.remodal({});
    var $selectLayer = $modal.find('.layers');
    var layers = EDITOR.gcad.store.layers;
    var newLayersList = [];
    var applyButton = $modal.find('#apply-grouping');

    inst.open();

    for (var keyLayer in layers) {
        var checkSelected = false;

        selectedLayers.forEach(function(selectedLayerId) {
            if (layers[keyLayer].id === selectedLayerId) {
                checkSelected = true;
             }
        });

        if (!checkSelected) {
            newLayersList.push(layers[keyLayer]);
        }
    }

    $selectLayer.find('option').remove();

    newLayersList.forEach(function(item) {
        var $option = $('<option></option>');

        $option.text(item.name);
        $option.attr('data-layer-id', item.id);
        $selectLayer.append($option);
    });

    $modal.on('closing', function () {
        $modal.off('closing');
        applyButton.off('click');
    });

    applyButton.on('click', function() {
        inst.close();
        EDITOR.groupingLayers(selectedLayers, $selectLayer.find('option:selected').attr('data-layer-id'));
        callback();
    });
};
