EDITOR.runDialogForPolyline  = function(polyline) {
    var $modal = $('[data-remodal-id=preferenceForPolyline]');
    var inst = $modal.remodal({});
    var $selectLayer = $modal.find('.select-layers');
    var layers = EDITOR.gcad.store.layers;

    inst.open();

    for (var key in EDITOR.gcad.store.layers) {
        var $option = $('<option></option>');

        $option.text(EDITOR.gcad.store.layers[key].name);
        $option.attr('data-layer-id', key);
        $selectLayer.append($option);
    }

    $selectLayer.val(layers[polyline.layerID].name);

    $modal.on('closing', function () {
        $modal.off('closing');

        $selectLayer.off('change');

        $selectLayer.find('option').remove();
    });

    $selectLayer.on('change', function() {
        var id = $(this).find('option:selected').attr('data-layer-id');
        EDITOR.removeObject(polyline);
        polyline.layerID = id;
        EDITOR.draw(polyline);
        EDITOR.gcad.selectedObjectsStore.add(polyline);
    });
};
