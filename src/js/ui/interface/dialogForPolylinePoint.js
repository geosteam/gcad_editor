EDITOR.runDialogForPolylinePoint  = function(polyline, point) {
    var $modal = $('[data-remodal-id=preferenceForPolylinePoint]');
    var inst = $modal.remodal({});
    var $inputX = $modal.find('input[name=pointX]');
    var $inputY = $modal.find('input[name=pointY]');

    function validateInput(dom) {
        dom.on('input', function(){
            this.value = this.value.replace(/^\.|[^\d\.]|\.(?=.*\.)|^0+(?=\d)/g, '');
        });
    }

    $modal.find('input[name=pointX]').val(point.cx);
    $modal.find('input[name=pointY]').val(point.cy);

    inst.open();

    $modal.on('closing', function () {
        $modal.off('closing');
        $inputX.off('input');
        $inputY.off('input');
    });

    validateInput($inputX);
    validateInput($inputY);

    $inputX.on('input', function() {
        redrawPointOfPolyline(+$inputX.val(), +$inputY.val());
    });

    $inputY.on('input', function() {
        redrawPointOfPolyline(+$inputX.val(), +$inputY.val());
    });

    function redrawPointOfPolyline(x, y) {
        if (x.length === 0 || y.length === 0) {
            return false;
        } else {
            setNewCoordinate(polyline, point, x, y);
        }

        EDITOR.redraw(polyline);
    }

    function setNewCoordinate(polyline, point, newX, newY) {
        point.cx = newX;
        point.cy = newY;

        if (point.id < 1) {
            if (polyline.isClose) {
                polyline.edges[point.id].coordinates.begin.x = newX;
                polyline.edges[point.id].coordinates.begin.y = newY;
                polyline.edges[polyline.edges.length - 1].coordinates.close.x = newX;
                polyline.edges[polyline.edges.length - 1].coordinates.close.y = newY;
            } else {
                polyline.edges[point.id].coordinates.begin.x = newX;
                polyline.edges[point.id].coordinates.begin.y = newY;
            }
        } else if (point.id === polyline.edges.length) {
            if (polyline.isClose) {
                polyline.edges[point.id + 1].coordinates.close.x = newX;
                polyline.edges[point.id + 1].coordinates.close.y = newY;
                polyline.edges[point.id].coordinates.begin.x = newX;
                polyline.edges[point.id].coordinates.begin.y = newY;
            } else {
                polyline.edges[point.id - 1].coordinates.close.x = newX;
                polyline.edges[point.id - 1].coordinates.close.y = newY;
            }
        } else {
            polyline.edges[point.id - 1].coordinates.close.x = newX;
            polyline.edges[point.id - 1].coordinates.close.y = newY;
            polyline.edges[point.id].coordinates.begin.x = newX;
            polyline.edges[point.id].coordinates.begin.y = newY;
        }
    }
};

