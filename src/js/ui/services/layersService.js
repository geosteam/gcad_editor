angular.module("GcadApp").factory('StoreFactory', function() {
    function StoreFactory() {
        this.store = EDITOR.gcad.store;
    }

    StoreFactory.prototype.getLayers = function() {
        return this.store.layers;
    };


    StoreFactory.prototype.getDefaultLayer = function() {
        return this.store.defaultLayer;
    };

    StoreFactory.prototype.setDefaultLayer = function(id) {
        var layer = this.store.layers[id];
        this.store.defaultLayer = layer;
        EDITOR.gcad.defaultLayer = layer;
    };

    StoreFactory.prototype.setNameLayer =  function(id, name) {
        EDITOR.renameLayer(id, name);
    };

    StoreFactory.prototype.getLayer =  function(id) {
        return EDITOR.gcad.store.layers[id];
    };

    return StoreFactory;
});
