/**
 * Перемещение объектов относительно базовой точки
 * @param callback
 */
EDITOR.translateObjectsOfBasePoint = function (callback) {
    var selectedPolylines = [];
    var selectedRectangles = [];
    var selectedCircles = [];
    var selectedSplines = [];

    var pseudoLines = {
        polylines: [],
        rectangles: [],
        circles: [],
        splines: []
    };

    var isMove = false;
    var self = this;
    var gcad = self.gcad;
    var zoomValueX = null;
    var zoomValueY = null;
    var scaleValue = null;
    var basePointX = 0;
    var basePointY = 0;
    var baseLine = new GCAD.Line();
    var temporaryArray = [];
    var pointBaseLine = 0;

    EDITOR.zoomOff();

    baseLine.strokeDasharray = 5;
    baseLine.strokeWidth = 2;
    baseLine.color = '#fff';

    /*if (isEmpty(gcad.selectedObjectsStore.polylines)) {
     return false;
     }*/

    for (var keyPolyline in gcad.selectedObjectsStore.polylines) {
        selectedPolylines.push(gcad.selectedObjectsStore.polylines[keyPolyline]);
    }

    for (var keySpline in gcad.selectedObjectsStore.splines) {
        selectedSplines.push(gcad.selectedObjectsStore.splines[keySpline]);
    }

    for (var keyRect in gcad.selectedObjectsStore.rectangles) {
        selectedRectangles.push(gcad.selectedObjectsStore.rectangles[keyRect]);
    }

    for (var keyCircle in gcad.selectedObjectsStore.circles) {
        selectedCircles.push(gcad.selectedObjectsStore.circles[keyCircle]);
    }

    if (selectedPolylines === [] &&
        selectedRectangles === [] &&
        selectedCircles === [] &&
        selectedSplines === []) {
        return false;
    }

    gcad.element.on('mousedown', function () {
        zoomValueX = gcad.getTranslateValueX();
        zoomValueY = gcad.getTranslateValueY();
        scaleValue = gcad.getScaleValue();

        if (isMove) {
            closeTranslate();
            return false;
        }

        selectedPolylines.forEach(function (item) {
            item.isMove = true;
        });

        var mouseDownX = d3.mouse(gcad.element.node())[0] / scaleValue - zoomValueX / scaleValue;
        var mouseDownY = d3.mouse(gcad.element.node())[1] / scaleValue - zoomValueY / scaleValue;

        baseLine.coordinates = {
            begin: {
                x: mouseDownX,
                y: mouseDownY
            },
            close: {
                x: mouseDownX,
                y: mouseDownY
            }
        };

        pointBaseLine = new GCAD.Point(mouseDownX, mouseDownY);
        pointBaseLine.radius = 4;
        pointBaseLine.color = '#fff';

        self.draw(pointBaseLine);
        self.draw(baseLine);

        gcad.element.on('mousemove', function () {
            isMove = true;

            var offsetX = 0;
            var offsetY = 0;

            zoomValueX = gcad.getTranslateValueX();
            zoomValueY = gcad.getTranslateValueY();
            scaleValue = gcad.getScaleValue();

            var mouseMoveX = d3.mouse(gcad.element.node())[0] / scaleValue - zoomValueX / scaleValue;
            var mouseMoveY = d3.mouse(gcad.element.node())[1] / scaleValue - zoomValueY / scaleValue;

            for (var key in pseudoLines) {
                pseudoLines[key].forEach(function(item) {
                    self.removeObject(item);
                });
            }

            pseudoLines = {
                polylines: [],
                rectangles: [],
                circles: [],
                splines: []
            };

            baseLine.coordinates.close = {
                x: mouseMoveX,
                y: mouseMoveY
            };

            self.redraw(baseLine);

            offsetX = (basePointX !== 0 && basePointY !== 0) ? mouseMoveX - basePointX : 0;
            offsetY = (basePointX !== 0 && basePointY !== 0) ? mouseMoveY - basePointY : 0;

            /**
             * Полилинии
             */
            selectedPolylines.forEach(function(item) {
                item.points.forEach(function(item) {
                    temporaryArray.push({x: item.cx, y: item.cy});
                });

                var pseudoPolyline = new GCAD.Path(temporaryArray);

                pseudoPolyline.layerID = item.layerID;
                self.applyStyleFromGlobal(pseudoPolyline);
                pseudoPolyline.strokeDasharray = 5;
                pseudoPolyline.setInterpolate('linear');

                pseudoLines.polylines.push(pseudoPolyline);
                temporaryArray = [];

                self.translatePolyline(item, offsetX, offsetY);
            });

            /**
             * Сплайны
             */

            selectedSplines.forEach(function(item) {

                for (var key in item.points) {
                    temporaryArray.push({x: item.points[key].pointPath.x, y: item.points[key].pointPath.y})
                }

                var pseudoSpline = new GCAD.Path(temporaryArray);

                pseudoSpline.layerID = item.layerID;
                self.applyStyleFromGlobal(pseudoSpline);
                pseudoSpline.strokeDasharray = 5;

                pseudoLines.splines.push(pseudoSpline);
                temporaryArray = [];

                self.translateSpline(item, offsetX, offsetY);

            });

            /**
             * Прямоугольники
             */
            selectedRectangles.forEach(function(item) {
                for (key in item.coordOfAngle) {
                    temporaryArray.push({
                        x: item.coordOfAngle[key].x,
                        y: item.coordOfAngle[key].y
                    });
                }
                temporaryArray.push({
                    x: item.coordOfAngle.upperLeft.x,
                    y: item.coordOfAngle.upperLeft.y
                });

                var pseudoRect = new GCAD.Path(temporaryArray);

                pseudoRect.layerID = gcad.defaultLayer.id;
                self.applyStyleFromGlobal(pseudoRect);
                pseudoRect.strokeDasharray = 5;

                pseudoLines.rectangles.push(pseudoRect);
                temporaryArray = [];

                self.translateRect(item, offsetX, offsetY);
            });

            /**
             * Окружности
             */
            selectedCircles.forEach(function(item) {
                var arrayPointsOfCircle = {};
                var pseudoCircle = new GCAD.Circle();

                for (var key in item.points) {
                    arrayPointsOfCircle[key] = item.points[key];
                }

                pseudoCircle.radius = item.radius;
                pseudoCircle.centerX = item.centerX;
                pseudoCircle.centerY = item.centerY;
                pseudoCircle.strokeDasharray = 5;
                pseudoCircle.color = GCAD.Style.path.color;
                pseudoCircle.strokeWidth = 1;
                pseudoCircle.layerID = gcad.defaultLayer.id;
                pseudoCircle.fill = 'none';

                pseudoLines.circles.push(pseudoCircle);

                self.translateCircle(item, offsetX, offsetY);
            });

            for (key in pseudoLines) {
                pseudoLines[key].forEach(function(item) {
                    self.draw(item);
                });
            }

            basePointX = mouseMoveX;
            basePointY = mouseMoveY;
        });

        function closeTranslate() {
            gcad.element.on('mousemove', null);

            for (var key in pseudoLines) {
                pseudoLines[key].forEach(function(item) {
                    self.removeObject(item);
                });
            }

            self.removeObject(pointBaseLine);
            self.removeObject(baseLine);

            selectedPolylines.forEach(function(item) {
                self.redraw(item);
                //Навешиваем событие наведения мыши на элемент
                EDITOR.handlersStore.polyline.setMouseMoveEvent(item);
            });

            selectedSplines.forEach(function(item) {
                self.redraw(item);
                EDITOR.handlersStore.spline.setMouseMoveEvent(item);
            });

            selectedRectangles.forEach(function(item) {
                self.redraw(item);
                EDITOR.handlersStore.rect.setMouseMoveEvent(item);
            });

            selectedCircles.forEach(function(item) {
                self.redraw(item);
                EDITOR.handlersStore.circle.setMouseMoveEvent(item);
            });

            isMove = false;
            self.setHandlersDefault();
            self.zoomOn();

            callback();
        }
    });
};

