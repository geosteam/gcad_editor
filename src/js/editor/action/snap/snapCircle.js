/**
 * Привязка к окружности
 * @param mouseX Положение курсора по x
 * @param mouseY Положение курсора по y
 * @param circle Проверяемая Окружность
 */
EDITOR.selectSnapCircle = function(mouseX, mouseY, circle) {
    var gcad = EDITOR.gcad;
    var snap = EDITOR.snap;
    var snapCircle = snap.snapCircle;
    var snapLine = snap.snapLine;
    var coordinate;

    var distanceToMouse = EDITOR.helpers.getDistanceBetweenTheTwoPoints(circle.centerX, circle.centerY, mouseX, mouseY);
    var distance = distanceToMouse - circle.radius;

    if (distance < 5 && distance > -5) {
        coordinate = getCoordinate(circle.centerX, circle.centerY, mouseX, mouseY, distanceToMouse, circle.radius);
    } else {
        if (snapCircle.selectionPoint) {
            EDITOR.removeObject(snapCircle.selectionPoint);
            snapCircle.selectionPoint = null;
        }

        return false;
    }

    var newSelectionPoint = new GCAD.Point();
    EDITOR.snap.statusSnap = true;

    if (snap.snapPoint.point) {
        if (snapCircle.selectionPoint) {
            EDITOR.removeObject(snapCircle.selectionPoint);
        }
        if (snapLine.selectionPoint) {
            EDITOR.removeObject(snapLine.selectionPoint);
        }
        return false;
    }
    
    snap.statusSnapCircle = true;

    newSelectionPoint.radius = 8;
    newSelectionPoint.id = 'none';
    newSelectionPoint.owner = 'none';
    newSelectionPoint.ownerID = 'none';
    newSelectionPoint.color = '#1BA3E4';
    newSelectionPoint.opacity = '0.2';
    newSelectionPoint.strokeWidth = 2;
    newSelectionPoint.stroke = '#1BA3E4';
    newSelectionPoint.layerID = gcad.generalLayer.attr('id');

    if (snapCircle.selectionPoint) {
        EDITOR.removeObject(snapCircle.selectionPoint);
        snapCircle.selectionPoint = newSelectionPoint;

        if (snapCircle.distance > distance) {
            snapCircle.circle = circle;
            snapCircle.distance = distance;
            snapCircle.coordinate = coordinate;
            setCircleSnapPoint(coordinate[0], coordinate[1]);
        } else {
            var circleOld = snapCircle.circle;

            var coordinateOld = getCoordinate(circleOld.centerX, circleOld.centerY,
                mouseX, mouseY,
                distanceToMouse, circleOld.radius);


            snapCircle.distance = EDITOR.helpers.getDistanceBetweenTheTwoPoints(coordinateOld[0], coordinateOld[1], mouseX, mouseY);
            snapCircle.coordinate = coordinateOld;

            setCircleSnapPoint(coordinateOld[0], coordinateOld[1]);
        }
    } else {
        snapCircle.selectionPoint = newSelectionPoint;
        snapCircle.circle = circle;
        snapCircle.coordinate = coordinate;
        snapCircle.distance = distance;
        setCircleSnapPoint(coordinate[0], coordinate[1]);
    }

    EDITOR.draw(snapCircle.selectionPoint);

    /**
     * Задает для точки привязки новые координаты
     * @param newX
     * @param newY
     */
    function setCircleSnapPoint(newX, newY) {
        snapCircle.coordinate[0] = newX;
        snapCircle.coordinate[1] = newY;
        snapCircle.selectionPoint.cx = newX;
        snapCircle.selectionPoint.cy = newY;
    }

    /**
     * Получить координату точки лежащей на окружности
     * @param centerX Координата x центра окружности
     * @param centerY Координата y центра окружности
     * @param xm Координата x мыши
     * @param ym Координата y мыши
     * @param d Диаметр окружности
     * @param r Радиус окружности
     * @returns {[]}  Координаты в виде массива [x, y]
     */
    function getCoordinate(centerX, centerY, xm, ym, d, r) {
        var vectorAC = [xm - centerX, ym - centerY];
        var k = d/r;
        var vectorAB = [vectorAC[0]/k, vectorAC[1]/k];

        return [vectorAB[0] + centerX, vectorAB[1] + centerY];
    }
};
