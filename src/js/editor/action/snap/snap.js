
EDITOR.snap = {
    snapPoint: {
        selectionPoint: null,
        point: null
    },
    snapLine: {
        selectionPoint: null,
        line: null,
        distance: null,
        coordinate: null
    },
    snapCircle: {
        circle: null,
        selectionPoint: null,
        coordinate: null,
        distance: null
    },
    enabledSnapPoint: false,
    enabledSnapLine: false,

    excludePoints: [], // Массив объектов вида [ {id: ... , ownerId: ...}, ... ]
    excludeLines: [], // Массив объектов вида  [ {id: ... , ownerId: ...}, ... ]
    excludeCirclesId: [],
    excludeRectsId: [],

    statusSnapPoint: false,
    statusSnapLine: false,
    statusSnapCircle: false
};

EDITOR.resetSnap = function () {
    var snap = EDITOR.snap;

    if (snap.snapPoint.selectionPoint) {
        EDITOR.removeObject(snap.snapPoint.selectionPoint);
        snap.snapPoint.selectionPoint = null;
    }

    if (snap.snapLine.selectionPoint) {
        EDITOR.removeObject(snap.snapLine.selectionPoint);
        snap.snapLine.selectionPoint = null;
    }

    if (snap.snapCircle.selectionPoint) {
        EDITOR.removeObject(snap.snapCircle.selectionPoint);
        snap.snapCircle.selectionPoint = null;
    }

    snap.snapPoint.point = null;
    snap.snapLine.line = null;
    snap.snapCircle.circle = null;

    snap.excludeLines = [];
    snap.excludePoints = [];
    snap.excludeCirclesId = [];
    snap.excludeRectsId = [];

};

/**
 * Режим привязки
 */
EDITOR.snapMousemove = function () {
    var gcad = EDITOR.gcad;
    var snap = EDITOR.snap;

    snap.snapPoint.point = null;
    snap.statusSnapPoint = false;
    snap.statusSnapLine = false;
    snap.statusSnapCircle = false;

    var translateValueX = gcad.getTranslateValueX();
    var translateValueY = gcad.getTranslateValueY();
    var scaleValue = gcad.getScaleValue();
    var mouseX = d3.mouse(gcad.element.node())[0] / scaleValue - translateValueX / scaleValue;
    var mouseY = d3.mouse(gcad.element.node())[1] / scaleValue - translateValueY / scaleValue;

    // === Перебираем все полилинии
    for (var polylineId in gcad.store.polylines) {
        var polyline = gcad.store.polylines[polylineId];

        if (snap.enabledSnapPoint) {
            // Перебираем все точки в текущей полилинии
            polyline.points.forEach(function (item) {
                var checkExcludePoints = false;

                snap.excludePoints.forEach(function(excludeItem) {
                    if (checkExclude(excludeItem.id, item.id, excludeItem.ownerId, item.ownerID)) {
                        checkExcludePoints = true;
                    }
                });
                if (!checkExcludePoints) {
                    EDITOR.selectSnapPoint(mouseX, mouseY, item);
                }
            });
        }

        if (snap.enabledSnapLine) {
            // Перебираем все сигменты в текущей полилинии

            polyline.edges.forEach(function (item) {
                var checkExcludeLines = false;

                snap.excludeLines.forEach(function(excludeItem) {
                    if (checkExclude(excludeItem.id, item.id, excludeItem.ownerId, item.ownerID)) {
                        checkExcludeLines = true;
                    }
                });
                if (!checkExcludeLines) {
                    EDITOR.selectSnapLine(mouseX, mouseY, item);
                }
            });
        }
    }

    // Перебираем все прямоугольники
    for (var rectId in gcad.store.rectangles) {
        var checkExcludeRect = false;

        snap.excludeRectsId.forEach(function(id) {
            if (checkExclude(rectId, id)) {
                checkExcludeRect = true;
            }
        });

        if (checkExcludeRect) {
            continue;
        }

        var rect = gcad.store.rectangles[rectId];

        if (snap.enabledSnapPoint) {
            for (var keyRect in rect.points) {
                EDITOR.selectSnapPoint(mouseX, mouseY, rect.points[keyRect]);
            }
        }

        if (snap.enabledSnapLine) {
            var pseudoLineUpper = new GCAD.Line();
            var pseudoLineLeft = new GCAD.Line();
            var pseudoLineLower = new GCAD.Line();
            var pseudoLineRight = new GCAD.Line();
            var pseudoLines = [];

            var coordOfAngle = rect.coordOfAngle;

            pseudoLineUpper.coordinates.begin.x = coordOfAngle.upperLeft.x;
            pseudoLineUpper.coordinates.begin.y = coordOfAngle.upperLeft.y;
            pseudoLineUpper.coordinates.close.x = coordOfAngle.upperRight.x;
            pseudoLineUpper.coordinates.close.y = coordOfAngle.upperRight.y;

            pseudoLineLeft.coordinates.begin.x = coordOfAngle.upperRight.x;
            pseudoLineLeft.coordinates.begin.y = coordOfAngle.upperRight.y;
            pseudoLineLeft.coordinates.close.x = coordOfAngle.lowerRight.x;
            pseudoLineLeft.coordinates.close.y = coordOfAngle.lowerRight.y;

            pseudoLineLower.coordinates.begin.x = coordOfAngle.lowerRight.x;
            pseudoLineLower.coordinates.begin.y = coordOfAngle.lowerRight.y;
            pseudoLineLower.coordinates.close.x = coordOfAngle.lowerLeft.x;
            pseudoLineLower.coordinates.close.y = coordOfAngle.lowerLeft.y;

            pseudoLineRight.coordinates.begin.x = coordOfAngle.upperLeft.x;
            pseudoLineRight.coordinates.begin.y = coordOfAngle.upperLeft.y;
            pseudoLineRight.coordinates.close.x = coordOfAngle.lowerLeft.x;
            pseudoLineRight.coordinates.close.y = coordOfAngle.lowerLeft.y;

            pseudoLines.push(pseudoLineUpper, pseudoLineLeft, pseudoLineLower, pseudoLineRight);

            pseudoLines.forEach(function (item) {
                EDITOR.selectSnapLine(mouseX, mouseY, item);
            });
        }
    }

    // Перебираем все окружности
    for (var circleId in gcad.store.circles) {
        var checkExcludeCircle = false;

        snap.excludeCirclesId.forEach(function(id) {
            if (checkExclude(circleId, id)) {
                checkExcludeCircle = true;
            }
        });

        if (checkExcludeCircle) {
            continue;
        }

        var circle = gcad.store.circles[circleId];

        if (snap.enabledSnapPoint) {
            for (var keyPoint in circle.points) {
                var checkExcludePoints = false;

                snap.excludePoints.forEach(function(excludeItem) {
                    if (checkExclude(excludeItem.id, circle.points[keyPoint].id, excludeItem.ownerId, circle.points[keyPoint].ownerID)) {
                        checkExcludePoints = true;
                    }
                });
                if (!checkExcludePoints) {
                    EDITOR.selectSnapPoint(mouseX, mouseY, circle.points[keyPoint]);
                }
            }
        }

        if (snap.enabledSnapLine) {
            EDITOR.selectSnapCircle(mouseX, mouseY, circle);
        }
    }

    if (!snap.statusSnapPoint) {
        if (snap.snapPoint.selectionPoint) {
            EDITOR.removeObject(snap.snapPoint.selectionPoint);
            snap.snapPoint.selectionPoint = null;
        }
    }

    if (!snap.statusSnapLine) {
        if (snap.snapLine.selectionPoint) {
            EDITOR.removeObject(snap.snapLine.selectionPoint);
            snap.snapLine.selectionPoint = null;
        }
    }

    if (!snap.statusSnapCircle) {
        if (snap.snapCircle.selectionPoint) {
            EDITOR.removeObject(snap.snapCircle.selectionPoint);
            snap.snapCircle.selectionPoint = null;
        }
    }

    if (snap.snapLine.selectionPoint && snap.snapCircle.selectionPoint) {
        if (snap.snapLine.distance < snap.snapCircle.distance) {
            EDITOR.removeObject(snap.snapCircle.selectionPoint);
            snap.statusSnapCircle = false;
        } else {
            EDITOR.removeObject(snap.snapLine.selectionPoint);
            snap.statusSnapLine = false;
        }
    }

    /**
     * Проверка. Находится ли объект в исключениях привязки
     * @param idExclude Идентификатор исключаемого объекта
     * @param id Идентификатор проверяемого объекта
     * @param ownerExclude Владелец исключаемого объекта
     * @param owner Владелец проверяемого объекта
     * @returns {boolean}
     */
    function checkExclude(idExclude, id, ownerExclude, owner) {
        if (ownerExclude && owner) {
            return (idExclude === id && ownerExclude === owner);
        } else {
            return (idExclude === id);
        }
    }
};
