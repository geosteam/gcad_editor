/**
 * Привязка к точке
 * @param mouseX Положение курсора по x
 * @param mouseY Положение курсора по y
 * @param point Проверяемая точка
 */
EDITOR.selectSnapPoint = function(mouseX, mouseY,point) {
    var snap = EDITOR.snap;
    var snapPoint = snap.snapPoint;
    var gcad = EDITOR.gcad;

    if (mouseX >= point.cx && mouseX <= (point.cx + 12) && mouseY >= point.cy && mouseY <= (point.cy + 12) ||
        mouseX <= point.cx && mouseX >= (point.cx - 12) && mouseY >= point.cy && mouseY <= (point.cy + 12) ||
        mouseX <= point.cx && mouseX >= (point.cx - 12) && mouseY <= point.cy && mouseY >= (point.cy - 12) ||
        mouseX >= point.cx && mouseX <= (point.cx + 12) && mouseY <= point.cy && mouseY >= (point.cy - 12)) {

        snap.statusSnapPoint = true;

        if (snapPoint.selectionPoint) {
            EDITOR.removeObject(snapPoint.selectionPoint);
        }

        var newSelectionPoint = new GCAD.Point();

        newSelectionPoint.radius = 8;
        newSelectionPoint.id = 'none';
        newSelectionPoint.owner = 'none';
        newSelectionPoint.ownerID = 'none';
        newSelectionPoint.color = '#1BA3E4';
        newSelectionPoint.opacity = '0.2';
        newSelectionPoint.strokeWidth = 2;
        newSelectionPoint.stroke = '#1BA3E4';
        newSelectionPoint.layerID = gcad.generalLayer.attr('id');

        snapPoint.selectionPoint = newSelectionPoint;

        // Если точка к которой выполнили привязку уже существует значит рядом с курсором находится еще
        // одна точка. Поэтому проверяем и привязываемся к той точке которая ближе всего к курсору.
        if (snapPoint.point) {
            if (snapPoint.point.id === point.id && snapPoint.point.ownerID === point.ownerID) {
                redrawSelectionRect(point.cx, point.cy);
            }
            if (snapPoint.point.id !== point.id || snapPoint.point.ownerID !== point.ownerID) {
                var distanceOfLinkPoint = EDITOR.helpers.getDistanceBetweenTheTwoPoints(
                    snapPoint.point.cx, snapPoint.point.cy, mouseX, mouseY
                );
                var distanceOfItem = EDITOR.helpers.getDistanceBetweenTheTwoPoints(
                    point.cx, point.cy, mouseX, mouseY
                );

                if (distanceOfItem < distanceOfLinkPoint) {
                    snapPoint.point = point;
                    redrawSelectionRect(point.cx, point.cy);
                } else {
                    redrawSelectionRect(snapPoint.point.cx, snapPoint.point.cy);
                }
                // Может возникнуть ситуация что id точек одинаковый, в этом случае проверяем
                // относятся ли точки к разным объектам
            } else if (snapPoint.point.id === point.id && snapPoint.point.ownerID !== point.ownerID) {
                snapPoint.point = point;
                redrawSelectionRect(point.cx, point.cy);
            }
            // Иначе привязываемся к текущей точке
        } else {
            snapPoint.point = point;
            redrawSelectionRect(point.cx, point.cy);
        }

        EDITOR.draw(snapPoint.selectionPoint);
    }

    // Координаты для прямоугольника точки к которой мы привязались
    function redrawSelectionRect(newX, newY) {
        snapPoint.selectionPoint.cx = newX;
        snapPoint.selectionPoint.cy = newY;
    }
};
