/**
 * Привязка к прямой
 * @param mouseX Положение курсора по x
 * @param mouseY Положение курсора по y
 * @param line Проверяемая прямая
 */
EDITOR.selectSnapLine = function(mouseX, mouseY, line) {
    var snap = EDITOR.snap;
    var snapLine = snap.snapLine;
    var snapCircle = snap.snapCircle;
    var gcad = EDITOR.gcad;
    var coordinateLine = line.coordinates;
    var getDistanceHelper = EDITOR.helpers.getDistanceBetweenTheTwoPoints;
    var getCoordinateToEdge = EDITOR.helpers.getCoordinateToEdge;
    var coordinate = getCoordinateToEdge(coordinateLine.begin.x, coordinateLine.begin.y, coordinateLine.close.x,
        coordinateLine.close.y, mouseX, mouseY);
    var distance = getDistanceHelper(coordinate[0], coordinate[1], mouseX, mouseY);
    
    if (distance < 5) {

        //Незнаю почему это условие работает, но оно работает
        if (getDistanceHelper(coordinate[0], coordinate[1], coordinateLine.begin.x, coordinateLine.begin.y) < 3 ||
            getDistanceHelper(coordinate[0], coordinate[1], coordinateLine.close.x, coordinateLine.close.y) < 3 ||
            coordinateLine.close.x > coordinateLine.begin.x && coordinate[0] > coordinateLine.close.x ||
            coordinateLine.begin.x > coordinateLine.close.x && coordinate[0] > coordinateLine.begin.x ||
            coordinateLine.close.x > coordinateLine.begin.x && coordinate[0] < coordinateLine.begin.x ||
            coordinateLine.begin.x > coordinateLine.close.x && coordinate[0] < coordinateLine.close.x) {

            resetSnap();
            return false;
        }

        if (coordinateLine.close.x === coordinateLine.begin.x) {
            if (coordinateLine.begin.y < coordinateLine.close.y) {
                if (coordinate[1] > coordinateLine.close.y || coordinate[1] < coordinateLine.begin.y) {
                    resetSnap();
                    return false;
                }
            }
            if (coordinateLine.begin.y > coordinateLine.close.y) {
                if (coordinate[1] < coordinateLine.close.y || coordinate[1] > coordinateLine.begin.y) {
                    resetSnap();
                    return false;
                }
            }
        }

        if (coordinateLine.close.y === coordinateLine.begin.y) {
            if (coordinateLine.begin.x < coordinateLine.close.x) {
                if (coordinate[0] > coordinateLine.close.x || coordinate[0] < coordinateLine.begin.x) {
                    resetSnap();
                    return false;
                }
            }
            if (coordinateLine.begin.x > coordinateLine.close.x) {
                if (coordinate[0] < coordinateLine.close.x || coordinate[1] > coordinateLine.begin.x) {
                    resetSnap();
                    return false;
                }
            }
        }

        var newSelectionPoint = new GCAD.Point();

        if (snap.statusSnapPoint) {
            if (snapCircle.selectionPoint) {
                EDITOR.removeObject(snapCircle.selectionPoint);
            }
            if (snapLine.selectionPoint) {
                EDITOR.removeObject(snapLine.selectionPoint);
            }
            snap.statusSnapCircle = false;
            snap.statusSnapLine = false;
            return false;
        }

        snap.statusSnapLine = true;

        newSelectionPoint.radius = 8;
        newSelectionPoint.id = 'none';
        newSelectionPoint.owner = 'none';
        newSelectionPoint.ownerID = 'none';
        newSelectionPoint.color = '#1BA3E4';
        newSelectionPoint.opacity = '0.2';
        newSelectionPoint.strokeWidth = 2;
        newSelectionPoint.stroke = '#1BA3E4';
        newSelectionPoint.layerID = gcad.generalLayer.attr('id');


        if (snapLine.selectionPoint) {
            EDITOR.removeObject(snapLine.selectionPoint);
            snapLine.selectionPoint = newSelectionPoint;

            if (snap.snapLine.distance > distance) {
                snapLine.line = line;
                snap.snapLine.distance = distance;
                snap.snapLine.coordinate = coordinate;
                setSnapLinePoint(coordinate[0], coordinate[1]);
            } else {
                var coordinateOldLine = snapLine.line.coordinates;
                var coordinateOld = getCoordinateToEdge(coordinateOldLine.begin.x, coordinateOldLine.begin.y,
                    coordinateOldLine.close.x, coordinateOldLine.close.y,
                    mouseX, mouseY);
                snap.snapLine.distance = getDistanceHelper(coordinateOld[0], coordinateOld[1], mouseX, mouseY);
                snap.snapLine.coordinate = coordinateOld;

                setSnapLinePoint(coordinateOld[0], coordinateOld[1]);
            }
        } else {
            snapLine.selectionPoint = newSelectionPoint;
            snapLine.line = line;
            snapLine.coordinate = coordinate;
            snapLine.distance = distance;
            setSnapLinePoint(coordinate[0], coordinate[1]);
        }

        EDITOR.draw(snapLine.selectionPoint);
    }

    /**
     * Задает для точки привязки новые координаты
     * @param newX
     * @param newY
     */
    function setSnapLinePoint(newX, newY) {
        snap.snapLine.coordinate[0] = newX;
        snap.snapLine.coordinate[1] = newY;
        snap.snapLine.selectionPoint.cx = newX;
        snap.snapLine.selectionPoint.cy = newY;
    }

    function resetSnap() {
        if (snapLine.selectionPoint) {
            EDITOR.removeObject(snapLine.selectionPoint);
        }
        snapLine.coordinate = null;
        snapLine.distance = null;
        snapLine.selectionPoint = null;
        snap.statusSnapLine = false;
    }
};
