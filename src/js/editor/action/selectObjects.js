/**
 * Выделение прямоугольником.
 * @param mouseDownX
 * @param mouseDownY
 */
EDITOR.selectionWithRectangle = function(mouseDownX, mouseDownY) {
    var self = this;
    var handlers = this.handlersStore;
    var gcad = self.gcad;
    var rectSelection = new GCAD.Rect();
    var mouseMoveX = 0;
    var mouseMoveY = 0;
    var selectedObjects = {
        polylines: {},
        rectangles: {},
        circles: {},
        rulers: {},
        splines: {}
    };

    key('esc', function() {
        if (rectSelection.element) {
            EDITOR.removeObject(rectSelection);
        }

        self.gcad.element.on('mousemove', null);
        self.gcad.element.on('mousedown', null);
        self.setHandlersDefault();
        self.zoomOn();
        self.disabledKeyBinds();
        self.setDefaultKeyBinds();
        EDITOR.gcad.element.style('cursor', 'default');
    });

    EDITOR.gcad.element.style('cursor', 'cell');

    self.zoomOff();

    rectSelection.id = EDITOR.getUniqueId();
    rectSelection.fill = '#193662';
    rectSelection.layerID = gcad.defaultLayer.id;

    self.gcad.element.on('mousemove', function() {
        // Отключаем наведение мыши при рисовании...
        gcad.element.on('mouseover', null);
        gcad.element.on('mouseout', null);

        mouseMoveX = d3.mouse(self.gcad.element.node())[0] / self.gcad.getScaleValue() -
            self.gcad.getTranslateValueX() / self.gcad.getScaleValue();

        mouseMoveY = d3.mouse(self.gcad.element.node())[1] / self.gcad.getScaleValue() -
            self.gcad.getTranslateValueY() / self.gcad.getScaleValue();

        rectSelection.coordOfAngle.upperLeft = {
            x: mouseDownX,
            y: mouseDownY
        };

        rectSelection.coordOfAngle.upperRight = {
            x: mouseMoveX,
            y: mouseDownY
        };
        rectSelection.coordOfAngle.lowerLeft = {
            x: mouseDownX,
            y: mouseMoveY
        };
        rectSelection.coordOfAngle.lowerRight = {
            x: mouseMoveX,
            y: mouseMoveY
        };

        if (rectSelection.element) {
            self.removeObject(rectSelection);
            self.draw(rectSelection);
        } else {
            self.draw(rectSelection);
        }
        rectSelection.element.attr('fill-opacity', '0.6');

        self.resetGcadSelections();

        /**
         * Попадает ли какой-нибуть объект под выделение
         * Тупой перебор всех точек (fps скорее всего просядет)
         */
        enumeratingPoints();

        function enumeratingPoints() {
            var store = gcad.store;
            var fitAllThePoints = true;
            //TODO: Оптимизировать выделение надо блять...

            //Проверка полилиний
            for (var polylineId in store.polylines) {
                var polyline = store.polylines[polylineId];

                /*for (var selectedPolylineKey in selectedObjects.polylines) {

                }*/
                fitAllThePoints = true;

                polyline.points.forEach(function(item) {
                    if (!checkPointInRectangle(item.cx, item.cy)) {
                        fitAllThePoints = false;
                    }
                });

                if (fitAllThePoints) {
                    handlers.polyline.setSelect(polyline);
                    //selectedObjects.polylines[polylineId] = polyline;
                }
            }

            //Проверка сплайнов
            for (var splineId in store.splines) {
                var spline = store.splines[splineId];

                fitAllThePoints = true;

                for (var splinePointKey in spline.points) {
                    var splinePoint = spline.points[splinePointKey].pointSpline;

                    if (!checkPointInRectangle(splinePoint.cx, splinePoint.cy)) {
                        fitAllThePoints = false;
                    }
                }

                if (fitAllThePoints) {
                    handlers.spline.setSelect(spline);
                }
            }

            //Проверка прямоугольников
            for (var rectId in store.rectangles) {
                fitAllThePoints = true;

                for (var key in store.rectangles[rectId].coordOfAngle) {
                    var rectPoint = store.rectangles[rectId].coordOfAngle[key];

                    if (!checkPointInRectangle(rectPoint.x, rectPoint.y)) {
                        fitAllThePoints = false;
                    }
                }

                if (fitAllThePoints) {
                    handlers.rect.setSelect(store.rectangles[rectId]);
                    gcad.selectedObjectsStore.add(store.rectangles[rectId]);
                }
            }

            //Проверка окружностей
            for (var circleId in store.circles) {
                fitAllThePoints = true;

                for (var key in store.circles[circleId].points) {
                    var circlePoint = store.circles[circleId].points[key];

                    if (!checkPointInRectangle(circlePoint.cx, circlePoint.cy)) {
                        fitAllThePoints = false;
                    }
                }

                if (fitAllThePoints) {
                    handlers.circle.setSelect(store.circles[circleId]);
                    gcad.selectedObjectsStore.add(store.circles[circleId]);
                }
            }

            //Проверка линеек
            for (var rulerId in store.rulers) {
                fitAllThePoints = true;

                for (var key in store.rulers[rulerId].extensionLines) {
                    var rulerLineCoordinates = store.rulers[rulerId].extensionLines[key].coordinates;

                    if (!checkPointInRectangle(rulerLineCoordinates.begin.x, rulerLineCoordinates.begin.y) ||
                        !checkPointInRectangle(rulerLineCoordinates.close.x, rulerLineCoordinates.close.y)) {
                        fitAllThePoints = false;
                    }

                    if (fitAllThePoints) {
                        handlers.ruler.setSelect(store.rulers[rulerId]);
                        gcad.selectedObjectsStore.add(store.rulers[rulerId]);
                    }
                }
            }
        }

        /**
         * Проверка. Находится ли точка с координатами x y внутри прямоугольника.
         * @param x
         * @param y
         * @returns {boolean}
         */
        function checkPointInRectangle(x, y) {
            var AM = getVector(
                {
                    x: rectSelection.coordOfAngle.upperLeft.x,
                    y: rectSelection.coordOfAngle.upperLeft.y
                },
                {
                    x: x,
                    y: y
                }
            );

            var AB = getVector(
                {
                    x: rectSelection.coordOfAngle.upperLeft.x,
                    y: rectSelection.coordOfAngle.upperLeft.y
                },
                {
                    x: rectSelection.coordOfAngle.upperRight.x,
                    y: rectSelection.coordOfAngle.upperRight.y
                }
            );

            var AD = getVector(
                {
                    x: rectSelection.coordOfAngle.upperLeft.x,
                    y: rectSelection.coordOfAngle.upperLeft.y
                },
                {
                    x: rectSelection.coordOfAngle.lowerLeft.x,
                    y: rectSelection.coordOfAngle.lowerLeft.y
                }
            );

            var AM_AB = getDotProduct({
                x: AM[0],
                y: AM[1],
                x2: AB[0],
                y2: AB[1]
            });
            var AB_AB = getDotProduct({
                x: AB[0],
                y: AB[1],
                x2: AB[0],
                y2: AB[1]
            });
            var AM_AD = getDotProduct({
                x: AM[0],
                y: AM[1],
                x2: AD[0],
                y2: AD[1]
            });
            var AD_AD = getDotProduct({
                x: AD[0],
                y: AD[1],
                x2: AD[0],
                y2: AD[1]
            });

            return ((AM_AB < AB_AB) && (AM_AD < AD_AD) && AM_AB > 0 && AM_AD > 0);
        }

        function getVector(point1, point2) {
            return [
                point2.x - point1.x,
                point2.y - point1.y
            ]
        }

        function getDotProduct(obj) {
            return obj.x * obj.x2 + obj.y * obj.y2;
        }
    });

    gcad.element.on('mousedown', function() {
        gcad.element.on('mousemove', null);
        gcad.element.on('mousedown', null);
        self.setHandlersDefault();
        if (rectSelection.element) {
            EDITOR.removeObject(rectSelection);
        }
        self.zoomOn();
        EDITOR.gcad.element.style('cursor', 'default');
    });
};
