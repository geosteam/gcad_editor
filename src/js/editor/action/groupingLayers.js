/**
 * Группировка слоев
 * @param idsLayers Массив с идентификаторами слоев
 * @param layerId Идентификатор слоя с которым нужно объединить другие слои
 */
EDITOR.groupingLayers = function(idsLayers, layerId) {
    var store = EDITOR.gcad.store;
    var objects = [];
    //var layer = EDITOR.getObjectById(layerId);

    idsLayers.forEach(function(layerId) {
        var layer = store.layers[layerId];

        for (var keyObj in layer.objects) {
            for (var key in layer.objects[keyObj]) {
                objects.push(layer.objects[keyObj][key]);
            }
        }

        EDITOR.removeObject(layer);
    });

    objects.forEach(function(item) {
        item.layerID = layerId;
        EDITOR.applyStyleFromGlobal(item);
        EDITOR.draw(item);
    });
};
