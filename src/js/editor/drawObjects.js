EDITOR.draw = function(obj) {
    var layer = this.gcad.defaultLayer;

    if (obj.type === 'polyline') {
        this.gcad.store.add(obj);
        this.polylinePainter.draw(obj, obj.layerID);
    }
    if (obj.type === 'spline') {
        this.gcad.store.add(obj);
        this.splinePainter.draw(obj, obj.layerID);
    }
    if (obj.type === 'path') {
        this.gcad.store.add(obj);
        this.pathPainter.draw(obj, layer.id);
    }
    if (obj.type === 'line') {
        this.gcad.store.add(obj);
        this.linePainter.draw(obj, layer.id);
    }
    if (obj.type === 'point') {
        this.gcad.store.add(obj);
        this.pointPainter.draw(obj, obj.layerID);
    }
    if (obj.type === 'text') {
        this.gcad.store.add(obj);
        this.textPainter.draw(obj, obj.layerID);
    }
    if (obj.type === 'ruler') {
        this.gcad.store.add(obj);
        this.rulerPainter.draw(obj, obj.layerID);
    }
    if (obj.type === 'rect') {
        this.gcad.store.add(obj);
        this.rectPainter.draw(obj, obj.layerID);
    }
    if (obj.type === 'circle') {
        this.gcad.store.add(obj);
        this.circlePainter.draw(obj, obj.layerID);
    }
};

EDITOR.redraw = function(obj) {
    var layer = this.gcad.defaultLayer;

    if (obj.type === 'polyline') {
        this.polylinePainter.remove(obj);
        this.polylinePainter.draw(obj, obj.layerID);
    }
    if (obj.type === 'spline') {
        this.splinePainter.remove(obj);
        this.splinePainter.draw(obj, obj.layerID);
    }
    if (obj.type === 'path') {
        this.pathPainter.remove(obj);
        this.pathPainter.draw(obj, layer.id);
    }
    if (obj.type === 'line') {
        this.linePainter.remove(obj);
        this.linePainter.draw(obj, layer.id);
    }
    if (obj.type === 'point') {
        this.pointPainter.remove(obj);
        this.pointPainter.draw(obj, layer.id);
    }
    if (obj.type === 'text') {
        this.textPainter.remove(obj);
        this.textPainter.draw(obj, obj.layerID);
    }
    if (obj.type === 'ruler') {
        this.rulerPainter.remove(obj);
        this.rulerPainter.draw(obj, obj.layerID);
    }
    if (obj.type === 'rect') {
        this.rectPainter.remove(obj);
        this.rectPainter.draw(obj, obj.layerID);
    }
    if (obj.type === 'circle') {
        this.circlePainter.remove(obj);
        this.circlePainter.draw(obj, obj.layerID);
    }
};