/**
 * Получить координаты точки которая лежит на линии и перпендикулярна курсору...
 * @param x1 Координата x начала линии
 * @param y1 Координата y начала линии
 * @param x2 Координата x конца линии
 * @param y2 Координата y конца линии
 * @param xm Координата x позиции курсора
 * @param ym Координата y позиции курсора
 * @returns {*[]} Координаты точки
 */
EDITOR.helpers.getCoordinateToEdge = function(x1, y1, x2, y2, xm, ym) {
    var a = EDITOR.helpers.getDistanceBetweenTheTwoPoints(x1, y1, x2, y2);
    var b = EDITOR.helpers.getDistanceBetweenTheTwoPoints(x2, y2, xm, ym);
    var c = EDITOR.helpers.getDistanceBetweenTheTwoPoints(xm, ym, x1, y1);
    var p = (a + b + c) / 2;
    var distance = 2 / a * Math.sqrt(p * (p - a) * (p - b) * (p - c));
    var k = a/Math.sqrt(Math.pow(c, 2) - Math.pow(distance, 2));
    var vectorB = [x2 - x1, y2 - y1];
    var vectorA = [vectorB[0] / k, vectorB[1] / k];

    return [vectorA[0] + x1, vectorA[1] + y1];
};
