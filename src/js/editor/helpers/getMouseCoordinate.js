/**
 * Получить текущие координаты курсора
 * @returns {*[]} Координаты в виде массива [x, y]
 */
EDITOR.helpers.getMouseCoordinate = function() {
    var mouseX;
    var mouseY;
    var snap = EDITOR.snap;
    var gcad = EDITOR.gcad;
    var translateValueX = gcad.getTranslateValueX();
    var translateValueY = gcad.getTranslateValueY();
    var scaleValue = gcad.getScaleValue();
    var d3MouseX = d3.mouse(gcad.element.node())[0];
    var d3MouseY = d3.mouse(gcad.element.node())[1];

    //При включенной привязке координаты должны быть равны координатам привязки если они есть
    if (snap.enabledSnapPoint || snap.enabledSnapLine) {
        if (snap.statusSnapPoint) {
            mouseX = snap.snapPoint.selectionPoint.cx;
            mouseY = snap.snapPoint.selectionPoint.cy;
        } else if (snap.statusSnapLine) {
            mouseX = snap.snapLine.selectionPoint.cx;
            mouseY = snap.snapLine.selectionPoint.cy;
        } else if (snap.statusSnapCircle) {
            mouseX = snap.snapCircle.selectionPoint.cx;
            mouseY = snap.snapCircle.selectionPoint.cy;
        } else {
            //Если их нет получаем координаты ( с учетом масщтабирования )
            mouseX = d3MouseX / scaleValue - translateValueX / scaleValue;
            mouseY = d3MouseY / scaleValue - translateValueY / scaleValue;
        }
    } else {
        mouseX = d3MouseX / scaleValue - translateValueX / scaleValue;
        mouseY = d3MouseY / scaleValue - translateValueY / scaleValue;
    }

    return [mouseX, mouseY];
};
