/**
 * Копировать полилинию
 * @param polyline Целевая полилиния
 * @param layerId Иденитфикатор слоя в который помещается новая полилиния
 * @returns {object} Копия полилинии
 */
EDITOR.helpers.clonePolyline = function(polyline, layerId) {
    var newPolyline = new GCAD.Polyline();

    newPolyline.id = EDITOR.getUniqueId();
    newPolyline.isClose = polyline.isClose;
    newPolyline.color = polyline.color;
    newPolyline.opacity = polyline.opacity;
    newPolyline.opacity = polyline.opacity;

    newPolyline.style.edge.color = polyline.style.edge.color;
    newPolyline.style.edge.mouseoverColor = polyline.style.edge.mouseoverColor;
    newPolyline.style.edge.strokeWidth = polyline.style.edge.strokeWidth;
    newPolyline.style.edge.strokeDasharray = polyline.style.edge.strokeDasharray;
    newPolyline.style.edge.selectedColor = polyline.style.edge.selectedColor;
    newPolyline.style.selectedColor = polyline.style.selectedColor;

    polyline.points.forEach(function(item) {
        EDITOR.handlersStore.polyline.addPointInPolyline(newPolyline, item.cx, item.cy);
    });
    console.log(layerId);

    newPolyline.layerID = layerId;

    return newPolyline;
};
