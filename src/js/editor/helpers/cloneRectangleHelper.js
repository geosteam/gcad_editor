/**
 * Копировать прямоугольник
 * @param rect Целевой прямоугольник
 * @param layerId Иденитфикатор слоя в который помещается новый прямоугольник
 * @returns {object} Копия прямоугольника
 */
EDITOR.helpers.cloneRect = function(rect, layerId) {
    var newRect = new GCAD.Rect();

    newRect.id = EDITOR.getUniqueId();
    newRect.layerID = layerId;
    newRect.ownerID = newRect.id;

    newRect.color = rect.color;
    newRect.newColor = rect.newColor;
    newRect.strokeWidth = rect.strokeWidth;
    newRect.selectedColor = rect.selectedColor;
    newRect.strokeWidth = rect.strokeWidth;
    newRect.strokeDasharray = rect.strokeDasharray;
    newRect.fill = rect.fill;
    newRect.opacity = rect.opacity;

    newRect.coordOfAngle.upperLeft.x = rect.coordOfAngle.upperLeft.x;
    newRect.coordOfAngle.upperLeft.y = rect.coordOfAngle.upperLeft.y;
    newRect.coordOfAngle.upperRight.x = rect.coordOfAngle.upperRight.x;
    newRect.coordOfAngle.upperRight.y = rect.coordOfAngle.upperRight.y;
    newRect.coordOfAngle.lowerRight.x = rect.coordOfAngle.lowerRight.x;
    newRect.coordOfAngle.lowerRight.y = rect.coordOfAngle.lowerRight.y;
    newRect.coordOfAngle.lowerLeft.x = rect.coordOfAngle.lowerLeft.x;
    newRect.coordOfAngle.lowerLeft.y = rect.coordOfAngle.lowerLeft.y;

    newRect.points.upperLeft = new GCAD.Point(rect.points.upperLeft.cx, rect.points.upperLeft.cy);
    newRect.points.upperRight = new GCAD.Point(rect.points.upperRight.cx, rect.points.upperRight.cy);
    newRect.points.upperMiddle = new GCAD.Point(rect.points.upperMiddle.cx, rect.points.upperMiddle.cy);
    newRect.points.left = new GCAD.Point(rect.points.left.cx, rect.points.left.cy);
    newRect.points.right = new GCAD.Point(rect.points.right.cx, rect.points.right.cy);
    newRect.points.lowerLeft = new GCAD.Point(rect.points.lowerLeft.cx, rect.points.lowerLeft.cy);
    newRect.points.lowerRight = new GCAD.Point(rect.points.lowerRight.cx, rect.points.lowerRight.cy);
    newRect.points.lowerMiddle = new GCAD.Point(rect.points.lowerMiddle.cx, rect.points.lowerMiddle.cy);

    newRect.points.upperLeft.positionOnRect = 'upperLeft';
    newRect.points.upperRight.positionOnRect = 'upperRight';
    newRect.points.upperMiddle.positionOnRect = 'upperMiddle';
    newRect.points.left.positionOnRect = 'left';
    newRect.points.right.positionOnRect = 'right';
    newRect.points.lowerLeft.positionOnRect = 'lowerLeft';
    newRect.points.lowerRight.positionOnRect = 'lowerRight';
    newRect.points.lowerMiddle.positionOnRect = 'lowerMiddle';

    for (var key in rect.points) {
        newRect.points[key].id = EDITOR.getUniqueId();
        newRect.points[key].owner = 'rect';
        newRect.points[key].ownerID = newRect.id;
        newRect.points[key].layerID = layerId;
        EDITOR.applyStyleFromGlobal(newRect.points[key]);
        newRect.points[key].hide = true;
    }

    return newRect;
};
