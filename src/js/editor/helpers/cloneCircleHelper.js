/**
 * Копировать окружность
 * @param circle Целевая окружность
 * @param layerId Иденитфикатор слоя в который помещается новая окружность
 * @returns {object} Копия окружности
 */
EDITOR.helpers.cloneCircle = function(circle, layerId) {
        var newCircle = new GCAD.Circle();

        newCircle.id = EDITOR.getUniqueId();
        newCircle.ownerID = 'without-owner';

        newCircle.radius = circle.radius;
        newCircle.centerX = circle.centerX;
        newCircle.centerY = circle.centerY;
        newCircle.fill = circle.fill;

        newCircle.color = circle.color;
        newCircle.newColor = circle.newColor;
        newCircle.selectedColor = circle.selectedColor;
        newCircle.strokeWidth = circle.strokeWidth;
        newCircle.strokeDasharray = circle.strokeDasharray;
        newCircle.opacity = circle.opacity;
        newCircle.color = circle.color;

        newCircle.points.right = new GCAD.Point(circle.radius + circle.centerX, circle.centerY);
        newCircle.points.upper = new GCAD.Point(circle.centerX, circle.radius + circle.centerY);
        newCircle.points.left = new GCAD.Point(-circle.radius + circle.centerX, circle.centerY);
        newCircle.points.lower = new GCAD.Point(circle.centerX, -circle.radius + circle.centerY);
        newCircle.points.center = new GCAD.Point(circle.centerX, circle.centerY);

        newCircle.points.center.center = true; // Это центральная точка
        newCircle.points.right.positionOfCircle = 'right';
        newCircle.points.upper.positionOfCircle = 'upper';
        newCircle.points.left.positionOfCircle = 'left';
        newCircle.points.lower.positionOfCircle = 'lower';

        for (var key in newCircle.points) {
            newCircle.points[key].id = EDITOR.getUniqueId();
            newCircle.points[key].owner = 'circle';
            newCircle.points[key].ownerID = newCircle.id;
            newCircle.points[key].layerID = layerId;
            EDITOR.applyStyleFromGlobal(newCircle.points[key]);
            newCircle.points[key].hide = false;
        }

        newCircle.layerID = layerId;

        return newCircle;
};
