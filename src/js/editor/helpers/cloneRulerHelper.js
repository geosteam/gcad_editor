EDITOR.helpers.cloneRuler = function(ruler, layerId) {
    var newRuler = new GCAD.Ruler();
    var extensionLines = ruler.extensionLines;

    newRuler.id = EDITOR.getUniqueId();
    newRuler.ownerID = newRuler.id;

    newRuler.position = ruler.position;
    newRuler.width = ruler.width;
    newRuler.angle = ruler.angle;

    newRuler.color = ruler.color;
    newRuler.opacity = ruler.opacity;
    newRuler.strokeDasharray = ruler.strokeDasharray;
    newRuler.strokeWidth = ruler.strokeWidth;
    newRuler.offset = ruler.offset;
    newRuler.color = ruler.color;

    newRuler.pointLeft.x = ruler.pointLeft.x;
    newRuler.pointLeft.y = ruler.pointLeft.y;

    newRuler.pointRight.x = ruler.pointRight.x;
    newRuler.pointRight.y = ruler.pointRight.y;

    newRuler.extensionLines.leftLine = new GCAD.Line();
    newRuler.extensionLines.rightLine = new GCAD.Line();
    newRuler.extensionLines.dimensionLine = new GCAD.Line();

    newRuler.extensionLines.leftLine.coordinates.begin.x = extensionLines.leftLine.coordinates.begin.x;
    newRuler.extensionLines.leftLine.coordinates.begin.y = extensionLines.leftLine.coordinates.begin.y;
    newRuler.extensionLines.leftLine.coordinates.close.x = extensionLines.leftLine.coordinates.close.x;
    newRuler.extensionLines.leftLine.coordinates.close.y = extensionLines.leftLine.coordinates.close.y;

    newRuler.extensionLines.rightLine.coordinates.begin.x = extensionLines.rightLine.coordinates.begin.x;
    newRuler.extensionLines.rightLine.coordinates.begin.y = extensionLines.rightLine.coordinates.begin.y;
    newRuler.extensionLines.rightLine.coordinates.close.x = extensionLines.rightLine.coordinates.close.x;
    newRuler.extensionLines.rightLine.coordinates.close.y = extensionLines.rightLine.coordinates.close.y;

    newRuler.extensionLines.dimensionLine.coordinates.begin.x = extensionLines.dimensionLine.coordinates.begin.x;
    newRuler.extensionLines.dimensionLine.coordinates.begin.y = extensionLines.dimensionLine.coordinates.begin.y;
    newRuler.extensionLines.dimensionLine.coordinates.close.x = extensionLines.dimensionLine.coordinates.close.x;
    newRuler.extensionLines.dimensionLine.coordinates.close.y = extensionLines.dimensionLine.coordinates.close.y;

    newRuler.dimensionPoint = new GCAD.Point();
    newRuler.label = new GCAD.Text();

    newRuler.dimensionPoint.cx = ruler.dimensionPoint.cx;
    newRuler.dimensionPoint.cy = ruler.dimensionPoint.cy;

    newRuler.label.position.x = ruler.label.position.x;
    newRuler.label.position.y = ruler.label.position.y;

    newRuler.label.ownerID = newRuler.id;
    newRuler.label.color = ruler.label.color;
    newRuler.label.size = ruler.label.size;
    newRuler.label.value = ruler.label.value;
    newRuler.label.rotate = ruler.label.rotate;

    newRuler.layerID = layerId;
    newRuler.dimensionPoint.layerID = layerId;

    EDITOR.applyStyleFromGlobal(newRuler.dimensionPoint);

    return newRuler;
};
