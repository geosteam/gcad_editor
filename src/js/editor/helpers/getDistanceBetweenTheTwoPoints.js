/**
 * Расстояние между двуми точками
 * @param x Координата x первой точки
 * @param y Координата y первой точки
 * @param x2 Координата x второй точки
 * @param y2 Координата y второй точки
 * @returns {number} Расстояние
 */
EDITOR.helpers.getDistanceBetweenTheTwoPoints = function(x, y, x2, y2) {
    return Math.sqrt(Math.pow(x2 - x, 2) + Math.pow(y2 - y, 2));
};
