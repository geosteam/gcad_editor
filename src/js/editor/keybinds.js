/**
 * Горячие клавиши
 * Используется библиотека keymaster.js
 */
EDITOR.setDefaultKeyBinds = function() {
    /**
     * Delete
     */
    key('delete', function() {
        var selectedObjectsStore = EDITOR.gcad.selectedObjectsStore;

        deletePolylines();
        deleteRulers();
        deleteRectangles();
        deleteCircles();
        deleteSplines();

        function deletePolylines() {
            for (var key in selectedObjectsStore.polylines) {
                EDITOR.removeObject(selectedObjectsStore.polylines[key]);
            }
        }

        function deleteSplines() {
            for (var key in selectedObjectsStore.splines) {
                EDITOR.removeObject(selectedObjectsStore.splines[key]);
            }
        }

        function deleteRulers() {
            for (var key in selectedObjectsStore.rulers) {
                EDITOR.removeObject(selectedObjectsStore.rulers[key]);
            }
        }

        function deleteRectangles() {
            for (var key in selectedObjectsStore.rectangles) {
                EDITOR.removeObject(selectedObjectsStore.rectangles[key]);
            }
        }

        function deleteCircles() {
            for (var key in selectedObjectsStore.circles) {
                EDITOR.removeObject(selectedObjectsStore.circles[key]);
            }
        }
    });

    /**
     * Esc
     */
    key('esc', function() {
        EDITOR.resetGcadSelections();
    });
};

EDITOR.disabledKeyBinds = function() {
    key.unbind('esc');
    key.unbind('delete');
};
