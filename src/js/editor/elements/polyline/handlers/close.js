EDITOR.handlersStore.polyline.close = function(polyline) {

    //EDITOR.resetGcadSelections();
    EDITOR.removeObject(polyline);

    var newEdge = new GCAD.Line();

    newEdge.setCoordinates({
        begin: {
            x: polyline.points[polyline.getAmountOfPoints() - 1].cx,
            y: polyline.points[polyline.getAmountOfPoints() - 1].cy
        },
        close: {
            x: polyline.points[0].cx,
            y: polyline.points[0].cy
        }
    });

    newEdge.id = polyline.edges.length;
    polyline.edges[newEdge.id] = newEdge;
    newEdge.ownerID = polyline.id;
    newEdge.owner = 'polyline';
    polyline.isClose = true;

    polyline.closingEdge = newEdge;

    EDITOR.draw(polyline);
    EDITOR.handlersStore.polyline.setSelect(polyline);

    EDITOR.gcad.UndoManager.add({
        undo: function() {
            EDITOR.handlersStore.polyline.open(polyline);
        },
        redo: function() {
            EDITOR.handlersStore.polyline.close(polyline);
        }
    });
};
