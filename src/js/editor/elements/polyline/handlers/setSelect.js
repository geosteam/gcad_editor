EDITOR.handlersStore.polyline.setSelect = function(polyline) {
    var gcad = EDITOR.gcad;

    polyline.color = GCAD.Style.polyline.selectedColor;

    polyline.edges.forEach(function(item) {
        item.color = polyline.color;
        item.element.style('stroke', polyline.style.edge.strokeWidth);
    });

    polyline.points.forEach(function(item) {
        if (item.hide === false) {
            return;
        }
        EDITOR.pointPainter.showPoint(item);
    });

    EDITOR.redraw(polyline);
    gcad.selectedObjectsStore.add(polyline);
    polyline.isSelected = true;
    EDITOR.handlersStore.polyline.setMouseMoveEvent(polyline);
};
