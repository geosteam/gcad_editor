EDITOR.handlersStore.polyline.deletePointOpening = function(polyline, point) {
    var handlersForPolyline = EDITOR.handlersStore.polyline;
    EDITOR.removeObject(polyline);

    if (polyline.isClose) {
        deletePointFromClosingPolyline();
    } else {
        deletePointFromPolyline();
    }

    /**
     * Удалить точку из полилинии
     */
    function deletePointFromPolyline() {
        var arrayPointLeft = polyline.points.slice(0, +point.id + 1 );
        var arrayPointRight = polyline.points.slice(point.id);
        var newPolylineLeft = new GCAD.Polyline();
        var newPolylineRight = new GCAD.Polyline();

        setLayerId(newPolylineLeft);
        setLayerId(newPolylineRight);
        setId(newPolylineLeft);
        setId(newPolylineRight);
        EDITOR.applyStyleFromGlobal(newPolylineLeft);
        EDITOR.applyStyleFromGlobal(newPolylineRight);


        for (var i = 0, l = arrayPointLeft.length; i < l; i++) {
            handlersForPolyline.addPointInPolyline(newPolylineLeft, arrayPointLeft[i].cx, arrayPointLeft[i].cy);
        }

        for (var j = 0, k = arrayPointRight.length; j < k; j++) {
            handlersForPolyline.addPointInPolyline(newPolylineRight, arrayPointRight[j].cx, arrayPointRight[j].cy);
        }

        EDITOR.draw(newPolylineLeft);
        EDITOR.draw(newPolylineRight);
        handlersForPolyline.setSelect(newPolylineLeft);
        handlersForPolyline.setSelect(newPolylineRight);
    }

    /**
     * Удалить точку из замкнутой полилинии
     */
    function deletePointFromClosingPolyline() {
        var newPolyline = new GCAD.Polyline();
        var points = [];
        var staticId = 0;

        setLayerId(newPolyline);
        setId(newPolyline);
        EDITOR.applyStyleFromGlobal(newPolyline);


        polyline.points.forEach(function(item) {
            if (item.id >= point.id) {
                points.push(item);
            }
        });

        polyline.points.forEach(function(item) {
            if (item.id < point.id) {
                points.push(item);
            }
        });

        points.forEach(function(item) {
            item.id = staticId;
            handlersForPolyline.addPointInPolyline(newPolyline, item.cx, item.cy);
            staticId++;
        });

        handlersForPolyline.addPointInPolyline(newPolyline, points[0].cx, points[0].cy);

        EDITOR.draw(newPolyline);
        handlersForPolyline.setSelect(newPolyline);
    }

    function setLayerId(obj) {
        obj.layerID = EDITOR.gcad.defaultLayer.id;
    }

    function setId(obj) {
        obj.id = EDITOR.getUniqueId();
    }

};
