EDITOR.handlersStore.polyline.getPointEvent = function(polylineID, pointID) {
    var gcad = EDITOR.gcad;
    var polyline = gcad.store.polylines[polylineID];
    var point = polyline.points[pointID];
    var snap = EDITOR.snap;
    var enabledSnap = false;

    EDITOR.zoomOff();
    EDITOR.gcad.element.style('cursor', 'cell');
    point.element.style('cursor', 'cell');

    if (snap.enabledSnapPoint || snap.enabledSnapLine) {
        enabledSnap = true;
        EDITOR.snapMousemove();
    }

    gcad.element.on('mousemove', function() {
        snap.excludePoints.push({id: pointID, ownerId: polyline.points[pointID].ownerID});

        if (enabledSnap) {
            EDITOR.snapMousemove();
        }

        if (pointID < 1) {
            snap.excludeLines.push({id: pointID, ownerId: polyline.edges[pointID].ownerID});
        } else if (pointID == polyline.points.length - 1) {
            snap.excludeLines.push({id: pointID - 1, ownerId: polyline.edges[pointID - 1].ownerID});
        } else {
            snap.excludeLines.push({id: pointID, ownerId: polyline.edges[pointID].ownerID});
            snap.excludeLines.push({id: pointID - 1, ownerId: polyline.edges[pointID - 1].ownerID});
        }

        var mouseX = EDITOR.helpers.getMouseCoordinate()[0];
        var mouseY = EDITOR.helpers.getMouseCoordinate()[1];

        point.cx = mouseX;
        point.cy = mouseY;

        if (pointID < 1) { //первая точка
            polyline.edges[pointID].coordinates.begin.x = mouseX;
            polyline.edges[pointID].coordinates.begin.y = mouseY;

            EDITOR.redraw(polyline.edges[pointID]);
            EDITOR.redraw(polyline.points[+pointID + 1]);

            if (polyline.isClose) {
                polyline.edges[polyline.edges.length - 1].coordinates.close.x = mouseX;
                polyline.edges[polyline.edges.length - 1].coordinates.close.y = mouseY;

                EDITOR.redraw(polyline.edges[polyline.edges.length - 1]);
                EDITOR.redraw(polyline.points[polyline.points.length - 1]);
            }
        } else if (pointID == polyline.points.length - 1) { //последняя точка
            polyline.edges[pointID - 1].coordinates.close.x = mouseX;
            polyline.edges[pointID - 1].coordinates.close.y = mouseY;

            EDITOR.redraw(polyline.edges[pointID - 1]);
            EDITOR.redraw(polyline.points[pointID - 1]);

            if (polyline.isClose) {
                polyline.edges[polyline.edges.length - 1].coordinates.begin.x = mouseX;
                polyline.edges[polyline.edges.length - 1].coordinates.begin.y = mouseY;

                EDITOR.redraw(polyline.edges[polyline.edges.length - 1]);
                EDITOR.redraw(polyline.points[0]);
            }
        } else { // остальные точки
            polyline.edges[pointID - 1].coordinates.close.x = mouseX;
            polyline.edges[pointID - 1].coordinates.close.y = mouseY;

            polyline.edges[pointID].coordinates.begin.x = mouseX;
            polyline.edges[pointID].coordinates.begin.y = mouseY;

            EDITOR.redraw(polyline.edges[pointID - 1]);
            EDITOR.redraw(polyline.edges[pointID]);

            EDITOR.redraw(polyline.points[pointID - 1]);
            EDITOR.redraw(polyline.points[+pointID + 1]);
        }

        EDITOR.redraw(point);

        /**
         * При перерисовке создается новый point, поэтому применяем стиль заново
         */
        point.element.style('cursor', 'cell');
    });

    gcad.element.on('mousedown', function () {
        gcad.element.on('mousedown', null);
        gcad.element.on('mousemove', null);
        EDITOR.setHandlersDefault();
        EDITOR.handlersStore.polyline.setMouseMoveEvent(polyline);
        EDITOR.gcad.element.style('cursor', 'default');
        point.element.style('cursor', 'default');
        EDITOR.resetSnap();
        EDITOR.zoomOn();
    });
};
