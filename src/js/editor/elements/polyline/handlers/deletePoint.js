EDITOR.handlersStore.polyline.deletePoint = function(polyline, point) {
    var newPoints = polyline.points;
    var newEdges = polyline.edges;
    var oldPolyline = new GCAD.Polyline();

    EDITOR.removeObject(polyline);

    if (polyline.points.length === 2) {
        return false;
    }

    polyline.points.forEach(function(item) {
        EDITOR.handlersStore.polyline.addPointInPolyline(oldPolyline, item.cx, item.cy);
    });

    oldPolyline.layerID = EDITOR.gcad.defaultLayer.id;
    oldPolyline.id = EDITOR.getUniqueId();
    EDITOR.applyStyleFromGlobal(oldPolyline);

    polyline.points = [];
    polyline.edges = [];
    polyline.selectedPoint = null;
    polyline.selectedEdge = null;

    newPoints.splice(point.id, 1);
    newEdges.splice(point.id - 1, 1);
    newEdges.splice(point.id, 1);

    for (var i = 0, l = newPoints.length; i < l; i++) {
        EDITOR.handlersStore.polyline.addPointInPolyline(polyline, newPoints[i].cx, newPoints[i].cy);
    }

    EDITOR.draw(polyline);
    EDITOR.handlersStore.polyline.setSelect(polyline);

    if (polyline.points.length <= 2) {
        polyline.isClose = false;
    }

    if (polyline.isClose) {
        EDITOR.handlersStore.polyline.close(polyline);
    }

    EDITOR.gcad.UndoManager.add({
        undo: function() {
            EDITOR.removeObject(polyline);
            EDITOR.draw(oldPolyline);
        },
        redo: function() {
            EDITOR.removeObject(oldPolyline);
            EDITOR.draw(polyline);
        }
    });
};
