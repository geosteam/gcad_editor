EDITOR.handlersStore.polyline.open = function(polyline) {
    if (polyline.isClose === false) {
        return false;
    }

    EDITOR.handlersStore.polyline.resetSelect(polyline);

    EDITOR.removeObject(polyline);
    polyline.open();
    EDITOR.draw(polyline);

    EDITOR.gcad.UndoManager.add({
        undo: function() {
            EDITOR.handlersStore.polyline.close(polyline);
        },
        redo: function() {
            EDITOR.handlersStore.polyline.open(polyline);
        }
    });
};
