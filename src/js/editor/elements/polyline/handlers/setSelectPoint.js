EDITOR.handlersStore.polyline.setSelectPoint = function(polyline, point) {
    var pointPainter = EDITOR.pointPainter;
    var linePainter = EDITOR.pathPainter;

    if (polyline.selectedPoint) {
        var selectPoint = polyline.selectedPoint;

        selectPoint.isSelected = false;
        pointPainter.setColor(selectPoint, GCAD.Style.point.color);
        EDITOR.redraw(selectPoint);
    }

    if (polyline.selectedEdge) {
        var selectEdge = polyline.selectedEdge;

        linePainter.setColor(selectEdge, polyline.color);

        selectEdge.isSelected = false;

        EDITOR.redraw(selectEdge);
        EDITOR.redraw(polyline.points[selectEdge.id]);
        EDITOR.redraw(polyline.points[+selectEdge.id + 1]);
    }

    polyline.selectedPoint = point;
    polyline.selectedEdge = null;

    pointPainter.setColor(point, GCAD.Style.point.selectedColor);
    EDITOR.redraw(point);
    point.isSelected = true;
};
