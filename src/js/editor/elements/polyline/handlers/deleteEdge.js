EDITOR.handlersStore.polyline.deleteEdge = function(polyline, edge) {
    var pointRight = polyline.points[edge.id + 1];
    var handlersForPolyline = EDITOR.handlersStore.polyline;

    if (polyline.isClose) {
        deleteEdgeFromClosingPolyline();
    } else {
        deleteEdgeFromPolyline();
    }

    function deleteEdgeFromPolyline() {
        var arrayPointLeft = polyline.points.slice(0, pointRight.id);
        var arrayPointRight = polyline.points.slice(pointRight.id);
        var newPolylineLeft = new GCAD.Polyline();
        var newPolylineRight = new GCAD.Polyline();

        newPolylineLeft.layerID = EDITOR.gcad.defaultLayer.id;
        newPolylineRight.layerID = EDITOR.gcad.defaultLayer.id;
        newPolylineLeft.id = EDITOR.getUniqueId();
        newPolylineRight.id = EDITOR.getUniqueId();
        EDITOR.applyStyleFromGlobal(newPolylineLeft);
        EDITOR.applyStyleFromGlobal(newPolylineRight);

        for (var i = 0, l = arrayPointLeft.length; i < l; i++) {
            handlersForPolyline.addPointInPolyline(newPolylineLeft, arrayPointLeft[i].cx, arrayPointLeft[i].cy);
        }

        for (var j = 0, k = arrayPointRight.length; j < k; j++) {
            handlersForPolyline.addPointInPolyline(newPolylineRight, arrayPointRight[j].cx, arrayPointRight[j].cy);
        }

        EDITOR.removeObject(polyline);

        if (newPolylineLeft.points.length >= 2) {
            EDITOR.draw(newPolylineLeft);
            handlersForPolyline.setSelect(newPolylineLeft);
        }
        if (newPolylineRight.points.length >= 2) {
            EDITOR.draw(newPolylineRight);
            handlersForPolyline.setSelect(newPolylineRight);
        }

        if (newPolylineLeft.points.length < 2) {
            EDITOR.gcad.UndoManager.add({
                undo: function() {
                    EDITOR.removeObject(newPolylineRight);
                    EDITOR.draw(polyline);
                },
                redo: function() {
                    EDITOR.draw(newPolylineRight);
                    EDITOR.removeObject(polyline);
                }
            });
        } else if (newPolylineRight.points.length < 2) {
            EDITOR.gcad.UndoManager.add({
                undo: function() {
                    EDITOR.removeObject(newPolylineLeft);
                    EDITOR.draw(polyline);
                },
                redo: function() {
                    EDITOR.draw(newPolylineLeft);
                    EDITOR.removeObject(polyline);
                }
            });
        } else {
            EDITOR.gcad.UndoManager.add({
                undo: function() {
                    EDITOR.removeObject(newPolylineLeft);
                    EDITOR.removeObject(newPolylineRight);
                    EDITOR.draw(polyline);
                },
                redo: function() {
                    EDITOR.draw(newPolylineLeft);
                    EDITOR.draw(newPolylineRight);
                    EDITOR.removeObject(polyline);
                }
            });
        }
    }

    function deleteEdgeFromClosingPolyline() {
        var newPolyline = new GCAD.Polyline();
        var points = [];
        var staticId = 0;

        newPolyline.id = EDITOR.getUniqueId();
        newPolyline.layerID = EDITOR.gcad.defaultLayer.id;
        EDITOR.applyStyleFromGlobal(newPolyline);

        if (polyline.closingEdge.id === edge.id) {
            polyline.points.forEach(function(item) {
                    points.push(item);
            });
        } else {
            polyline.points.forEach(function(item) {
                if (item.id >= pointRight.id) {
                    points.push(item);
                }
            });

            polyline.points.forEach(function(item) {
                if (item.id < pointRight.id) {
                    points.push(item);
                }
            });
        }

        points.forEach(function(item) {
            item.id = staticId;
            handlersForPolyline.addPointInPolyline(newPolyline, item.cx, item.cy);
            staticId++;
        });

        EDITOR.removeObject(polyline);
        EDITOR.draw(newPolyline);
        handlersForPolyline.setSelect(newPolyline);

        EDITOR.gcad.UndoManager.add({
            undo: function() {
                EDITOR.removeObject(newPolyline);
                EDITOR.draw(polyline);
            },
            redo: function() {
                EDITOR.removeObject(polyline);
                EDITOR.draw(newPolyline);
            }
        });
    }
};
