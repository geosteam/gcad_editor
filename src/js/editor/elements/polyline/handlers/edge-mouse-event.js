EDITOR.handlersStore.polyline.getEdgeEvent = function(polylineID) {
    var translateValueX = null;
    var translateValueY = null;
    var scaleValue = null;
    var gcad = EDITOR.gcad;
    var polyline = gcad.store.polylines[polylineID];
    var pseudoPolyline = new GCAD.Path();
    var basePointX = 0;
    var basePointY = 0;
    var offsetX = 0;
    var offsetY = 0;
    var pseudoPolylinePainted = false;
    var arrayPointsOfPolyline = [];


    pseudoPolyline.layerID = gcad.defaultLayer.id;
    EDITOR.applyStyleFromGlobal(pseudoPolyline);
    pseudoPolyline.strokeDasharray = 5;
    pseudoPolyline.setInterpolate('linear');

    polyline.points.forEach(function(item) {
        var point = {x: item.cx, y: item.cy};
        arrayPointsOfPolyline.push(point);
    });

    if (polyline.isClose) {
        var points = {x: polyline.points[0].cx, y: polyline.points[0].cy};
        arrayPointsOfPolyline.push(points);
    }

    gcad.element.on('mousemove', null);
    gcad.element.on('mousedown', null);
    gcad.element.on('mouseup', null);

    EDITOR.zoomOff();

    gcad.element.on('mousemove', function() {
        translateValueX = gcad.getTranslateValueX();
        translateValueY = gcad.getTranslateValueY();
        scaleValue = gcad.getScaleValue();

        var moveX = d3.mouse(gcad.element.node())[0] / scaleValue - translateValueX / scaleValue;
        var moveY = d3.mouse(gcad.element.node())[1] / scaleValue - translateValueY / scaleValue;

        offsetX = (basePointX !== 0 && basePointY !== 0) ? moveX - basePointX : 0;
        offsetY = (basePointX !== 0 && basePointY !== 0) ? moveY - basePointY : 0;

        for (var i = 0, l = arrayPointsOfPolyline.length; i < l; i++) {
            arrayPointsOfPolyline[i].x = arrayPointsOfPolyline[i].x + offsetX;
            arrayPointsOfPolyline[i].y = arrayPointsOfPolyline[i].y + offsetY;
        }

        pseudoPolyline.arrayOfPoints = arrayPointsOfPolyline;

        basePointX = moveX;
        basePointY = moveY;

        if (!pseudoPolylinePainted) {
            EDITOR.draw(pseudoPolyline);
        } else {
            EDITOR.redraw(pseudoPolyline);
        }

        pseudoPolylinePainted = true;
    });

    gcad.element.on('mouseup', function() {
        if (pseudoPolylinePainted) {
            var newPolyline = new GCAD.Polyline();

            newPolyline.id = EDITOR.getUniqueId();
            newPolyline.layerID = polyline.layerID;

            EDITOR.applyStyleFromGlobal(newPolyline);
            EDITOR.removeObject(pseudoPolyline);
            EDITOR.removeObject(polyline);

            if (polyline.isClose) {
                pseudoPolyline.arrayOfPoints.pop();
            }

            for (var i = 0, l = pseudoPolyline.arrayOfPoints.length; i < l; i++) {
                EDITOR.handlersStore.polyline.addPointInPolyline(
                    newPolyline,
                    pseudoPolyline.arrayOfPoints[i].x,
                    pseudoPolyline.arrayOfPoints[i].y
                );
            }

            EDITOR.draw(newPolyline);
            EDITOR.handlersStore.polyline.setSelect(newPolyline);

            if (polyline.isClose) {
                EDITOR.handlersStore.polyline.close(newPolyline);
                newPolyline.isClose = true;
            }
        }

        gcad.element.on('mousemove', null);
        gcad.element.on('mousedown', null);
        gcad.element.on('mouseup', null);

        //EDITOR.handlersStore.polyline.resetSelect(newPolyline);
        //EDITOR.handlersStore.polyline.setMouseMoveEvent(newPolyline);
        EDITOR.setHandlersDefault();
        EDITOR.zoomOn();
    });
};
