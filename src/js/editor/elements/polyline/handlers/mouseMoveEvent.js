EDITOR.handlersStore.polyline.setMouseMoveEvent = function(polyline) {
    var handlers = EDITOR.handlersStore;

    polyline.edges.forEach(function(item) {
        if (item.isSelected) {
            item.element.style('stroke', polyline.style.edge.selectedColor);
        }
        handlers.setMouseoverLine(item, polyline.style.edge.mouseoverColor);

        if (item.isSelected) {
            handlers.setMouseoutLine(item, polyline.style.edge.selectedColor);
        } else {
            handlers.setMouseoutLine(item, polyline.color);
        }
    });

    polyline.points.forEach(function(item) {
        handlers.setMouseoverPoint(item, GCAD.Style.point.mouseoverColor);

        if (item.isSelected) {
            handlers.setMouseoutPoint(item, GCAD.Style.point.selectedColor);
        } else {
            handlers.setMouseoutPoint(item, GCAD.Style.point.color);
        }
    });
};
