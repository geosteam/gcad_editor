EDITOR.handlersStore.polyline.resetSelect = function(polyline) {
    var gcad  = EDITOR.gcad;
    var linePainter = EDITOR.linePainter;

    polyline.color = GCAD.Style.polyline.color;

    if (polyline.selectedEdge) {
        linePainter.setColor(polyline.selectedEdge, polyline.style.edge.color);
        EDITOR.redraw(polyline.selectedEdge);
        EDITOR.redraw(polyline.points[polyline.selectedEdge.id]);
        if (polyline.isClose && (polyline.edges.length - 1) === polyline.selectedEdge.id) {
            EDITOR.redraw(polyline.points[0]);
        } else {
            EDITOR.redraw(polyline.points[polyline.selectedEdge.id + 1]);
        }
        polyline.selectedEdge.isSelected = false;
        polyline.selectedEdge = null;
    }

    if (polyline.selectedPoint) {
        polyline.selectedPoint.color = GCAD.Style.point.color;
        EDITOR.pointPainter.redraw(polyline.selectedPoint);
        polyline.selectedPoint.isSelected = false;
        polyline.selectedPoint = null;
    }

    polyline.points.forEach(function(item) {
        if (item.hide === true) {
            return;
        }
        EDITOR.pointPainter.hidePoint(item);
        item.isSelected = false;
    });

    EDITOR.applyStyleFromGlobal(polyline);
    EDITOR.redraw(polyline);

    delete gcad.selectedObjectsStore.polylines[polyline.id];
    polyline.isSelected = false;

    EDITOR.handlersStore.polyline.setMouseMoveEvent(polyline);
};
