EDITOR.handlersStore.polyline.addPoint = function(polyline, edge) {
    var edgeID = edge.id;
    var newPolyline = new GCAD.Polyline();
    var amountOfPolylinePoints = polyline.points.length;
    var handlerPolyline = EDITOR.handlersStore.polyline;

    newPolyline.id = EDITOR.getUniqueId();
    newPolyline.layerID = EDITOR.gcad.defaultLayer.id;
    EDITOR.removeObject(polyline);
    EDITOR.applyStyleFromGlobal(newPolyline);

    if (polyline.isClose) {
        if (polyline.closingEdge.id === edgeID) {
            addPointInClosingPolyline();
        } else {
            addPoint();
        }
        EDITOR.draw(newPolyline);
        handlerPolyline.close(newPolyline);
    } else {
        addPoint();
        EDITOR.draw(newPolyline);
    }

    handlerPolyline.setSelect(newPolyline);

    EDITOR.gcad.UndoManager.add({
        undo: function() {
            EDITOR.removeObject(newPolyline);
            EDITOR.draw(polyline);
        },
        redo: function() {
            EDITOR.removeObject(polyline);
            EDITOR.draw(newPolyline);
        }
    });

    /**
     * Добавляем точку в полилинию
     */
    function addPoint() {
        var pointLeft = polyline.points[edgeID];
        var pointRight = polyline.points[edgeID + 1];
        var offsetX = (pointRight.cx - pointLeft.cx)/2;
        var offsetY = (pointRight.cy - pointLeft.cy)/2;

        for (var i = 0, l = amountOfPolylinePoints; i < l; i++) {
            if (i === edgeID + 1) {
                handlerPolyline.addPointInPolyline(newPolyline, pointLeft.cx + offsetX, pointLeft.cy + offsetY);
                handlerPolyline.addPointInPolyline(newPolyline, polyline.points[i].cx, polyline.points[i].cy);
            } else {
                handlerPolyline.addPointInPolyline(newPolyline, polyline.points[i].cx, polyline.points[i].cy);
            }
        }
    }

    /**
     * Добавляем точку в замкнутую полилинию
     */
    function addPointInClosingPolyline() {
        var pointLeft = polyline.points[polyline.points.length - 1];
        var pointRight = polyline.points[0];
        var offsetX = (pointRight.cx - pointLeft.cx)/2;
        var offsetY = (pointRight.cy - pointLeft.cy)/2;

        for (var i = 0, l = amountOfPolylinePoints; i < l; i++) {
            if (i === polyline.points.length - 1) {
                handlerPolyline.addPointInPolyline(newPolyline, polyline.points[i].cx, polyline.points[i].cy);
                handlerPolyline.addPointInPolyline(newPolyline, pointLeft.cx + offsetX, pointLeft.cy + offsetY);
            } else {
                handlerPolyline.addPointInPolyline(newPolyline, polyline.points[i].cx, polyline.points[i].cy);
            }
        }
    }
};

EDITOR.handlersStore.polyline.addPointInPolyline = function(polyline, x, y) {
    var newPoint = new GCAD.Point(x, y);
    var amountOfPoints = polyline.points.length;

    newPoint.id = polyline.points.length;
    newPoint.layerID = EDITOR.gcad.defaultLayer.id;
    EDITOR.applyStyleFromGlobal(newPoint);

    polyline.points[newPoint.id] = newPoint;
    newPoint.owner = 'polyline';
    newPoint.ownerID = polyline.id;

    if (amountOfPoints > 0) {
        var newEdge = new GCAD.Line();
        newEdge.layerID = polyline.layerID;


        newEdge.setCoordinates({
            begin: {
                x: polyline.points[amountOfPoints - 1].cx,
                y: polyline.points[amountOfPoints - 1].cy
            },
            close: {
                x: x,
                y: y
            }
        });
        newEdge.id = polyline.edges.length;
        newEdge.layerID = EDITOR.gcad.defaultLayer.id;
        EDITOR.applyStyleFromGlobal(newEdge);
        newEdge.color = polyline.color;
        polyline.edges[newEdge.id] = newEdge;
        newEdge.owner = 'polyline';
        newEdge.ownerID = polyline.id;
    }
};
