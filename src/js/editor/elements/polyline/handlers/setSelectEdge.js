EDITOR.handlersStore.polyline.setSelectEdge = function(polyline, line) {
    var linePainter = EDITOR.linePainter;
    var pointPainter = EDITOR.pointPainter;

    if (polyline.selectedEdge) {
        if (line.id === polyline.selectedEdge.id) {
            return false;
        }

        var selectEdge = polyline.selectedEdge;

        linePainter.setColor(polyline.selectedEdge, polyline.color);
        polyline.selectedEdge.isSelected = false;

        EDITOR.redraw(selectEdge);
        EDITOR.redraw(polyline.points[selectEdge.id]);
        EDITOR.redraw(polyline.points[+selectEdge.id + 1]);
    }

    if (polyline.selectedPoint) {
        pointPainter.setColor(polyline.selectedPoint, GCAD.Style.point.color);
        EDITOR.redraw(polyline.selectedPoint);
    }

    polyline.selectedEdge = line;
    polyline.selectedPoint = null;

    linePainter.setColor(line, polyline.style.edge.selectedColor);
    EDITOR.redraw(line);

    EDITOR.redraw(polyline.points[line.id]);

    if (polyline.isClose && (polyline.edges.length - 1) === line.id) {
        EDITOR.redraw(polyline.points[0]);
    } else {
        EDITOR.redraw(polyline.points[line.id + 1]);
    }

    line.isSelected = true;

    EDITOR.handlersStore.polyline.setMouseMoveEvent(polyline);
};