/**
 * Рисовальшик для полилинии
 */
EDITOR.polylinePainter = {
    draw: function(polyline, layerID) {
        //Сначала рисум сигменты (которые являются линиями)
        for (var i = 0, l = polyline.edges.length; i < l; i++) {
            polyline.edges[i].color = polyline.color;
            polyline.edges[i].ownerID = polyline.id;
            polyline.edges[i].strokeWidth = polyline.style.edge.strokeWidth;
            polyline.edges[i].opacity = polyline.style.edge.opacity;
            polyline.edges[i].strokeDasharray = polyline.style.edge.strokeDasharray;

            EDITOR.linePainter.draw(polyline.edges[i], layerID);
        }

        //Потом рисуем точки
        for (var j = 0, k = polyline.points.length; j < k; j++) {
            EDITOR.pointPainter.draw(polyline.points[j], layerID);
        }

        polyline.layerID = layerID;
    },
    remove: function(polyline) {
        for (var i = 0, l = polyline.edges.length; i < l; i++) {
            EDITOR.linePainter.remove(polyline.edges[i]);
        }
        for (var j = 0, k = polyline.points.length; j < k; j++) {
            EDITOR.pointPainter.remove(polyline.points[j]);
        }
    },
    redraw: function(polyline) {
        this.remove(polyline);
        this.draw(polyline, polyline.layerID);
    }
};
