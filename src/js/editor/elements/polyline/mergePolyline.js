/**
 * Объединяем две полилинии
 * @param callback
  */
EDITOR.mergePolylines = function(callback) {
    var gcad = EDITOR.gcad;
    var selectedPolylines = gcad.selectedObjectsStore.polylines;
    var newPolyline = new GCAD.Polyline();
    var handlerPolyline = EDITOR.handlersStore.polyline;

    newPolyline.layerID = EDITOR.gcad.defaultLayer.id;
    newPolyline.id = EDITOR.getUniqueId();

    /**
     * Длинный блок проверок
     */
    if (Object.keys(selectedPolylines) < 2 || Object.keys(selectedPolylines) > 2) {
        console.warn('Можно объединить только две полилинии...');
        return false;
    }

    for (var key in selectedPolylines) {
        if (selectedPolylines[key].selectedPoint) {

            if (selectedPolylines[key].selectedPoint.id !== 0) {
                if (selectedPolylines[key].selectedPoint.id !== selectedPolylines[key].points.length - 1) {
                    console.warn('Можно выбирать только крайние точки...');
                    return false;
                }
            }
        } else {
            console.warn('Надо выбрать точку соединения... ');
            return false;
        }
    }


    /**
     * Проверки пройдены успешно...
     */

    var firstIteration = true;

    for (key in selectedPolylines) {
        if (selectedPolylines[key].selectedPoint.id === 0) {
            if (firstIteration) {
                sortPointsForNewPolylineFromEnd(selectedPolylines[key]);
            } else {
                sortPointsForNewPolylineAtFirst(selectedPolylines[key]);
            }
        } else {
            if (firstIteration) {
                sortPointsForNewPolylineAtFirst(selectedPolylines[key]);
            } else {
                sortPointsForNewPolylineFromEnd(selectedPolylines[key]);
            }
        }
        firstIteration = false;
    }

    /**
     * Перебор точек полилинии с начала
     * @param polyline
     */
    function sortPointsForNewPolylineAtFirst(polyline) {
        polyline.points.forEach(function(item) {
            handlerPolyline.addPointInPolyline(newPolyline, item.cx, item.cy);
        });
    }

    /**
     * Перебор точек полилинии с конца
     * @param polyline
     */
    function sortPointsForNewPolylineFromEnd(polyline) {
        for (var i = polyline.points.length - 1; i >=0; --i) {
            handlerPolyline.addPointInPolyline(newPolyline, polyline.points[i].cx, polyline.points[i].cy);
        }
    }

    for (key in selectedPolylines) {
        EDITOR.removeObject(selectedPolylines[key]);
    }

    EDITOR.draw(newPolyline);
    gcad.selectedObjectsStore.add(newPolyline);
    EDITOR.resetGcadSelections();

    callback();
};
