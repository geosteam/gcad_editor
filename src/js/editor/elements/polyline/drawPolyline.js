/**
 * Рисуем полилинию
 * @param callback
 */
EDITOR.enabledDrawPolyline = function (callback) {
    var polyline = new GCAD.Polyline();
    var self = this;
    var gcad = self.gcad;
    var handlers = self.handlersStore;
    var snap = self.snap;

    key('esc', function() {
        callback();
        self.enabledContextMenu();
        self.setHandlersDefault();
        self.zoomOn();
        key.unbind('esc');
    });

    polyline.layerID = gcad.defaultLayer.id;
    polyline.id = EDITOR.getUniqueId();

    self.applyStyleFromGlobal(polyline);

    self.disabledContextMenu();
    self.zoomOff();
    self.resetGcadSelections();

    gcad.selectedObjectsStore.add(polyline);

    gcad.element.on('mouseover', null);
    gcad.element.on('mouseout', null);

    if (snap.enabledSnapPoint || snap.enabledSnapLine) {
        gcad.element.on('mousemove', function() {
            self.snapMousemove();
        });
    }

    gcad.element.on('mousedown', function () {
        key.unbind('esc');

        var mousedownX = self.helpers.getMouseCoordinate()[0];
        var mousedownY = self.helpers.getMouseCoordinate()[1];
        
        handlers.polyline.addPointInPolyline(
            polyline,
            mousedownX,
            mousedownY
        );

        if (polyline.getAmountOfPoints() === 1) {
            handlers.polyline.addPointInPolyline(
                polyline,
                mousedownX,
                mousedownY
            );
            drawLastEdge(polyline.points[0]);
        }

        var lastPointID = polyline.getAmountOfPoints() - 1;
        var lastEdgeID = polyline.getAmountOfEdges() - 1;

        snap.excludePoints = [];
        snap.excludePoints.push({id: lastPointID, ownerId: polyline.points[lastPointID].ownerID});

        polyline.edges[lastEdgeID].strokeWidth = polyline.style.edge.strokeWidth;
        polyline.edges[lastEdgeID].opacity = polyline.style.edge.opacity;
        polyline.edges[lastEdgeID].strokeDasharray = polyline.style.edge.strokeDasharray;

        drawLastEdge(polyline.points[lastPointID], polyline.edges[lastEdgeID]);

        /**
         * TODO: сбрасываем snap.excludeLines чтобы не добавлялись предыдущие сегменты, возможно в
         * будущем это вызовет проблемы
         */
        snap.excludeLines = [];
        snap.excludeLines.push({id: lastEdgeID, ownerId: polyline.edges[polyline.getAmountOfEdges() - 1].ownerID});

        gcad.element.on('mousemove', null);

        gcad.element.on('mousemove', function () {
          
            self.snapMousemove();

            var moveX = self.helpers.getMouseCoordinate()[0];
            var moveY = self.helpers.getMouseCoordinate()[1];

            polyline.points[lastPointID].cx = moveX;
            polyline.points[lastPointID].cy = moveY;

            polyline.edges[lastEdgeID].coordinates.close.x = moveX;
            polyline.edges[lastEdgeID].coordinates.close.y = moveY;

            self.redraw(polyline.edges[lastEdgeID]);
            self.redraw(polyline.points[lastPointID]);
            self.redraw(polyline.points[lastPointID - 1]);
        });

        function drawLastEdge(point, edge) {
            self.draw(point, gcad.defaultLayer.id);
            if (edge) {
                self.draw(edge, gcad.defaultLayer.id);
            }
        }

        d3.select("body").on('keydown', function () {
            if (d3.event.keyCode !== 13) {
                return false;
            }

            gcad.element.on('mousemove', null);
            gcad.element.on('mouseup', null);

            self.handlersStore.polyline.deletePoint(polyline, polyline.points[lastPointID]);
            self.resetGcadSelections();
            self.enabledContextMenu();
            self.setHandlersDefault();
            self.setDefaultKeyBinds();
            self.zoomOn();

            self.resetSnap();
            d3.select("body").on('keydown', null);
            callback();

            gcad.UndoManager.add({
                undo: function() {
                    self.removeObject(polyline);
                },
                redo: function() {
                    self.draw(polyline);
                }
            });
        });
    });
};
