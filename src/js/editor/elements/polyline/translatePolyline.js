/**
 * перемещение полилинии
 * @param polyline
 * @param offsetX Смещение по x
 * @param offsetY Смещение по y
 */
EDITOR.translatePolyline = function(polyline, offsetX, offsetY) {
    var points = polyline.points;
    var edges = polyline.edges;

    points.forEach(function(item) {
        item.cx = item.cx + offsetX;
        item.cy = item.cy + offsetY;
    });

    edges.forEach(function(item) {
        item.coordinates = {
            begin: {
                x: item.coordinates.begin.x + offsetX,
                y: item.coordinates.begin.y + offsetY
            },
            close: {
                x: item.coordinates.close.x + offsetX,
                y: item.coordinates.close.y + offsetY
            }
        }
    });
};