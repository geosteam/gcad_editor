/**
 * Объединяем два сплайна
 * @param callback
 */
EDITOR.mergeSplines = function(callback) {
    var gcad = EDITOR.gcad;
    var selectedSplines = gcad.selectedObjectsStore.splines;
    var newSpline = new GCAD.Spline();
    var newPath = new GCAD.Path();
    var handlerSpline = EDITOR.handlersStore.spline;

    newSpline.layerID = EDITOR.gcad.defaultLayer.id;
    newSpline.id = EDITOR.getUniqueId();

    EDITOR.applyStyleFromGlobal(newSpline);

    newPath.color = newSpline.color;
    newPath.strokeWidth = newSpline.strokeWidth;
    newPath.strokeDasharray = newSpline.strokeDasharray;
    newPath.fill = newSpline.fill;
    newPath.owner = 'spline';
    newPath.ownerID = newSpline.id;
    newPath.id = EDITOR.getUniqueId();

    newSpline.path = newPath;

    /**
     * Блок проверок
     */
    if (Object.keys(selectedSplines) < 2 || Object.keys(selectedSplines) > 2) {
        console.warn('Можно объединить только два сплайна...');
        return false;
    }

    for (var key in selectedSplines) {
        if (selectedSplines[key].selectedPoint) {
            if (selectedSplines[key].selectedPoint.number !== 0) {
                if (selectedSplines[key].selectedPoint.number !== Object.keys(selectedSplines[key].points).length - 1) {
                    console.warn('Можно выбирать только крайние точки...');
                    return false;
                }
            }
        } else {
            console.warn('Не выбраны точки соединения... ');
            return false;
        }
    }

    callback();

    var firstIteration = true;

    for (key in selectedSplines) {
        if (selectedSplines[key].selectedPoint.number === 0) {
            if (firstIteration) {
                sortPointsForSplineFromEnd(selectedSplines[key]);
            } else {
                sortPointsForSplineAtFirst(selectedSplines[key]);
            }
        } else {
            if (firstIteration) {
                sortPointsForSplineAtFirst(selectedSplines[key]);
            } else {
                sortPointsForSplineFromEnd(selectedSplines[key]);
            }
        }
        firstIteration = false;
    }

    /**
     * Перебор точек сплайна с начала
     * @param spline
     */
    function sortPointsForSplineAtFirst(spline) {
        for (var key in spline.points) {
            handlerSpline.addPointInSpline(newSpline, spline.points[key].pointSpline.cx, spline.points[key].pointSpline.cy);
        }
    }

    /**
     * Перебор точек сплайна с конца
     * @param spline
     */
    function sortPointsForSplineFromEnd(spline) {
        var pointsArray = [];
        for (var key in spline.points) {
            pointsArray.push(spline.points[key].pointSpline);
        }
        for (var i = pointsArray.length - 1; i >=0; --i) {
            handlerSpline.addPointInSpline(newSpline, pointsArray[i].cx, pointsArray[i].cy);
        }
    }

    for (key in selectedSplines) {
        EDITOR.removeObject(selectedSplines[key]);
    }

    EDITOR.draw(newSpline);
    gcad.selectedObjectsStore.add(newSpline);
    EDITOR.handlersStore.spline.setMouseMoveEvent(newSpline);
    EDITOR.resetGcadSelections();

    callback();
};
