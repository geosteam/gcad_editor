/**
 * Рисовальщик для сплайна
 */
EDITOR.splinePainter = {
    draw: function(spline, layerID) {
        spline.path.color = spline.color;
        spline.path.strokeWidth = spline.strokeWidth;
        spline.path.strokeDasharray = spline.strokeDasharray;
        spline.path.fill = spline.fill;

        spline.path.arrayOfPoints = [];

        for (var keyPointPath in spline.points) {
            spline.path.arrayOfPoints.push(spline.points[keyPointPath].pointPath);
        }

        EDITOR.pathPainter.draw(spline.path, layerID);

        for (var keyPoint in spline.points) {
            EDITOR.pointPainter.draw(spline.points[keyPoint].pointSpline, layerID);
        }
    },
    remove: function(spline) {
        EDITOR.pathPainter.remove(spline.path);

        for (var key in spline.points) {
            EDITOR.pointPainter.remove(spline.points[key].pointSpline);
        }
    },
    redraw: function(spline) {
        this.remove(spline);
        this.draw(spline, spline.layerID);
    }
};
