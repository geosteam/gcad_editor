/**
 * Замкнуть сплайн
 * @param splineId
 */
EDITOR.handlersStore.spline.close = function(splineId) {
    var spline = EDITOR.gcad.store.splines[splineId];
   
    EDITOR.removeObject(spline);
    
    spline.path.setInterpolate('cardinal-closed');
    spline.isClose = true;
    
    EDITOR.draw(spline);
    EDITOR.gcad.selectedObjectsStore.add(spline);
    EDITOR.handlersStore.spline.setMouseMoveEvent(spline);
};
