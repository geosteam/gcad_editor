/**
 * Сбросить выделение сплайна
 * @param spline
 */
EDITOR.handlersStore.spline.resetSelect = function(spline) {
    var gcad  = EDITOR.gcad;

    spline.color = GCAD.Style.spline.color;

    if (spline.selectedPoint) {
        spline.selectedPoint.color = GCAD.Style.point.color;
        EDITOR.pointPainter.redraw(spline.selectedPoint);
        spline.selectedPoint.isSelected = false;
        spline.selectedPoint = null;
    }

    for (var key in spline.points) {
        if (spline.points[key].pointSpline.hide === true) {
            return;
        }
        EDITOR.pointPainter.hidePoint(spline.points[key].pointSpline);
        //spline.points[key].pointSpline.isSelected = false;
    }

    EDITOR.applyStyleFromGlobal(spline);
    EDITOR.redraw(spline);

    delete gcad.selectedObjectsStore.splines[spline.id];
    spline.isSelected = false;
    EDITOR.handlersStore.spline.setMouseMoveEvent(spline);
};

