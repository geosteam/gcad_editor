/**
 * Клик по точке сплайна
 * @param splineID
 * @param pointID
 */
EDITOR.handlersStore.spline.getPointEvent = function(splineID, pointID) {
    var gcad = EDITOR.gcad;
    var spline = gcad.store.splines[splineID];
    var point = spline.points[pointID].pointSpline;

    EDITOR.zoomOff();
    EDITOR.gcad.element.style('cursor', 'cell');
    point.element.style('cursor', 'cell');

    gcad.element.on('mousemove', function() {
        var mouseX = EDITOR.helpers.getMouseCoordinate()[0];
        var mouseY = EDITOR.helpers.getMouseCoordinate()[1];

        point.cx = mouseX;
        point.cy = mouseY;

        spline.points[pointID].pointPath.x = mouseX;
        spline.points[pointID].pointPath.y = mouseY;

        EDITOR.redraw(spline);

        gcad.element.on('mousedown', function () {
            gcad.element.on('mousedown', null);
            gcad.element.on('mousemove', null);
            EDITOR.setHandlersDefault();
            EDITOR.handlersStore.spline.setMouseMoveEvent(spline);
            EDITOR.gcad.element.style('cursor', 'default');
            point.element.style('cursor', 'default');
            EDITOR.zoomOn();
        });
    });
};
