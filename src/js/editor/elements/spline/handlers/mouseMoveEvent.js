/**
 * Событие срабатывает при наведении на сплайн
 * @param spline
 */
EDITOR.handlersStore.spline.setMouseMoveEvent = function(spline) {
    var handlers = EDITOR.handlersStore;
    
    handlers.setMouseoverLine(spline.path, spline.mouseoverColor);
    handlers.setMouseoutLine(spline.path, spline.selectedColor);

    for (var key in spline.points) {
        var point = spline.points[key].pointSpline;

        handlers.setMouseoverPoint(point, GCAD.Style.point.mouseoverColor);

        if (point.isSelected) {
            handlers.setMouseoutPoint(point, GCAD.Style.point.selectedColor);
        } else {
            handlers.setMouseoutPoint(point, GCAD.Style.point.color);
        }
    }
};
