/**
 * Разомкнуть сплайн
 * @param splineId
 */
EDITOR.handlersStore.spline.open = function(splineId) {
    var spline = EDITOR.gcad.store.splines[splineId];

    EDITOR.removeObject(spline);

    spline.path.setInterpolate('cardinal');
    spline.isClose = false;

    EDITOR.draw(spline);
    EDITOR.gcad.selectedObjectsStore.add(spline);
    EDITOR.handlersStore.spline.setMouseMoveEvent(spline);
};