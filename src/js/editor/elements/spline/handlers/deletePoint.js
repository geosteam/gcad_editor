/**
 * Удалить точку сплайна
 * @param splineId
 * @param pointId
 */
EDITOR.handlersStore.spline.deletePoint = function(splineId, pointId) {
    var spline = EDITOR.gcad.store.splines[splineId];

    EDITOR.removeObject(spline);

    if (spline.selectedPoint) {
        spline.selectedPoint = null;
    }

    delete spline.points[pointId];

    EDITOR.draw(spline);
    EDITOR.gcad.selectedObjectsStore.add(spline);
    EDITOR.handlersStore.spline.setMouseMoveEvent(spline);
};
