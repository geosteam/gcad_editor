/**
 * Клик по сплайну
 * @param splineID
 */
EDITOR.handlersStore.spline.getPathEvent = function(splineID) {
    var translateValueX = null;
    var translateValueY = null;
    var scaleValue = null;
    var gcad = EDITOR.gcad;
    var spline = gcad.store.splines[splineID];
    var pseudoSpline = new GCAD.Path();
    var basePointX = 0;
    var basePointY = 0;
    var offsetX = 0;
    var offsetY = 0;
    var pseudoSplinePainted = false;
    var arrayPointsOfSpline = [];

    pseudoSpline.layerID = gcad.defaultLayer.id;
    EDITOR.applyStyleFromGlobal(pseudoSpline);
    pseudoSpline.strokeDasharray = 5;


    for (var key in spline.points) {
        var point = spline.points[key];
        var pointForPath = {x: point.pointPath.x, y: point.pointPath.y};
        arrayPointsOfSpline.push(pointForPath);
    }

    gcad.element.on('mousemove', null);
    gcad.element.on('mousedown', null);
    gcad.element.on('mouseup', null);

    EDITOR.zoomOff();

    gcad.element.on('mousemove', function() {
        translateValueX = gcad.getTranslateValueX();
        translateValueY = gcad.getTranslateValueY();
        scaleValue = gcad.getScaleValue();

        var moveX = d3.mouse(gcad.element.node())[0] / scaleValue - translateValueX / scaleValue;
        var moveY = d3.mouse(gcad.element.node())[1] / scaleValue - translateValueY / scaleValue;

        offsetX = (basePointX !== 0 && basePointY !== 0) ? moveX - basePointX : 0;
        offsetY = (basePointX !== 0 && basePointY !== 0) ? moveY - basePointY : 0;

        for (var i = 0, l = arrayPointsOfSpline.length; i < l; i++) {
            arrayPointsOfSpline[i].x = arrayPointsOfSpline[i].x + offsetX;
            arrayPointsOfSpline[i].y = arrayPointsOfSpline[i].y + offsetY;
        }

        pseudoSpline.arrayOfPoints = arrayPointsOfSpline;

        basePointX = moveX;
        basePointY = moveY;

        if (!pseudoSplinePainted) {
            EDITOR.draw(pseudoSpline);
        } else {
            EDITOR.redraw(pseudoSpline);
        }

        pseudoSplinePainted = true;
    });

    gcad.element.on('mouseup', function() {
        if (pseudoSplinePainted) {
            var newSpline = new GCAD.Spline();
            var newPath = new GCAD.Path();

            newSpline.id = EDITOR.getUniqueId();
            newSpline.layerID = spline.layerID;

            EDITOR.applyStyleFromGlobal(newSpline);
            EDITOR.removeObject(pseudoSpline);
            EDITOR.removeObject(spline);

            newPath.color = newSpline.color;
            newPath.strokeWidth = newSpline.strokeWidth;
            newPath.strokeDasharray = newSpline.strokeDasharray;
            newPath.fill = newSpline.fill;
            newPath.owner = 'spline';
            newPath.ownerID = newSpline.id;
            newPath.id = EDITOR.getUniqueId();

            newSpline.path = newPath;

            for (var i = 0, l = pseudoSpline.arrayOfPoints.length; i < l; i++) {
                EDITOR.handlersStore.spline.addPointInSpline(
                    newSpline,
                    pseudoSpline.arrayOfPoints[i].x,
                    pseudoSpline.arrayOfPoints[i].y
                );
            }

            EDITOR.draw(newSpline);
            EDITOR.handlersStore.spline.setSelect(newSpline);

            if (spline.isClose) {
                EDITOR.handlersStore.spline.close(newSpline);
                newSpline.isClose = true;
            }
        }

        gcad.element.on('mousemove', null);
        gcad.element.on('mousedown', null);
        gcad.element.on('mouseup', null);

        EDITOR.setHandlersDefault();
        EDITOR.zoomOn();
    });
};
