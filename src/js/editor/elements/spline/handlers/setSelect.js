/**
 * Выделить сплайн
 * @param spline
 */
EDITOR.handlersStore.spline.setSelect = function(spline) {
    var gcad = EDITOR.gcad;

    spline.color = GCAD.Style.spline.selectedColor;

    for (var key in spline.points) {
        if (spline.points[key].pointSpline.hide === false) {
            continue;
        }
        EDITOR.pointPainter.showPoint(spline.points[key].pointSpline);
    }

    EDITOR.redraw(spline);
    gcad.selectedObjectsStore.add(spline);
    spline.isSelected = true;
    EDITOR.handlersStore.spline.setMouseMoveEvent(spline);
};

