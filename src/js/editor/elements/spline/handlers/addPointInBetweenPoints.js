/**
 * Добавить точку в произвольное место сплайна
 * @param spline Исходный сплай
 * @param point Точка перед которой будет добавлена новая точка
 * @param x Координата x новой точки
 * @param y Координата y новой точки
 */
EDITOR.handlersStore.spline.addPointInSplineBetweenPoints = function(spline, point, x, y) {
    var newPoint = new GCAD.Point(x, y);
    //var number = spline.getAmountOfPoints();
    var arrayOfPoints = [];

    newPoint.id = EDITOR.getUniqueId();
    newPoint.layerID = spline.layerID;
    newPoint.hide = false;
    newPoint.owner = 'spline';
    newPoint.ownerID = spline.id;
    EDITOR.applyStyleFromGlobal(newPoint);

    for (var key in spline.points) {
        arrayOfPoints.push(spline.points[key]);
    }

    EDITOR.splinePainter.remove(spline);

    spline.points = {};

    newPoint.number = getIndexOfPoint(point) + 1;

    arrayOfPoints.splice(getIndexOfPoint(point), 0, {
        pointSpline: newPoint,
        pointPath: {
            'x': x,
            'y': y
        }
    });

    arrayOfPoints.forEach(function(item, index) {
        item.pointSpline.number = index;
        spline.points[item.pointSpline.id] = item;
    });

    EDITOR.splinePainter.draw(spline, spline.layerID);

    EDITOR.handlersStore.spline.getPointEvent(spline.id, newPoint.id);

    function getIndexOfPoint(point) {
        var ind = null;

        arrayOfPoints.forEach(function(item, index) {
            if (item.pointSpline.id === point.pointSpline.id) {
                ind = index;
            }
        });

        return ind;
    }

    //TODO: Не доделано...

    console.log(spline.path.element.attr('d').split(/(\d)/));

    function parsePath(path) {
        var list = path.attr('d');
        var res = [];
        for(var i = 0; i < list.length; i++) {
            var cmd = list[i].pathSegTypeAsLetter;
            var sub = [];
            switch(cmd) {
                case "Q":
                    sub.unshift(list[i].y1); sub.unshift(list[i].x1);
                    break;
                case "M":
                    sub.push(list[i].x); sub.push(list[i].y);
                    break;
            }
            sub.unshift(cmd);
            res.push(sub);
        }
        return res;
    }

};
