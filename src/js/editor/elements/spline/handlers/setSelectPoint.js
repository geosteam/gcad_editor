/**
 * Выделить точку сплайна
 * @param spline
 * @param point
 */
EDITOR.handlersStore.spline.setSelectPoint = function(spline, point) {
    var pointPainter = EDITOR.pointPainter;

    if (spline.selectedPoint) {
        var selectPoint = spline.selectedPoint;

        selectPoint.isSelected = false;
        pointPainter.setColor(selectPoint, GCAD.Style.point.color);
        EDITOR.redraw(selectPoint);
    }

    spline.selectedPoint = point;

    pointPainter.setColor(point, GCAD.Style.point.selectedColor);
    EDITOR.redraw(point);
    point.isSelected = true;
};

