/**
 * Добавить точку в сплайн
 * @param spline
 * @param x Координата x новой точки
 * @param y Координата y новой точки
 */
EDITOR.handlersStore.spline.addPointInSpline = function(spline, x, y) {
    var newPoint = new GCAD.Point(x, y);
    var number = spline.getAmountOfPoints();
    
    if (number === 0) {
        newPoint.number = 0;
    } else {
        newPoint.number = number++;
    }
    
    newPoint.id = EDITOR.getUniqueId();
    newPoint.layerID = EDITOR.gcad.defaultLayer.id;
    newPoint.hide = true;
    EDITOR.applyStyleFromGlobal(newPoint);

    if (spline.element) {
        EDITOR.splinePainter.remove(spline);
    }
    
    spline.points[newPoint.id] = {
        pointSpline: newPoint,
        pointPath: {
            'x': x,
            'y': y
        }
    };
    
    newPoint.owner = 'spline';
    newPoint.ownerID = spline.id;
    spline.lastPointId = newPoint.id;

    EDITOR.splinePainter.draw(spline);
};
