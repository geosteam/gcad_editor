/**
 * Перемещение сплайна
 * @param spline
 * @param offsetX Смещение по x
 * @param offsetY Смещение по y
 */
EDITOR.translateSpline = function(spline, offsetX, offsetY) {
    var points = spline.points;

    for (var key in points) {
        points[key].pointSpline.cx += offsetX;
        points[key].pointSpline.cy += offsetY;

        points[key].pointPath.x += offsetX;
        points[key].pointPath.y += offsetY;
    }
};
