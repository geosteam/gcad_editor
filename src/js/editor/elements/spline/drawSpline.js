/**
 * Рисуем сплайн
 * @param callback
 */
EDITOR.enabledDrawSpline = function (callback) {
    var spline = new GCAD.Spline();
    var self = this;
    var gcad = self.gcad;
    var path = new GCAD.Path([]);
    var handlers = self.handlersStore;
    
    spline.id = self.getUniqueId();
    spline.layerID = gcad.defaultLayer.id;

    path.layerID = EDITOR.gcad.defaultLayer.id;
    
    self.applyStyleFromGlobal(spline);

    /**
     * Сплайн является обычным svg элементом path
     */

    path.color = spline.color;
    path.strokeWidth = spline.strokeWidth;
    path.strokeDasharray = spline.strokeDasharray;
    path.fill = spline.fill;
    path.owner = 'spline';
    path.ownerID = spline.id;
    path.id = self.getUniqueId();

    spline.path = path;

    self.disabledContextMenu();
    self.zoomOff();
    self.resetGcadSelections();
    gcad.selectedObjectsStore.add(spline);
    gcad.element.on('mouseover', null);
    gcad.element.on('mouseout', null);

    spline.layerID = self.gcad.defaultLayer.id;

    gcad.element.on('mousedown', function () {
        var mousedownX = self.helpers.getMouseCoordinate()[0];
        var mousedownY = self.helpers.getMouseCoordinate()[1];
        var lastPointID;

        if (spline.path.element) {
            self.removeObject(spline);
        }

        handlers.spline.addPointInSpline(
            spline,
            mousedownX,
            mousedownY
        );

        if (spline.getAmountOfPoints() === 1) {
            handlers.spline.addPointInSpline(
                spline,
                mousedownX,
                mousedownY
            );
        }

        lastPointID = spline.lastPointId;


        if (spline.element) {
            self.redraw(spline);
        } else {
            self.draw(spline);
        }

        gcad.element.on('mousemove', null);

        gcad.element.on('mousemove', function () {
            var moveX = self.helpers.getMouseCoordinate()[0];
            var moveY = self.helpers.getMouseCoordinate()[1];

            spline.points[lastPointID].pointSpline.cx = moveX;
            spline.points[lastPointID].pointSpline.cy = moveY;

            spline.points[lastPointID].pointPath.x = moveX;
            spline.points[lastPointID].pointPath.y = moveY;
            
            self.redraw(spline);

            d3.select("body").on('keydown', function () {
                if (d3.event.keyCode !== 13) {
                    return false;
                }

                gcad.element.on('mousemove', null);
                gcad.element.on('mouseup', null);

                handlers.spline.deletePoint(spline.id, spline.points[lastPointID].pointSpline.id);

                self.enabledContextMenu();
                self.setHandlersDefault();
                self.resetGcadSelections();
                self.setDefaultKeyBinds();
                self.zoomOn();

                //self.resetSnap();
                d3.select("body").on('keydown', null);
                callback();
            });
        });
    });
};
