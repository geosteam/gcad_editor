/**
 * Рисовальщик для прямоугольника
 */
EDITOR.rectPainter = {
    draw: function(rect, layerID) {
        /**
         * Определяем путь для прямоугольника. Путь это координаты
         * svg элемента polygon в виде строки
         * @type {string}
         */
        var path = rect.coordOfAngle.upperLeft.x + ',' + rect.coordOfAngle.upperLeft.y + ' ' +
            rect.coordOfAngle.upperRight.x + ',' + rect.coordOfAngle.upperRight.y + ' ' +
            rect.coordOfAngle.lowerRight.x + ',' + rect.coordOfAngle.lowerRight.y + ' ' +
            rect.coordOfAngle.lowerLeft.x + ',' + rect.coordOfAngle.lowerLeft.y;


        //Добавляем svg элемент polygon и задаем ему свойства из объекта Rect
        rect.element = d3.selectAll('#' + layerID).append('polygon').attr({
            points: path,
            'type': rect.type,
            'stroke-dasharray': rect.strokeDasharray,
            'stroke-width': rect.strokeWidth,
            'id': rect.id,
            'fill': rect.fill,
            'owner': rect.owner,
            'ownerID': rect.ownerID
        }).style({
            'stroke': rect.color,
            'opacity': rect.opacity
        });

        //Рисуем точки прямоугольника
        for (var key in rect.points) {
            if (rect.points.hasOwnProperty(key)) {
                if (rect.points[key]) {
                    EDITOR.pointPainter.draw(rect.points[key], layerID);
                }
            }
        }

        rect.layerID = layerID;
    },

    remove: function(rect) {
        for (var key in rect.points) {
            if (rect.points.hasOwnProperty(key)) {
                if (rect.points[key]) {
                    rect.points[key].element.remove();
                }
            }
        }
        rect.element.remove();
    },

    redraw: function(rect) {
        this.remove(rect);
        this.draw(rect, rect.layerID);
    }
};
