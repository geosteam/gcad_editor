/**
 * Рисуем прямоугольник
 * @param callback
 */
EDITOR.drawRect = function(callback) {
    var self = this;
    var gcad = self.gcad;
    var snap = self.snap;
    var rect = new GCAD.Rect();
    var mouseMoveX;
    var mouseMoveY;
    var mouseDownX;
    var mouseDownY;
    var enabledSnap = false;

    rect.id = GCAD.getUniqueId();
    rect.ownerID = rect.id;
    rect.layerID = EDITOR.gcad.defaultLayer.id;
    self.applyStyleFromGlobal(rect);
    self.zoomOff();

    if (snap.enabledSnapPoint || snap.enabledSnapLine) {
        gcad.element.on('mousemove', function() {
            self.snapMousemove();
            enabledSnap = true;
        });
    }

    gcad.element.on('mousedown', function() {

        // Отключаем наведение мыши при рисовании...
        gcad.element.on('mouseover', null);
        gcad.element.on('mouseout', null);

        mouseDownX = self.helpers.getMouseCoordinate()[0];
        mouseDownY = self.helpers.getMouseCoordinate()[1];

        rect.coordOfAngle.upperLeft = {
            x: mouseDownX,
            y: mouseDownY
        };

        gcad.element.on('mousemove', function() {
            if (enabledSnap) {
                self.snapMousemove();
            }

            mouseMoveX = self.helpers.getMouseCoordinate()[0];
            mouseMoveY = self.helpers.getMouseCoordinate()[1];
            
            rect.coordOfAngle.upperRight = {
                x: mouseMoveX,
                y: mouseDownY
            };
            rect.coordOfAngle.lowerLeft = {
                x: mouseDownX,
                y: mouseMoveY
            };
            rect.coordOfAngle.lowerRight = {
                x: mouseMoveX,
                y: mouseMoveY
            };

            if (rect.element) {
                EDITOR.redraw(rect);
            } else {
                EDITOR.draw(rect);
            }
            snap.excludeRectsId.push(rect.id);
        });

        gcad.element.on('mousedown', function() {

            /**
             * Если пользователь нажал на холст и сразу отпустил мышь то ничего не рисуем
             */
            if (mouseMoveX === 0 || mouseMoveY === 0) {
                gcad.element.on('mousemove', null);
                gcad.element.on('mousedown', null);
                self.setHandlersDefault();
                self.zoomOn();

                callback();

                return false;
            }

            rect.points.upperLeft = new GCAD.Point(mouseDownX, mouseDownY);
            rect.points.upperRight = new GCAD.Point(mouseMoveX, mouseDownY);
            rect.points.upperMiddle = new GCAD.Point((mouseMoveX + mouseDownX) / 2, mouseDownY);
            rect.points.left = new GCAD.Point(mouseDownX, (mouseMoveY + mouseDownY) / 2);
            rect.points.right = new GCAD.Point(mouseMoveX, (mouseMoveY + mouseDownY) / 2);
            rect.points.lowerLeft = new GCAD.Point(mouseDownX, mouseMoveY);
            rect.points.lowerRight = new GCAD.Point(mouseMoveX, mouseMoveY);
            rect.points.lowerMiddle = new GCAD.Point((mouseDownX + mouseMoveX) / 2, mouseMoveY);

            rect.points.upperLeft.positionOnRect = 'upperLeft';
            rect.points.upperRight.positionOnRect = 'upperRight';
            rect.points.upperMiddle.positionOnRect = 'upperMiddle';
            rect.points.left.positionOnRect = 'left';
            rect.points.right.positionOnRect = 'right';
            rect.points.lowerLeft.positionOnRect = 'lowerLeft';
            rect.points.lowerRight.positionOnRect = 'lowerRight';
            rect.points.lowerMiddle.positionOnRect = 'lowerMiddle';

            for (var key in rect.points) {
                rect.points[key].id = EDITOR.getUniqueId();
                rect.points[key].owner = 'rect';
                rect.points[key].ownerID = rect.id;
                rect.points[key].layerID = EDITOR.gcad.defaultLayer.id;
                self.applyStyleFromGlobal(rect.points[key]);
                EDITOR.draw(rect.points[key]);
                rect.points[key].hide = true;
                EDITOR.redraw(rect.points[key]);
            }

            gcad.element.on('mousemove', null);
            gcad.element.on('mousedown', null);
            self.setHandlersDefault();
            self.resetSnap();
            self.zoomOn();

            gcad.UndoManager.add({
                undo: function() {
                    self.removeObject(rect);
                },
                redo: function() {
                    self.draw(rect);
                }
            });

            callback();
        });
    });

};
