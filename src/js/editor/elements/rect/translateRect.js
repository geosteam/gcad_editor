/**
 * Перемещение прямоугольника
 * @param rect
 * @param offsetX Смещение по x
 * @param offsetY Смещение по y
 */
EDITOR.translateRect = function(rect, offsetX, offsetY) {
    var points = rect.points;
    var coordOfAngle = rect.coordOfAngle;

    for (var key in points) {
        points[key].cx += offsetX;
        points[key].cy += offsetY;
    }

    for (key in coordOfAngle) {
        coordOfAngle[key].x += offsetX;
        coordOfAngle[key].y += offsetY;
    }
};