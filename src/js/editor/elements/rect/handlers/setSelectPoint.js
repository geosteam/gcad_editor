/**
 * Выделяем точку прямоугольника
 * @param rect
 * @param point
 */
EDITOR.handlersStore.rect.setSelectPoint = function(rect, point) {

    if (rect.selectedPointId) {
        EDITOR.handlersStore.rect.resetSelectPoint(rect, rect.selectedPointId);
    }

    rect.selectedPointId = point.id;

    EDITOR.pointPainter.setColor(point, GCAD.Style.point.selectedColor);
    EDITOR.redraw(point);
};
