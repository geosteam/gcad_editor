/**
 * Сбытие срабатывает при нажатии на сам прямоугольник
 * @param rectID
 */
EDITOR.handlersStore.rect.getLineEvent = function(rectID) {
    var translateValueX = null;
    var translateValueY = null;
    var scaleValue = null;
    var gcad = EDITOR.gcad;
    var rect = gcad.store.rectangles[rectID];
    var pseudoRect = new GCAD.Path();
    var basePointX = 0;
    var basePointY = 0;
    var offsetX = 0;
    var offsetY = 0;
    var pseudoRectPainted = false;
    var arrayPointsOfRect = [];

    EDITOR.zoomOff();

    pseudoRect.layerID = EDITOR.gcad.defaultLayer.id;
    EDITOR.applyStyleFromGlobal(pseudoRect);
    pseudoRect.strokeDasharray = 5;


    for (var key in rect.coordOfAngle) {
        if (rect.coordOfAngle.hasOwnProperty(key)) {
            var point = {x: rect.coordOfAngle[key].x, y: rect.coordOfAngle[key].y};
            arrayPointsOfRect.push(point);
        }
    }

    gcad.element.on('mouseover', null);

    arrayPointsOfRect.push({x: rect.coordOfAngle.upperLeft.x, y: rect.coordOfAngle.upperLeft.y});

    gcad.element.on('mousemove', function() {
        translateValueX = gcad.getTranslateValueX();
        translateValueY = gcad.getTranslateValueY();
        scaleValue = gcad.getScaleValue();

        var moveX = d3.mouse(gcad.element.node())[0] / scaleValue - translateValueX / scaleValue;
        var moveY = d3.mouse(gcad.element.node())[1] / scaleValue - translateValueY / scaleValue;

        offsetX = (basePointX !== 0 && basePointY !== 0) ? moveX - basePointX : 0;
        offsetY = (basePointX !== 0 && basePointY !== 0) ? moveY - basePointY : 0;

        for (var i = 0, l = arrayPointsOfRect.length; i < l; i++) {
            arrayPointsOfRect[i].x = arrayPointsOfRect[i].x + offsetX;
            arrayPointsOfRect[i].y = arrayPointsOfRect[i].y + offsetY;
        }

        for (var key in rect.points) {
            if (rect.points.hasOwnProperty(key)) {
                rect.points[key].cx += offsetX;
                rect.points[key].cy += offsetY;
            }
        }

        pseudoRect.arrayOfPoints = arrayPointsOfRect;

        basePointX = moveX;
        basePointY = moveY;

        if (!pseudoRectPainted) {
            EDITOR.draw(pseudoRect);
        } else {
            EDITOR.redraw(pseudoRect);
        }

        pseudoRectPainted = true;
    });

    gcad.element.on('mouseup', function() {
        gcad.element.on('mousemove', null);

        if (pseudoRectPainted) {
            EDITOR.removeObject(pseudoRect);

            rect.coordOfAngle.upperLeft.x = pseudoRect.arrayOfPoints[0].x;
            rect.coordOfAngle.upperLeft.y = pseudoRect.arrayOfPoints[0].y;
            rect.coordOfAngle.upperRight.x = pseudoRect.arrayOfPoints[1].x;
            rect.coordOfAngle.upperRight.y = pseudoRect.arrayOfPoints[1].y;
            rect.coordOfAngle.lowerRight.x = pseudoRect.arrayOfPoints[2].x;
            rect.coordOfAngle.lowerRight.y = pseudoRect.arrayOfPoints[2].y;
            rect.coordOfAngle.lowerLeft.x = pseudoRect.arrayOfPoints[3].x;
            rect.coordOfAngle.lowerLeft.y = pseudoRect.arrayOfPoints[3].y;

            EDITOR.redraw(rect);
        }

        gcad.element.on('mouseup', null);
        EDITOR.zoomOn();
        /*EDITOR.handlersStore.polyline.setMouseMoveEvent(polyline);*/
    });
};
