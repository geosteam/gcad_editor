/**
 * Выделяем прямоугольник
 * @param rect
 */
EDITOR.handlersStore.rect.setSelect = function(rect) {
    rect.color = rect.selectedColor;

    for (var key in rect.points) {
        EDITOR.pointPainter.showPoint(rect.points[key]);
    }

    rect.isSelected = true;

    EDITOR.redraw(rect);
    EDITOR.handlersStore.rect.setMouseMoveEvent(rect);
};
