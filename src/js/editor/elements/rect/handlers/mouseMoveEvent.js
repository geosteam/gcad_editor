/**
 * Событие срабатывает при наведени на прямоугольник
 * @param rect
 */
EDITOR.handlersStore.rect.setMouseMoveEvent = function(rect) {
    var rectPoints = rect.points;

    rect.element.on('mouseover', function() {
        rect.element.style('stroke', GCAD.Style.rect.mouseoverColor);
    });

    rect.element.on('mouseout', function() {
        rect.element.style('stroke', rect.color);
    });

    for (var key in rectPoints) {
        (function() {
            var i = key;

            rectPoints[i].element.on('mouseover', function() {
                rectPoints[i].element.style('fill', GCAD.Style.point.mouseoverColor);
            });
            rectPoints[i].element.on('mouseout', function() {
                rectPoints[i].element.style('fill', GCAD.Style.point.color);
            });
        })();
    }

};
