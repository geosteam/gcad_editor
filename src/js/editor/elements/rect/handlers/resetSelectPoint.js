/**
 * Сбрасываем выделение точки прямоугольника
 * @param rect
 * @param pointId
 */
EDITOR.handlersStore.rect.resetSelectPoint = function(rect, pointId) {
    var point;

    for (var key in rect.points) {
        if (rect.points[key].id === pointId) {
            point = rect.points[key];
        }
    }
    EDITOR.pointPainter.setColor(point, GCAD.Style.point.color);
    EDITOR.redraw(point);
};
