/**
 * Сбрасываем выделение прямоугольника
 * @param rect
 */
EDITOR.handlersStore.rect.resetSelect = function(rect) {
    if (rect.selectedPointId) {
        EDITOR.handlersStore.rect.resetSelectPoint(rect, rect.selectedPointId);
    }

    if (rect.newColor) {
        rect.color = rect.newColor;
    } else {
        EDITOR.applyStyleFromGlobal(rect);
    }

    for (var key in rect.points) {
        if (rect.points[key].hide) {
            return;
        }
        EDITOR.pointPainter.hidePoint(rect.points[key]);
    }

    rect.isSelected = false;
    EDITOR.redraw(rect);
    EDITOR.handlersStore.rect.setMouseMoveEvent(rect);
};
