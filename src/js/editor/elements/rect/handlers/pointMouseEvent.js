/**
 * Событие срабатывает при нажатии на точку прямоугольника
 * @param rectID
 * @param pointID
 */
EDITOR.handlersStore.rect.getPointEvent = function(rectID, pointID) {
    var gcad = EDITOR.gcad;
    var snap = EDITOR.snap;
    var rect = gcad.store.rectangles[rectID];
    var point = null;
    var offsetX = 0;
    var offsetY = 0;
    var pseudoRect = new GCAD.Rect();
    var enabledSnap = false;

    pseudoRect.pseudo = true;
    pseudoRect.layerID = EDITOR.gcad.defaultLayer.id;
    pseudoRect.color = '#EA9800';
    pseudoRect.strokeWidth = 1;
    pseudoRect.strokeDasharray = 5;
    pseudoRect.fill = 'none';
    pseudoRect.id = EDITOR.getUniqueId();

    gcad.element.on('mouseover', null);

    for (var key in rect.points) {
        if (rect.points[key].id === pointID) {
            point = rect.points[key];
        }
    }

    var baseMoveX = point.cx;
    var baseMoveY = point.cy;

    EDITOR.zoomOff();

    for (var key in rect.coordOfAngle) {
        pseudoRect.coordOfAngle[key] = rect.coordOfAngle[key];
    }

    EDITOR.draw(pseudoRect);

    if (snap.enabledSnapPoint || snap.enabledSnapLine) {
        enabledSnap = true;
    }

    gcad.element.on('mousemove', function() {
        if (enabledSnap) {
            snap.excludeRectsId.push(pseudoRect.id);
            snap.excludeRectsId.push(rect.id);
            EDITOR.snapMousemove();            
        }

        var moveX = EDITOR.helpers.getMouseCoordinate()[0];
        var moveY = EDITOR.helpers.getMouseCoordinate()[1];

        offsetX = moveX - baseMoveX;
        offsetY = moveY - baseMoveY;

        baseMoveX = moveX;
        baseMoveY = moveY;

        if (point.positionOnRect === 'upperLeft') {
            pseudoRect.coordOfAngle.upperLeft = {
                x: moveX,
                y: moveY
            };
        }

        if (point.positionOnRect === 'upperRight') {
            pseudoRect.coordOfAngle.upperRight = {
                x: moveX,
                y: moveY
            };
        }

        if (point.positionOnRect === 'lowerLeft') {
            pseudoRect.coordOfAngle.lowerLeft = {
                x: moveX,
                y: moveY
            };
        }

        if (point.positionOnRect === 'lowerRight') {
            pseudoRect.coordOfAngle.lowerRight = {
                x: moveX,
                y: moveY
            };
        }

        if (point.positionOnRect === 'upperMiddle') {
            pseudoRect.coordOfAngle.upperLeft.x += offsetX;
            pseudoRect.coordOfAngle.upperLeft.y += offsetY;
            pseudoRect.coordOfAngle.upperRight.x += offsetX;
            pseudoRect.coordOfAngle.upperRight.y += offsetY;
        }

        if (point.positionOnRect === 'lowerMiddle') {
            pseudoRect.coordOfAngle.lowerLeft.x += offsetX;
            pseudoRect.coordOfAngle.lowerLeft.y += offsetY;
            pseudoRect.coordOfAngle.lowerRight.x += offsetX;
            pseudoRect.coordOfAngle.lowerRight.y += offsetY;
        }

        if (point.positionOnRect === 'left') {
            pseudoRect.coordOfAngle.upperLeft.x += offsetX;
            pseudoRect.coordOfAngle.upperLeft.y += offsetY;
            pseudoRect.coordOfAngle.lowerLeft.x += offsetX;
            pseudoRect.coordOfAngle.lowerLeft.y += offsetY;
        }

        if (point.positionOnRect === 'right') {
            pseudoRect.coordOfAngle.upperRight.x += offsetX;
            pseudoRect.coordOfAngle.upperRight.y += offsetY;
            pseudoRect.coordOfAngle.lowerRight.x += offsetX;
            pseudoRect.coordOfAngle.lowerRight.y += offsetY;
        }

        EDITOR.redraw(pseudoRect);
    });

    gcad.element.on('mousedown', function () {
        var pseudoRectCoordOfAngle = pseudoRect.coordOfAngle;
        var rectCoordOfAngle = rect.coordOfAngle;

        for (var key in rect.coordOfAngle) {
            rectCoordOfAngle[key].x = pseudoRectCoordOfAngle[key].x;
            rectCoordOfAngle[key].y = pseudoRectCoordOfAngle[key].y;
        }

        rect.points.upperLeft.cx = rectCoordOfAngle.upperLeft.x;
        rect.points.upperLeft.cy = rectCoordOfAngle.upperLeft.y;

        rect.points.upperRight.cx = rectCoordOfAngle.upperRight.x;
        rect.points.upperRight.cy = rectCoordOfAngle.upperRight.y;

        rect.points.lowerLeft.cx = rectCoordOfAngle.lowerLeft.x;
        rect.points.lowerLeft.cy = rectCoordOfAngle.lowerLeft.y;

        rect.points.lowerRight.cx = rectCoordOfAngle.lowerRight.x;
        rect.points.lowerRight.cy = rectCoordOfAngle.lowerRight.y;

        rect.points.upperMiddle.cx = (pseudoRectCoordOfAngle.upperRight.x + pseudoRectCoordOfAngle.upperLeft.x) / 2;
        rect.points.upperMiddle.cy = (pseudoRectCoordOfAngle.upperRight.y + pseudoRectCoordOfAngle.upperLeft.y) / 2;

        rect.points.lowerMiddle.cx = (pseudoRectCoordOfAngle.lowerLeft.x + pseudoRectCoordOfAngle.lowerRight.x) / 2;
        rect.points.lowerMiddle.cy = (pseudoRectCoordOfAngle.lowerLeft.y + pseudoRectCoordOfAngle.lowerRight.y) / 2;

        rect.points.left.cx = (pseudoRectCoordOfAngle.lowerLeft.x + pseudoRectCoordOfAngle.upperLeft.x) / 2;
        rect.points.left.cy = (pseudoRectCoordOfAngle.lowerLeft.y + pseudoRectCoordOfAngle.upperLeft.y) / 2;

        rect.points.right.cx = (pseudoRectCoordOfAngle.lowerRight.x + pseudoRectCoordOfAngle.upperRight.x) / 2;
        rect.points.right.cy = (pseudoRectCoordOfAngle.lowerRight.y + pseudoRectCoordOfAngle.upperRight.y) / 2;

        EDITOR.removeObject(pseudoRect);

        EDITOR.redraw(rect);

        EDITOR.resetSnap();

        gcad.element.on('mousemove', null);
        EDITOR.setHandlersDefault();
        EDITOR.zoomOn();
    });
};
