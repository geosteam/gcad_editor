/**
 * Рисовальшик для линейки
 */
EDITOR.rulerPainter = {
    draw: function(ruler, layerID) {
        //Задаем свойства для трех линий. Свойства берем из объекта Ruler
        ruler.extensionLines.leftLine.strokeWidth = ruler.strokeWidth;
        ruler.extensionLines.rightLine.strokeWidth = ruler.strokeWidth;
        ruler.extensionLines.dimensionLine.strokeWidth = ruler.strokeWidth;

        ruler.extensionLines.leftLine.opacity = ruler.opacity;
        ruler.extensionLines.rightLine.opacity = ruler.opacity;
        ruler.extensionLines.dimensionLine.opacity = ruler.opacity;

        ruler.extensionLines.leftLine.strokeDasharray = ruler.strokeDasharray;
        ruler.extensionLines.rightLine.strokeDasharray = ruler.strokeDasharray;
        ruler.extensionLines.dimensionLine.strokeDasharray = ruler.strokeDasharray;

        ruler.extensionLines.leftLine.color = ruler.color;
        ruler.extensionLines.rightLine.color = ruler.color;
        ruler.extensionLines.dimensionLine.color = ruler.color;

        EDITOR.linePainter.draw(ruler.extensionLines.leftLine, layerID);
        EDITOR.linePainter.draw(ruler.extensionLines.rightLine, layerID);
        EDITOR.linePainter.draw(ruler.extensionLines.dimensionLine, layerID);
        EDITOR.textPainter.draw(ruler.label, layerID);

        //Стрелки
        ruler.extensionLines.dimensionLine.element.attr({
            'marker-end': 'url(#mark)',
            'marker-start': 'url(#mark-left)'
        });

        EDITOR.pointPainter.draw(ruler.dimensionPoint, layerID);

        ruler.extensionLines.leftLine.ownerID = ruler.id;
        ruler.extensionLines.rightLine.ownerID = ruler.id;
        ruler.extensionLines.dimensionLine.ownerID = ruler.id;
        ruler.dimensionPoint.ownerID = ruler.id;
        //handlers.extensionLines.dimensionLine.dimension = true;

        ruler.layerID = layerID;
    },

    redraw: function(ruler) {
        this.remove(ruler);
        this.draw(ruler, ruler.layerID);
    },

    remove: function(ruler) {
        ruler.extensionLines.leftLine.element.remove();
        ruler.extensionLines.rightLine.element.remove();
        ruler.extensionLines.dimensionLine.element.remove();
        ruler.dimensionPoint.element.remove();
        ruler.label.element.remove();
    }
};
