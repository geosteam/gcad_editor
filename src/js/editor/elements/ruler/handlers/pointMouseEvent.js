/**
 * Событие срабытывает при клике на точку линейки
 * @param ruler
 */
EDITOR.handlersStore.ruler.getRulerPointEvent = function(ruler) {
    var gcad  = EDITOR.gcad;
    var handlersOfRuler = EDITOR.handlersStore.ruler;

    EDITOR.zoomOff();

    gcad.element.on('mousemove', function() {
        var zoomValueX = gcad.getTranslateValueX();
        var zoomValueY = gcad.getTranslateValueY();
        var scaleValue = gcad.getScaleValue();
        var mouseMoveX = d3.mouse(gcad.element.node())[0] / scaleValue - zoomValueX / scaleValue;
        var mouseMoveY = d3.mouse(gcad.element.node())[1] / scaleValue - zoomValueY / scaleValue;

        if (ruler.position === 0) {
            handlersOfRuler.drawRulerVertical(ruler, mouseMoveX);
        }

        else if (ruler.position === 1) {
            handlersOfRuler.drawRulerHorizontal(ruler, mouseMoveY);
        }

        else if (ruler.position === 2) {
            redrawRuler();
        }

        else if (ruler.position === 4) {
            EDITOR.handlersStore.ruler.drawRulerParallel(ruler, mouseMoveX ,mouseMoveY);
        }

        function redrawRuler() {
            if ((mouseMoveY > ruler.pointLeft.y && mouseMoveY < ruler.pointRight.y) ||
                (mouseMoveY > ruler.pointRight.y && mouseMoveY < ruler.pointLeft.y)) {
                handlersOfRuler.drawRulerVertical(ruler, mouseMoveX);
            } else
            {
                handlersOfRuler.drawRulerHorizontal(ruler, mouseMoveY);
            }
        }
    });
    gcad.element.on('mousedown', function() {
        gcad.element.on('mousemove', null);
        EDITOR.setHandlersDefault();

        EDITOR.zoomOn();
    })
};
