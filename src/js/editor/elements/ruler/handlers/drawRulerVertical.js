/**
 * Рисуем вертикальную линейку
 * @param ruler
 * @param mouseX Смощение курсора по X относительно начала выностных линий
 */
EDITOR.handlersStore.ruler.drawRulerVertical = function(ruler, mouseX) {
    //noinspection JSUnresolvedFunction
    var labelSize = ruler.label.element.node().getBBox(); // Длина label
    var offsetArrow = 3; // отступ для стрелок
    var self = EDITOR;

    var leftLine = ruler.extensionLines.leftLine;
    var rightLine = ruler.extensionLines.rightLine;
    var dimensionLine = ruler.extensionLines.dimensionLine;

    ruler.width = self.helpers.getDistanceBetweenTheTwoPoints(dimensionLine.coordinates.begin.x, dimensionLine.coordinates.begin.y,
        dimensionLine.coordinates.begin.x, dimensionLine.coordinates.close.y);

    ruler.label.value = Math.ceil(ruler.width);

    /**
     * Координаты для выностных линий
     */
    leftLine.coordinates.close.y = ruler.pointLeft.y;
    rightLine.coordinates.close.y = ruler.pointRight.y;
    leftLine.coordinates.close.x = mouseX;
    rightLine.coordinates.close.x = mouseX;

    /**
     * Отступ выностных линий от размерной линии
     */
    if (mouseX > ruler.pointLeft.x && mouseX < ruler.pointRight.x) {
        leftLine.coordinates.close.x += ruler.offset;
        rightLine.coordinates.close.x -= ruler.offset;
    } else if (mouseX < ruler.pointLeft.x && mouseX > ruler.pointRight.x) {
        leftLine.coordinates.close.x -= ruler.offset;
        rightLine.coordinates.close.x += ruler.offset;
    } else  if (mouseX > ruler.pointLeft.x) {
        leftLine.coordinates.close.x += ruler.offset;
        rightLine.coordinates.close.x += ruler.offset;
    } else {
        leftLine.coordinates.close.x -= ruler.offset;
        rightLine.coordinates.close.x -= ruler.offset;
    }

    /**
     * Координаты для размерной линии
     */
    dimensionLine.coordinates.begin.x = mouseX;
    dimensionLine.coordinates.close.x = mouseX;
    dimensionLine.coordinates.begin.y = rightLine.coordinates.close.y;
    dimensionLine.coordinates.close.y = leftLine.coordinates.close.y;

    /*if (ruler.pointLeft.y > ruler.pointRight.y) {
        dimensionLine.coordinates.begin.y += offsetArrow;
        dimensionLine.coordinates.close.y -= offsetArrow;
    } else if (ruler.pointLeft.y < ruler.pointRight.y) {
        dimensionLine.coordinates.begin.y -= offsetArrow;
        dimensionLine.coordinates.close.y += offsetArrow;
    } else {
        dimensionLine.coordinates.begin.y -= offsetArrow;
        dimensionLine.coordinates.close.y += offsetArrow;
    }*/

    /**
     *  Координаты положения label
     */
    if (mouseX < ruler.pointLeft.x) {
        ruler.label.position.x = leftLine.coordinates.close.x;
    } else {
        ruler.label.position.x = leftLine.coordinates.close.x - labelSize.height;
    }
    ruler.label.position.y = (leftLine.coordinates.close.y + rightLine.coordinates.close.y) /
        2 + labelSize.width / 2;

    ruler.label.rotate = -90;

    /**
     * Координаты для точки изменения размера линейки
     */
    var dimensionLineBeginX = dimensionLine.coordinates.begin.x;
    var dimensionLineBeginY = dimensionLine.coordinates.begin.y;
    var dimensionLineCloseY = dimensionLine.coordinates.close.y;

    ruler.dimensionPoint.cx = dimensionLineBeginX;
    ruler.dimensionPoint.cy = (dimensionLineCloseY - dimensionLineBeginY) / 2 + dimensionLineBeginY;

    EDITOR.redraw(ruler);
};
