/**
 * Выделяем линейку
 * @param ruler
 */
EDITOR.handlersStore.ruler.setSelect = function(ruler) {
    ruler.color = GCAD.Style.ruler.selectedColor;

    EDITOR.pointPainter.showPoint(ruler.dimensionPoint);

    ruler.isSelected = true;
    EDITOR.redraw(ruler);
};
