/**
 * Рисуем параллельную линейку
 * @param ruler
 * @param mouseX Смощение курсора по X относительно начала выностных линий
 * @param mouseY Смощение курсора по Y относительно начала выностных линий
 */
EDITOR.handlersStore.ruler.drawRulerParallel = function(ruler, mouseX, mouseY) {
    //noinspection JSUnresolvedFunction
    var labelSize = ruler.label.element.node().getBBox();
    var offsetArrow = 3; // отступ для стрелок
    var self = EDITOR;

    var leftLine = ruler.extensionLines.leftLine;
    var rightLine = ruler.extensionLines.rightLine;
    var dimensionLine = ruler.extensionLines.dimensionLine;

    ruler.width = self.helpers.getDistanceBetweenTheTwoPoints(dimensionLine.coordinates.begin.x, dimensionLine.coordinates.begin.y,
        dimensionLine.coordinates.close.x, dimensionLine.coordinates.close.y);

    ruler.label.value = Math.ceil(ruler.width);

    /**
     * Координаты для выностных линий
     */
    rightLine.coordinates.close.x = getCoordinateRulerRightLine(ruler.pointLeft, ruler.pointRight, mouseX, mouseY)[0];
    rightLine.coordinates.close.y = getCoordinateRulerRightLine(ruler.pointLeft, ruler.pointRight, mouseX, mouseY)[1];

    leftLine.coordinates.close.x = getCoordinateRulerLeftLine(ruler.pointLeft, ruler.pointRight, rightLine.coordinates.close.x,
        rightLine.coordinates.close.y)[0];

    leftLine.coordinates.close.y = getCoordinateRulerLeftLine(ruler.pointLeft, ruler.pointRight, rightLine.coordinates.close.x,
        rightLine.coordinates.close.y)[1];

    /**
     * Координаты для размерной линии
     */
    dimensionLine.coordinates.begin.x = leftLine.coordinates.close.x;
    dimensionLine.coordinates.begin.y = leftLine.coordinates.close.y;

    dimensionLine.coordinates.close.x = rightLine.coordinates.close.x;
    dimensionLine.coordinates.close.y = rightLine.coordinates.close.y;


    /*if (ruler.pointLeft.x > ruler.pointRight.x) {
        ruler.label.rotate = -Math.abs(ruler.angle - 90);
    } else if (ruler.pointLeft.x < ruler.pointRight.x) {
        ruler.label.rotate = -Math.abs(ruler.angle - 90);
    }*/

    //handlers.label.rotate = -Math.abs(handlers.angle - 90);

    ruler.dimensionPoint.cx = (dimensionLine.coordinates.begin.x + dimensionLine.coordinates.close.x) / 2;
    ruler.dimensionPoint.cy = (dimensionLine.coordinates.begin.y + dimensionLine.coordinates.close.y) / 2;

    /**
     * Отступ выностных линий от размерной линии
     */
    leftLine.coordinates.close.x = getOffsetExtensionLine(leftLine)[0];
    rightLine.coordinates.close.x = getOffsetExtensionLine(rightLine)[0];

    leftLine.coordinates.close.y = getOffsetExtensionLine(leftLine)[1];
    rightLine.coordinates.close.y = getOffsetExtensionLine(rightLine)[1];

    function getOffsetExtensionLine(line) {
        var m = widthLine(line);
        var d = ruler.offset;
        var x = (d*(line.coordinates.close.x - line.coordinates.begin.x)) / m + line.coordinates.close.x;
        var y = (d*(line.coordinates.close.y - line.coordinates.begin.y)) / m + line.coordinates.close.y;

        return [x, y];
    }

    /**
     *  Координаты положения label
     */

    ruler.label.position.x = leftLine.coordinates.close.x + rightLine.coordinates.close.x;
    ruler.label.position.y = leftLine.coordinates.close.y + rightLine.coordinates.close.y;



    if (ruler.pointLeft.y < ruler.pointRight.y) {
        if (ruler.pointLeft.x < ruler.pointRight.x) { //==== сверху вниз \ =====

            ruler.label.rotate = Math.abs(ruler.angle - 90);

            ruler.label.position.x =  ruler.label.position.x / 2;
            ruler.label.position.y = ruler.label.position.y / 2;
            /*ruler.label.position.x -= labelSize.width / 2 * Math.cos((ruler.angle - 90) * Math.PI / 180);
            ruler.label.position.y -= labelSize.width / 2 * Math.cos((ruler.angle - 90) * Math.PI / 180);*/
        }
        if (ruler.pointLeft.x > ruler.pointRight.x) { //==== сверху вниз / =====
            ruler.label.rotate = -Math.abs(ruler.angle - 90);

            ruler.label.position.x =  ruler.label.position.x / 2;
            ruler.label.position.y = ruler.label.position.y / 2;
        }
    }
    if (ruler.pointRight.y < ruler.pointLeft.y) {
        if (ruler.pointLeft.x > ruler.pointRight.x) { //==== снизу вверх \ =====
            ruler.label.rotate = Math.abs(ruler.angle - 90);

            ruler.label.position.x =  ruler.label.position.x / 2;
            ruler.label.position.y = ruler.label.position.y / 2;
        }
        if (ruler.pointLeft.x < ruler.pointRight.x) { //==== снизу вверх / =====
            ruler.label.rotate = -Math.abs(ruler.angle - 90);

            ruler.label.position.x =  ruler.label.position.x / 2;
            ruler.label.position.y = ruler.label.position.y / 2;

        }
    }

    EDITOR.redraw(ruler);

    /**
     * Получить координаты для правой выностной линии
     * @param pointLeft Левая точка измеряемого отрезка
     * @param pointRight Правая точка измеряемого отрезка
     * @param mouseX Положение мыши по оси X
     * @param mouseY Положение мыши по оси Y
     * @returns {*[]} Массив к координатами [x, y]
     */
    function getCoordinateRulerRightLine(pointLeft, pointRight, mouseX, mouseY) {
        var x1 = pointLeft.x;
        var y1 = pointLeft.y;
        var x2 = pointRight.x;
        var y2 = pointRight.y;

        var d = y2 - y1;
        var c = x2 - x1;

        var t = c * x2 + d * y2;
        var k = c * mouseY - d * mouseX;

        var y = (d * t + c * k) / (c * c + d * d );
        var x = (t - d * y) / c;

        return [x, y];
    }

    /**
     * Получить координаты для левой выностной линии
     * @param pointLeft Левая точка измеряемого отрезка
     * @param pointRight Правая точка измеряемого отрезка
     * @param rightLineX Координата X конца правой выностной линии
     * @param rightLineY Координата Y конца правой выностной линии
     * @returns {*[]} Массив к координатами [x, y]
     */
    function getCoordinateRulerLeftLine(pointLeft, pointRight, rightLineX, rightLineY) {
        var x1 = pointLeft.x;
        var y1 = pointLeft.y;
        var x2 = pointRight.x;
        var y2 = pointRight.y;

        var x = x1 - (x2 - rightLineX);
        var y = y1 - (y2 - rightLineY);

        return [x, y];
    }

    function widthLine(line) {
        return Math.sqrt(Math.pow((line.coordinates.close.x - line.coordinates.begin.x), 2) +
            Math.pow((line.coordinates.close.y - line.coordinates.begin.y), 2));
    }
};
