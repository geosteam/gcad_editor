/**
 * Рисуем горизонтальную линейку
 * @param ruler
 * @param mouseY Смощение курсора по Y относительно начала выностных линий
 */
EDITOR.handlersStore.ruler.drawRulerHorizontal = function(ruler, mouseY) {
    //noinspection JSUnresolvedFunction
    var labelSize = ruler.label.element.node().getBBox(); // Размеры label
    var offsetArrow = 3; // отступ для стрелок
    var self = EDITOR;

    var leftLine = ruler.extensionLines.leftLine;
    var rightLine = ruler.extensionLines.rightLine;
    var dimensionLine = ruler.extensionLines.dimensionLine;

    ruler.width = self.helpers.getDistanceBetweenTheTwoPoints(dimensionLine.coordinates.begin.x, dimensionLine.coordinates.begin.y,
        dimensionLine.coordinates.close.x, dimensionLine.coordinates.begin.y);
    
    ruler.label.value = Math.ceil(ruler.width);

    /**
     * Координаты для выностных линий
     */
    leftLine.coordinates.close.x = ruler.pointLeft.x;
    leftLine.coordinates.close.y = mouseY;
    rightLine.coordinates.close.x = ruler.pointRight.x;
    rightLine.coordinates.close.y = mouseY;

    /**
     * Отступ выностных линий от размерной линии
     */
    if (mouseY > ruler.pointLeft.y && mouseY < ruler.pointRight.y) {
        leftLine.coordinates.close.y += ruler.offset;
        rightLine.coordinates.close.y -= ruler.offset;
    } else if (mouseY < ruler.pointLeft.y && mouseY > ruler.pointRight.y) {
        leftLine.coordinates.close.y += ruler.offset;
        rightLine.coordinates.close.y += ruler.offset;
    } else  if (mouseY > ruler.pointLeft.y) {
        leftLine.coordinates.close.y += ruler.offset;
        rightLine.coordinates.close.y += ruler.offset;
    } else {
        leftLine.coordinates.close.y -= ruler.offset;
        rightLine.coordinates.close.y -= ruler.offset;
    }

    /**
     * Координаты для размерной линии
     */
    dimensionLine.coordinates.begin.y = mouseY;
    dimensionLine.coordinates.close.y = mouseY;
    dimensionLine.coordinates.begin.x = leftLine.coordinates.close.x;
    dimensionLine.coordinates.close.x = rightLine.coordinates.close.x;


    /*if (ruler.pointLeft.x > ruler.pointRight.x) {
        dimensionLine.coordinates.begin.x -= offsetArrow;
        dimensionLine.coordinates.close.x += offsetArrow;
    } else if (ruler.pointLeft.x < ruler.pointRight.x) {
        dimensionLine.coordinates.begin.x += offsetArrow;
        dimensionLine.coordinates.close.x -= offsetArrow;
    } else {
        dimensionLine.coordinates.begin.x -= offsetArrow;
        dimensionLine.coordinates.close.x += offsetArrow;
    }*/

    /**
     *  Координаты положения label
     */
    ruler.label.position.x = (leftLine.coordinates.close.x + rightLine.coordinates.close.x) /
        2 - labelSize.width / 2;

    if (mouseY < leftLine.coordinates.begin.y) {
        ruler.label.position.y = leftLine.coordinates.close.y;
    }

    if (mouseY > leftLine.coordinates.begin.y) {
        ruler.label.position.y = leftLine.coordinates.close.y + labelSize.height / 2;
    }

    ruler.label.rotate = 0;

    /**
     * Координаты для точки изменения размера линейки
     */
    var dimensionLineBeginX = dimensionLine.coordinates.begin.x;
    var dimensionLineBeginY = dimensionLine.coordinates.begin.y;
    var dimensionLineCloseX = dimensionLine.coordinates.close.x;

    ruler.dimensionPoint.cx = (dimensionLineCloseX - dimensionLineBeginX) / 2 + dimensionLineBeginX;
    ruler.dimensionPoint.cy = dimensionLineBeginY;

    EDITOR.redraw(ruler);
};
