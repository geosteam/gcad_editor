/**
 * Редактирование текста
 * @param ruler
 */

//TODO: Не работает. Не удается отловить двойной клик по svg элементу
EDITOR.handlersStore.text.editTheTextRuler = function(ruler) {
    var handlers = EDITOR.handlersStore;

    /**
     * Вызывается после редактирования текста
     */
    var closeEditTheTextRuler = function() {
        /*var rulerLines = ruler.extensionLines;
        var angleRuler;*/
        handlers.ruler.resetSelect(ruler);

        //TODO: Нужно сделать автоматическое выравнивание текста
        /*var labelWidth = ruler.label.element.node().getBBox();

        if (rulerLines.leftLine.coordinates.begin.y - mouseMoveY < 20 && rulerLines.leftLine.coordinates.begin.y - mouseMoveY > -20) {
            angleRuler = 1; //горизонтально
        } else if (rulerLines.leftLine.coordinates.begin.x - mouseMoveX < 20 && rulerLines.leftLine.coordinates.begin.x - mouseMoveX > -20) {
            angleRuler = 0; //вертикально
        } else {
            angleRuler = 2; //под углом
        }

        if (angleRuler === 0) {
            ruler.label.position.x = rulerLines.leftLine.coordinates.close.x - labelWidth.height;
            ruler.label.position.y = (rulerLines.leftLine.coordinates.close.y + rulerLines.rightLine.coordinates.close.y) /
                2 + labelWidth.width / 2;
        }

        else if (angleRuler === 1) {
            ruler.label.position.x = (rulerLines.leftLine.coordinates.close.x + rulerLines.rightLine.coordinates.close.x) /
                2 - labelWidth.width / 2;
        }*/
    };

    handlers.ruler.setSelect(ruler);
    handlers.text.edit(ruler.label, closeEditTheTextRuler);

};
