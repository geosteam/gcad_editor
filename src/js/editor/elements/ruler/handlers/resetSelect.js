/**
 * Сбрасываем выделение линейки
 * @param ruler
 */
EDITOR.handlersStore.ruler.resetSelect = function(ruler) {
    EDITOR.applyStyleFromGlobal(ruler);
    EDITOR.pointPainter.hidePoint(ruler.dimensionPoint);

    ruler.isSelected = false;

    EDITOR.redraw(ruler);
};
