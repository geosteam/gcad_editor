/**
 * Рисуем линейку
 *
 * @param options Опция с помощью которой определяем какую линейку будем рисовать
 * если указано {parallel: true} значит рисуем паралелльную линейку, если ничего
 * не указано то обычную
 *
 * @param callback
 */
EDITOR.drawRuler = function(options, callback) {
    var self = this,
        gcad = self.gcad,
        snap = self.snap,
        ruler = new GCAD.Ruler(),
        baseLine = new GCAD.Line(), // Базовая линия
        angleRuler = 0, // Угол расположения линейки
        lineForRuler = new GCAD.Line(), // Размерная линия
        leftLineForRuler = new GCAD.Line(), // Левая выностная линия
        rightLineForRuler = new GCAD.Line(), // Правая выностная линия
        labelForRuler = new GCAD.Text(),
        point = new GCAD.Point(),
        mouseDownX,
        mouseDownY,
        mouseMoveX,
        mouseMoveY;

    /**
     * Длина линии
     * @param {object} line Обьект линии
     * @returns {number} Длина линии
     */
    function widthLine(line) {
        return Math.sqrt(Math.pow((line.coordinates.close.x - line.coordinates.begin.x), 2) +
            Math.pow((line.coordinates.close.y - line.coordinates.begin.y), 2));
    }

    function angleLine(line) {
        var x = line.coordinates.begin.x;
        var y = line.coordinates.begin.y;
        var x1 = line.coordinates.close.x;
        var y1 = line.coordinates.close.y;

        return Math.acos((y1 - y) / Math.sqrt(Math.pow(x1 - x, 2) + Math.pow(y1 - y, 2))) *180/Math.PI;
    }

    self.zoomOff();

    ruler.layerID = gcad.defaultLayer.id;
    ruler.id = self.getUniqueId();

    baseLine.color = '#fff';
    baseLine.strokeWidth = '2';

    point.radius = GCAD.Style.ruler.pointRange;
    point.color = GCAD.Style.ruler.pointColor;
    point.hide = true;
    point.id = 'none';

    self.applyStyleFromGlobal(ruler);

    if (snap.enabledSnapPoint || snap.enabledSnapLine) {
        gcad.element.on('mousemove', function() {
            self.snapMousemove();
        });
    }

    gcad.element.on('mousedown', function() {
        // Первый клик: Рисуем базовую линию

        mouseDownX = self.helpers.getMouseCoordinate()[0];
        mouseDownY = self.helpers.getMouseCoordinate()[1];

        baseLine.coordinates = {
            begin: {
                x: mouseDownX,
                y: mouseDownY
            },
            close: {
                x: mouseDownX,
                y: mouseDownY
            }
        };

        //Левая точка измеряемого отрезка
        ruler.pointLeft.x = mouseDownX;
        ruler.pointLeft.y = mouseDownY;

        self.draw(baseLine);

        gcad.element.on('mousemove', function() {
            self.snapMousemove();

            //redraw базовой линии при перемещении мыши

            mouseMoveX = self.helpers.getMouseCoordinate()[0];
            mouseMoveY = self.helpers.getMouseCoordinate()[1];

            if (!snap.statusSnapPoint && !snap.statusSnapLine && !snap.statusSnapCircle) {
                if (baseLine.coordinates.begin.y - mouseMoveY < 20 && baseLine.coordinates.begin.y - mouseMoveY > -20) {
                    baseLine.coordinates.close.x = mouseMoveX;
                    baseLine.coordinates.close.y = baseLine.coordinates.begin.y;
                    angleRuler = 1; //горизонтально
                    ruler.position = 1;
                } else if (baseLine.coordinates.begin.x - mouseMoveX < 20 && baseLine.coordinates.begin.x - mouseMoveX > -20) {
                    baseLine.coordinates.close.x = baseLine.coordinates.begin.x;
                    baseLine.coordinates.close.y = mouseMoveY;
                    angleRuler = 0; //вертикально
                    ruler.position = 0;
                } else {
                    baseLine.coordinates.close.x = mouseMoveX;
                    baseLine.coordinates.close.y = mouseMoveY;
                    angleRuler = 2; //под углом
                    ruler.position = 2;
                }
            } else {
                baseLine.coordinates.close.x = mouseMoveX;
                baseLine.coordinates.close.y = mouseMoveY;
                angleRuler = 2; //под углом
                ruler.position = 2;
            }

            self.redraw(baseLine);
        });

        gcad.element.on('mousedown', function() {
            //Второй клик: Удаляем базовую линию. Рисуем линейку.

            mouseDownX = self.helpers.getMouseCoordinate()[0];
            mouseDownY = self.helpers.getMouseCoordinate()[1];

            leftLineForRuler.coordinates = { //левая выностная линия
                begin: {
                    x: baseLine.coordinates.begin.x,
                    y: baseLine.coordinates.begin.y
                },
                close: {
                    x: baseLine.coordinates.begin.x,
                    y: baseLine.coordinates.begin.y
                }
            };

            rightLineForRuler.coordinates = { // правая выностная линия
                begin: {
                    x: baseLine.coordinates.close.x,
                    y: baseLine.coordinates.close.y
                },
                close: {
                    x: baseLine.coordinates.close.x,
                    y: baseLine.coordinates.close.y
                }
            };

            ruler.extensionLines.leftLine = leftLineForRuler;
            ruler.extensionLines.rightLine = rightLineForRuler;
            ruler.extensionLines.dimensionLine = lineForRuler;
            ruler.dimensionPoint = point;

            var leftLine = ruler.extensionLines.leftLine;
            var rightLine = ruler.extensionLines.rightLine;
            var dimensionLine = ruler.extensionLines.dimensionLine;
            var pointOfRuler = ruler.dimensionPoint;

            leftLine.owner = 'ruler';
            rightLine.owner = 'ruler';
            dimensionLine.owner = 'ruler';
            pointOfRuler.owner = 'ruler';

            leftLine.ownerID = ruler.id;
            rightLine.ownerID = ruler.id;
            dimensionLine.ownerID = ruler.id;
            pointOfRuler.ownerID = ruler.id;

            leftLine.id = 'none';
            rightLine.id = 'none';
            dimensionLine.id = 'none';
            pointOfRuler.id = 'none';

            leftLine.color = ruler.color;
            rightLine.color = ruler.color;
            dimensionLine.color = ruler.color;

            leftLine.opacity = ruler.opacity;
            rightLine.opacity = ruler.opacity;
            dimensionLine.opacity = ruler.opacity;

            leftLine.strokeDasharray = ruler.strokeDasharray;
            rightLine.strokeDasharray = ruler.strokeDasharray;
            dimensionLine.strokeDasharray = ruler.strokeDasharray;

            leftLine.strokeWidth = ruler.strokeWidth;
            rightLine.strokeWidth = ruler.strokeWidth;
            dimensionLine.strokeWidth = ruler.strokeWidth;

            pointOfRuler.hide = true;

            ruler.addLabel(labelForRuler);

            //Правая точка измеряемого отрезка
            if (angleRuler === 1) {
                ruler.pointRight.x = mouseDownX;
                ruler.pointRight.y = ruler.pointLeft.y;
            } else if (angleRuler === 0) {
                ruler.pointRight.x = ruler.pointLeft.x;
                ruler.pointRight.y = mouseDownY;
            } else {
                ruler.pointRight.x = mouseDownX;
                ruler.pointRight.y = mouseDownY;
            }

            //ruler.width = widthLine(baseLine); //Длина измеряемого отрезка
            ruler.angle = angleLine(baseLine); //Угол поворота измеряемого отрезка

            /*if (options.parallel) {
                console.log(baseLine.coordinates.begin.x, baseLine.coordinates.begin.y, baseLine.coordinates.close.x, baseLine.coordinates.close.y);

                ruler.width = self.helpers.getDistanceBetweenTheTwoPoints(baseLine.coordinates.begin.x, baseLine.coordinates.begin.y,
                    baseLine.coordinates.close.x, baseLine.coordinates.close.y);
            } else {
                console.log(baseLine.coordinates.begin.x, baseLine.coordinates.begin.y, baseLine.coordinates.close.x, baseLine.coordinates.begin.y);
                /!*
                 * getDistanceBetweenTheTwoPoints: 2-й и 4-й аргументы одинаковые, потому что так надо...
                 * *!/
                ruler.width = self.helpers.getDistanceBetweenTheTwoPoints(baseLine.coordinates.begin.x, baseLine.coordinates.begin.y,
                    baseLine.coordinates.close.x, baseLine.coordinates.begin.y);
            }*/

            //labelForRuler.value = Math.ceil(ruler.width);
            //labelForRuler.layerID = self.gcad.defaultLayer.id;

            self.removeObject(baseLine);
            self.draw(ruler);

            gcad.element.on('mousemove', function() {
                //redraw линейки при перемещении мыши

                var zoomValueX = gcad.getTranslateValueX();
                var zoomValueY = gcad.getTranslateValueY();
                var scaleValue = gcad.getScaleValue();

                mouseMoveX = d3.mouse(gcad.element.node())[0] / scaleValue - zoomValueX / scaleValue;
                mouseMoveY = d3.mouse(gcad.element.node())[1] / scaleValue - zoomValueY / scaleValue;

                if (options.parallel) {
                    if (ruler.pointLeft.x === ruler.pointRight.x) {
                        self.handlersStore.ruler.drawRulerVertical(ruler, mouseMoveX);
                    } else if (ruler.pointLeft.y === ruler.pointRight.y) {
                        self.handlersStore.ruler.drawRulerHorizontal(ruler, mouseMoveY);
                    } else {
                        self.handlersStore.ruler.drawRulerParallel(ruler, mouseMoveX ,mouseMoveY);
                        ruler.position = 4;
                    }
                } else {
                    if (angleRuler === 0) { //если линейка располагается вертикально
                        self.handlersStore.ruler.drawRulerVertical(ruler, mouseMoveX);
                    }

                    else if (angleRuler === 1) { //если линейка располагается горизонтально
                        self.handlersStore.ruler.drawRulerHorizontal(ruler, mouseMoveY);
                    }

                    else if (angleRuler === 2) { //если линейка располагается под углом
                        redrawRuler();
                    }
                }

                function redrawRuler() {
                    if ((mouseMoveY > ruler.pointLeft.y && mouseMoveY < ruler.pointRight.y) ||
                        (mouseMoveY > ruler.pointRight.y && mouseMoveY < ruler.pointLeft.y)) {
                        self.handlersStore.ruler.drawRulerVertical(ruler, mouseMoveX);
                    } else {
                        self.handlersStore.ruler.drawRulerHorizontal(ruler, mouseMoveY);
                    }
                }
            });

            gcad.element.on('mousedown', function() {
                gcad.element.on('mousemove', null);
                gcad.element.on('mouseup', null);
                gcad.element.on('mousedown', null);
                self.setHandlersDefault();
                self.resetSnap();
                self.zoomOn();

                gcad.UndoManager.add({
                    undo: function() {
                        self.removeObject(ruler);
                    },
                    redo: function() {
                        self.draw(ruler);
                    }
                });

                callback();
            })
        });
    })
};
