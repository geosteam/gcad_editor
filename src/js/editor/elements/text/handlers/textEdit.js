/**
 * Редактирование текста
 * @param text
 * @param callback
 */
EDITOR.handlersStore.text.edit = function(text, callback) {
    EDITOR.zoomOff();

    var gcad = EDITOR.gcad;
    var inputValue;
    var container = d3.select(EDITOR.containerSelector);

    gcad.element.on('mousedown', null);

    var input = container.append('input').attr({
        'type': 'text',
        'class': 'handlers-handlers-edit'
    });

    input.style('left', d3.mouse(container.node())[0] + 'px')
         .style('top', d3.mouse(container.node())[1] + 'px')
         .style('position', 'absolute');

    text.element.style('fill', 'red');

    input.on('input', function() {
        inputValue = this.value;
    });

    gcad.element.on('click', function() {
        closeEditText();
    });

    container.on('keydown', function() {
        if (d3.event.keyCode === 13) {
            closeEditText();
        }
    });

    function closeEditText() {
        if (inputValue) {
            text.value = inputValue;
        }
        EDITOR.redraw(text);
        input.remove();
        text.element.style('fill', '#fff');
        gcad.element.on('click', null);
        EDITOR.setHandlersDefault();
        EDITOR.zoomOn();
        if (callback) {
            callback();
        }
    }
};
