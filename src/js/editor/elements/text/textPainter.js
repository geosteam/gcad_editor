/**
 * Рисовальщик текста
 */
EDITOR.textPainter = {
    draw: function(text, layerID) {
        text.element = d3.selectAll('#' + layerID).append('text').attr({
            'x': text.position.x,
            'y': text.position.y,
            'owner': text.owner,
            'ownerID': text.ownerID
        }).style({
            'fill': text.color,
            'font-size': text.size
        }).text(text.value);


        text.layerID = layerID;

        if (text.rotate) {
            text.element.attr('transform', 'rotate(' + text.rotate + ' ' + text.position.x + ',' +  text.position.y + ')')
        }
    },

    remove: function(text) {
        text.element.remove();
    },

    redraw: function(text) {
        this.remove(text);
        this.draw(text, text.layerID);
    }
};
