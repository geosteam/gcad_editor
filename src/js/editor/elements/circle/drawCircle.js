/**
 * Рисуем окружность
 */
EDITOR.drawCircle = function(callback) {
    var self = this;
    var gcad = self.gcad;
    var circle = new GCAD.Circle();
    var zoomValueX = gcad.getTranslateValueX();
    var zoomValueY = gcad.getTranslateValueY();
    var scaleValue = gcad.getScaleValue();
    var mouseDownX = 0;
    var mouseDownY = 0;
    var mouseMoveX = 0;
    var mouseMoveY = 0;

    circle.id = self.getUniqueId();
    circle.ownerID = circle.id;
    circle.layerID = gcad.defaultLayer.id;
    self.zoomOff();

    self.applyStyleFromGlobal(circle);

    gcad.element.on('mousedown', function() {
        // Отключаем наведение мыши при рисовании...
        gcad.element.on('mouseover', null);
        gcad.element.on('mouseout', null);

        mouseDownX = d3.mouse(gcad.element.node())[0] / scaleValue - zoomValueX / scaleValue;
        mouseDownY = d3.mouse(gcad.element.node())[1] / scaleValue - zoomValueY / scaleValue;

        circle.centerX = mouseDownX;
        circle.centerY = mouseDownY;

        gcad.element.on('mousemove', function() {
            mouseMoveX = d3.mouse(gcad.element.node())[0] / scaleValue - zoomValueX / scaleValue;
            mouseMoveY = d3.mouse(gcad.element.node())[1] / scaleValue - zoomValueY / scaleValue;

            circle.radius = getRadius(circle.centerX, circle.centerY, mouseMoveX, mouseMoveY);

            if (circle.element) {
                EDITOR.redraw(circle);
            } else {
                EDITOR.draw(circle);
            }
        });

        gcad.element.on('mousedown', function() {
            if (mouseMoveX === 0 || mouseMoveY === 0) {
                gcad.element.on('mousemove', null);
                gcad.element.on('mousedown', null);
                self.setHandlersDefault();
                self.zoomOn();

                callback();

                return false;
            }

            circle.points.right = new GCAD.Point(circle.radius + circle.centerX, circle.centerY);
            circle.points.upper = new GCAD.Point(circle.centerX, circle.radius + circle.centerY);
            circle.points.left = new GCAD.Point(-circle.radius + circle.centerX, circle.centerY);
            circle.points.lower = new GCAD.Point(circle.centerX, -circle.radius + circle.centerY);
            circle.points.center = new GCAD.Point(circle.centerX, circle.centerY);

            circle.points.center.center = true; // Это центральная точка
            circle.points.right.positionOfCircle = 'right';
            circle.points.upper.positionOfCircle = 'upper';
            circle.points.left.positionOfCircle = 'left';
            circle.points.lower.positionOfCircle = 'lower';

            circle.points.right.id = EDITOR.getUniqueId();
            circle.points.upper.id = EDITOR.getUniqueId();
            circle.points.left.id = EDITOR.getUniqueId();
            circle.points.lower.id = EDITOR.getUniqueId();
            circle.points.center.id = EDITOR.getUniqueId();

            for (var key in circle.points) {
                circle.points[key].owner = 'circle';
                circle.points[key].ownerID = circle.id;
                circle.points[key].layerID = self.gcad.defaultLayer.id;
                self.applyStyleFromGlobal(circle.points[key]);
                self.draw(circle.points[key]);
                circle.points[key].hide = true;
                self.redraw(circle.points[key]);
            }

            gcad.element.on('mousedown', null);
            gcad.element.on('mousemove', null);
            self.setHandlersDefault();
            self.zoomOn();

            gcad.UndoManager.add({
                undo: function() {
                    self.removeObject(circle);
                },
                redo: function() {
                    self.draw(circle);
                }
            });

            callback();
        });
    });

    function getRadius(x, y, x2, y2) {
        return Math.sqrt(Math.pow((x2 - x), 2) + Math.pow((y2 - y), 2));
    }
};
