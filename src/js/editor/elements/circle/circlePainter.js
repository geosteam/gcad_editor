/**
 * Рисовальшик для окружности
 */
EDITOR.circlePainter = {
    draw: function(circle, layerID) {
        //Добавляем svg элемент circle и задаем ему свойства из объекта Circle
        circle.element = d3.selectAll('#' + layerID).append('circle').attr({
            cx: circle.centerX,
            cy: circle.centerY,
            r: circle.radius,
            id: circle.id,
            owner: circle.owner,
            ownerID: circle.id,
            fill: circle.fill,
            type: 'circle'
        }).style({
            'opacity': circle.opacity,
            'stroke-width': circle.strokeWidth,
            stroke: circle.color,
            'stroke-dasharray': circle.strokeDasharray
        });

        //Рисуем точки окружности
        for (var key in circle.points) {
            if (circle.points.hasOwnProperty(key)) {
                if (circle.points[key]) {
                    EDITOR.pointPainter.draw(circle.points[key], layerID);
                }
            }
        }

        circle.layerID = layerID;
    },

    remove: function(circle) {
        for (var key in circle.points) {
            if (circle.points.hasOwnProperty(key)) {
                if (circle.points[key]) {
                    circle.points[key].element.remove();
                }
            }
        }
        circle.element.remove();
    },

    setColor: function(circle, color) {
        circle.element.style({
            stroke: color
        });
        circle.color = color;
    },

    redraw: function(circle) {
        this.remove(circle);
        this.draw(circle, circle.layerID);
    }
};
