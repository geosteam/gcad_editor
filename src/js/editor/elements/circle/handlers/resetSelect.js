/**
 * Сбрасываем выделение окружности
 */
EDITOR.handlersStore.circle.resetSelect = function(circle) {
    if (circle.selectedPointId) {
        EDITOR.handlersStore.circle.resetSelectPoint(circle, circle.selectedPointId);
    }

    EDITOR.applyStyleFromGlobal(circle);

    if (circle.newStrokeWidth) {
        circle.strokeWidth = circle.newStrokeWidth;
    }

    if (circle.newColor) {
        circle.color = circle.newColor;
    }

    for (var key in circle.points) {
        if (circle.points[key].hide) {
            return;
        }
        EDITOR.pointPainter.hidePoint(circle.points[key]);
    }

    circle.isSelected = false;
    EDITOR.redraw(circle);
};
