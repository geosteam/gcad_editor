/**
 * Сбрасываем выделение точки окружности
 * @param circle
 * @param pointId
 */
EDITOR.handlersStore.circle.resetSelectPoint = function(circle, pointId) {
    for (var key in circle.points) {
        if (circle.points[key].id === pointId) {
            EDITOR.pointPainter.setColor(circle.points[key], GCAD.Style.point.color);
        }
    }
};
