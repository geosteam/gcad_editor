/**
 * Событие срабатывает при нажатии на точку окружности кроме центральной
 * @param circleID
 */
EDITOR.handlersStore.circle.getPointEvent = function(circleID) {
    var translateValueX = null;
    var translateValueY = null;
    var scaleValue = null;
    var gcad = EDITOR.gcad;
    var circle = gcad.store.circles[circleID];
    var pseudoCircle = new GCAD.Circle();
    var pseudoCirclePainted = false;

    pseudoCircle.strokeDasharray = 5;
    pseudoCircle.color = GCAD.Style.path.color;
    pseudoCircle.strokeWidth = 1;
    pseudoCircle.layerID = gcad.defaultLayer.id;
    pseudoCircle.fill = 'none';

    pseudoCircle.radius = circle.radius;
    pseudoCircle.centerX = circle.centerX;
    pseudoCircle.centerY = circle.centerY;

    gcad.element.on('mouseover', null);

    EDITOR.zoomOff();

    gcad.element.on('mousemove', function() {
        translateValueX = gcad.getTranslateValueX();
        translateValueY = gcad.getTranslateValueY();
        scaleValue = gcad.getScaleValue();

        var moveX = d3.mouse(gcad.element.node())[0] / scaleValue - translateValueX / scaleValue;
        var moveY = d3.mouse(gcad.element.node())[1] / scaleValue - translateValueY / scaleValue;

        pseudoCircle.radius = EDITOR.helpers.getDistanceBetweenTheTwoPoints(pseudoCircle.centerX, pseudoCircle.centerY, moveX, moveY);

        if (!pseudoCirclePainted) {
            EDITOR.draw(pseudoCircle);
        } else {
            EDITOR.redraw(pseudoCircle);
        }

        pseudoCirclePainted = true;
    });

    gcad.element.on('mousedown', function() {
        gcad.element.on('mousemove', null);

        if (pseudoCirclePainted) {

            circle.radius = pseudoCircle.radius;

            circle.points.right.cx = circle.radius + circle.centerX;
            circle.points.right.cy = circle.centerY;

            circle.points.upper.cx = circle.centerX;
            circle.points.upper.cy = circle.radius + circle.centerY;

            circle.points.left.cx = -circle.radius + circle.centerX;
            circle.points.left.cy = circle.centerY;

            circle.points.lower.cx = circle.centerX;
            circle.points.lower.cy = -circle.radius + circle.centerY;


            EDITOR.removeObject(pseudoCircle);
            EDITOR.redraw(circle);
        }

        EDITOR.setHandlersDefault();
        EDITOR.handlersStore.circle.setMouseMoveEvent(circle);
        EDITOR.zoomOn();
    });
};
