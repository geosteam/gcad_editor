/**
 * Выделение точки окружности
 * @param circle
 * @param point
 */
EDITOR.handlersStore.circle.setSelectPoint = function(circle, point) {
    if (circle.selectedPointId) {
        EDITOR.handlersStore.circle.resetSelectPoint(circle, circle.selectedPointId);
    }

    circle.selectedPointId = point.id;
    EDITOR.pointPainter.setColor(point, GCAD.Style.point.selectedColor);
    EDITOR.redraw(point);
};
