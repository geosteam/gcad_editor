/**
 * Выделение окружности
 */
EDITOR.handlersStore.circle.setSelect = function(circle) {
    if (circle.selectedPoint) {
        EDITOR.circlePainter.setColor(circle.selectedPoint, GCAD.Style.point.color);
        EDITOR.redraw(circle.selectedPoint);
        circle.selectedPoint = null;
    }

    circle.color = circle.selectedColor;

    for (var key in circle.points) {
        EDITOR.pointPainter.showPoint(circle.points[key]);
    }

    circle.isSelected = true;

    EDITOR.redraw(circle);
    EDITOR.handlersStore.circle.setMouseMoveEvent(circle);
};
