/**
 * Сбытие которое срабатывает при наведении на окружность
 * @param circle
 */
EDITOR.handlersStore.circle.setMouseMoveEvent = function(circle) {
    var circlePoints = circle.points;

    circle.element.on('mouseover', function() {
        circle.element.style('stroke', GCAD.Style.circle.mouseoverColor);
    });

    circle.element.on('mouseout', function() {
        circle.element.style('stroke', circle.color);
    });

    for (var key in circlePoints) {
        (function () {
            var i = key;

            circlePoints[i].element.on('mouseover', function () {
                circlePoints[i].element.style('fill', GCAD.Style.point.mouseoverColor);
            });
            circlePoints[i].element.on('mouseout', function () {
                circlePoints[i].element.style('fill', GCAD.Style.point.color);
            });
        })();
    }

};

