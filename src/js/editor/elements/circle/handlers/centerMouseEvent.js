/**
 * Событие срабатывает при нажатии на центральную точку окружности
 * @param circleID
 */
EDITOR.handlersStore.circle.getCenterEvent = function(circleID) {
    var translateValueX = null;
    var translateValueY = null;
    var scaleValue = null;
    var gcad = EDITOR.gcad;
    var snap = EDITOR.snap;
    var circle = gcad.store.circles[circleID];
    var pseudoCircle = new GCAD.Circle();
    var pseudoCirclePainted = false;
    var arrayPointsOfCircle = {};

    EDITOR.zoomOff();

    pseudoCircle.strokeDasharray = 5;
    pseudoCircle.color = GCAD.Style.path.color;
    pseudoCircle.strokeWidth = 1;
    pseudoCircle.layerID = gcad.defaultLayer.id;
    pseudoCircle.fill = 'none';
    pseudoCircle.id = EDITOR.getUniqueId();

    for (var key in circle.points) {
        arrayPointsOfCircle[key] = circle.points[key];
    }

    pseudoCircle.radius = circle.radius;
    pseudoCircle.centerX = circle.centerX;
    pseudoCircle.centerY = circle.centerY;

    gcad.element.on('mouseover', null);


    //Добавляем псевдо-окружность и центральную точку в
    //исключения привязки, чтобы сама окружность не привязывалась
    //к самой себе
    snap.excludeCirclesId.push(pseudoCircle.id);
    snap.excludePoints.push({id: circle.points.center.id, ownerId: circle.points.center.ownerID});

    var moveX;
    var moveY;

    gcad.element.on('mousemove', function() {
        translateValueX = gcad.getTranslateValueX();
        translateValueY = gcad.getTranslateValueY();
        scaleValue = gcad.getScaleValue();

        if (snap.enabledSnapPoint || snap.enabledSnapLine) {
            EDITOR.snapMousemove();

            if (snap.snapPoint.selectionPoint) {
                moveX = snap.snapPoint.selectionPoint.cx;
                moveY = snap.snapPoint.selectionPoint.cy;
            } else if (snap.snapCircle.selectionPoint) {
                moveX = snap.snapCircle.selectionPoint.cx;
                moveY = snap.snapCircle.selectionPoint.cy;
            }  else if (snap.snapLine.selectionPoint) {
                moveX = snap.snapLine.selectionPoint.cx;
                moveY = snap.snapLine.selectionPoint.cy;
            } else {
                moveX = d3.mouse(gcad.element.node())[0] / scaleValue - translateValueX / scaleValue;
                moveY = d3.mouse(gcad.element.node())[1] / scaleValue - translateValueY / scaleValue;
            }
        } else {
            moveX = d3.mouse(gcad.element.node())[0] / scaleValue - translateValueX / scaleValue;
            moveY = d3.mouse(gcad.element.node())[1] / scaleValue - translateValueY / scaleValue;
        }

        pseudoCircle.centerX = moveX;
        pseudoCircle.centerY = moveY;

        for (var key in circle.points) {
            if (key === 'upper') {
                arrayPointsOfCircle[key].cx = moveX;
                arrayPointsOfCircle[key].cy = moveY - circle.radius;
            }
            if (key === 'left') {
                arrayPointsOfCircle[key].cx = moveX - circle.radius;
                arrayPointsOfCircle[key].cy = moveY;
            }
            if (key === 'right') {
                arrayPointsOfCircle[key].cx = moveX + circle.radius;
                arrayPointsOfCircle[key].cy = moveY;
            }
            if (key === 'lower') {
                arrayPointsOfCircle[key].cx = moveX;
                arrayPointsOfCircle[key].cy = moveY + circle.radius;
            }
            if (key === 'center') {
                arrayPointsOfCircle[key].cx = moveX;
                arrayPointsOfCircle[key].cy = moveY;
            }
        }

        if (!pseudoCirclePainted) {
            EDITOR.draw(pseudoCircle);
        } else {
            EDITOR.redraw(pseudoCircle);
        }

        pseudoCirclePainted = true;
    });

    gcad.element.on('mousedown', function() {
        gcad.element.on('mousemove', null);

        if (pseudoCirclePainted) {
            for (var key in pseudoCircle.points) {
                circle.points[key] = arrayPointsOfCircle[key];
            }

            circle.centerX = pseudoCircle.centerX;
            circle.centerY = pseudoCircle.centerY;

            EDITOR.removeObject(pseudoCircle);
            EDITOR.redraw(circle);
        }

        EDITOR.resetSnap();
        EDITOR.setHandlersDefault();
        EDITOR.handlersStore.circle.setMouseMoveEvent(circle);
        EDITOR.zoomOn();
    });
};
