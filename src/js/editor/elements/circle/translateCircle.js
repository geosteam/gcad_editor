/**
 * Перемещение окружности
 * @param circle
 * @param offsetX - Смещение по x
 * @param offsetY - Смещение по y
 */
EDITOR.translateCircle = function(circle, offsetX, offsetY) {
    var circlePoints = circle.points;

    for (var key in circle.points) {
        circlePoints[key].cx += offsetX;
        circlePoints[key].cy += offsetY;
    }

    circle.centerX += offsetX;
    circle.centerY += offsetY;
};
