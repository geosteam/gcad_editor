/**
 * Удаляем нарисованный объект
 * @param obj Объект фигуры
 */
EDITOR.removeObject = function (obj) {
    var layer = this.gcad.store.layers[obj.layerID];

    if (obj.type === 'polyline') {
        this.polylinePainter.remove(obj);
        delete this.gcad.store.polylines[obj.id];
        delete layer.objects.polylines[obj.id];
        if (this.gcad.selectedObjectsStore.polylines[obj.id]) {
            delete this.gcad.selectedObjectsStore.polylines[obj.id];
        }
    }

    if (obj.type === 'spline') {
        this.splinePainter.remove(obj);
        delete this.gcad.store.splines[obj.id];
        delete layer.objects.splines[obj.id];
        if (this.gcad.selectedObjectsStore.splines[obj.id]) {
            delete this.gcad.selectedObjectsStore.splines[obj.id];
        }
    }

    if (obj.type === 'ruler') {
        this.rulerPainter.remove(obj);
        delete this.gcad.store.rulers[obj.id];
        delete layer.objects.rulers[obj.id];
        if (this.gcad.selectedObjectsStore.rulers[obj.id]) {
            delete this.gcad.selectedObjectsStore.rulers[obj.id];
        }
    }

    if (obj.type === 'rect') {
        this.rectPainter.remove(obj);
        delete this.gcad.store.rectangles[obj.id];
        delete layer.objects.rectangles[obj.id];
        if (this.gcad.selectedObjectsStore.rectangles[obj.id]) {
            delete this.gcad.selectedObjectsStore.rectangles[obj.id];
        }
    }

    if (obj.type === 'circle') {
        this.circlePainter.remove(obj);
        delete this.gcad.store.circles[obj.id];
        delete layer.objects.circles[obj.id];
        if (this.gcad.selectedObjectsStore.circles[obj.id]) {
            delete this.gcad.selectedObjectsStore.circles[obj.id];
        }
    }

    if (obj.type === 'path') {
        this.pathPainter.remove(obj);
        delete this.gcad.store.path[obj.id];
    }

    if (obj.type === 'point') {
        this.pointPainter.remove(obj);
        delete this.gcad.store.points[obj.id];
    }

    if (obj.type === 'line') {
        this.linePainter.remove(obj);
        delete this.gcad.store.lines[obj.id];
    }

    if (obj.type === 'layer') {
        obj.element.remove();
        delete this.gcad.store.layers[obj.id];
    }
};
