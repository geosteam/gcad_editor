/**
 * Инициализация редактора
 * @param selector Селектор в который будет помещено окно редактора
  */
EDITOR.Initialization = function (selector) {
    var generalLayer = new GCAD.Layer('general'); // Слой-контейнер для масштабирования, главный слой.
    var emptyLayer = new GCAD.Layer('default'); // Пустой слой

    this.gcad = new GCAD.Core();
    this.gcad.UndoManager = new UndoManager(); // Undo Redo Менеджер. До конца не реализован...
    this.gcad.init(selector);
    this.gcad.store = new GCAD.Store();
    this.gcad.selectedObjectsStore = new GCAD.SelectedObjectsStore();
    this.containerSelector = selector;
    this.gcad.generalLayer = this.gcad.element.append('g').attr('id', generalLayer.name);
    this.addLayer(emptyLayer);
    this.setDefaultLayer(emptyLayer);
    this.setHandlersDefault();
    this.zoomOn();
    this.enabledContextMenu();
    this.setDefaultKeyBinds();
    this.initDefs();

    // Отключает контнкстное меню на всем документе
    document.oncontextmenu = function () {return false};
};

/**
 * Получить уникальный id
 * @returns {string} id
 */
EDITOR.getUniqueId = function() {
    return 'id' + Math.random().toString(36).substr(2, 9);
};

/**
 * Инициализировать контекстное меню
 */
EDITOR.enabledContextMenu = function() {
    var self = this;

    self.gcad.element.on('contextmenu', d3.contextMenu(
        function () {
            return EDITOR.showContextMenu(d3.event.target);
        },
        {
            onOpen: function() {
                self.disabledKeyBinds();
            },
            onClose: function() {
                self.setDefaultKeyBinds();
            }
        }
    ));
};

/**
 * Отключить контекстное меню
 */
EDITOR.disabledContextMenu = function() {
    this.gcad.element.on('contextmenu', null);
};

/**
 * Установить слой по умолчанию
 * @param layer Слой
 */
EDITOR.setDefaultLayer = function (layer) {
    this.gcad.defaultLayer = layer;
    this.gcad.store.defaultLayer = layer;
};

/**
 * Добавить новый слой
 * @param layer Объект слоя
 */
EDITOR.addLayer = function (layer) {
    if (!layer.id) {
        layer.id = GCAD.getUniqueId();
    }

    this.gcad.store.add(layer);
    layer.element = d3.selectAll('#general').append('g').attr('name', layer.name).attr('id', layer.id);
};

/**
 * Удаление слоя
 * @param layer Объект слоя
 */
EDITOR.deleteLayer = function(layer) {
    this.gcad.store.delete(layer);
    layer.element.remove();
};

/**
 * Переименование слоя
 * @param layerId Идентификатор слоя
 * @param name Новое имя слоя
 */
EDITOR.renameLayer = function(layerId, name) {
    var layer = EDITOR.gcad.store.layers[layerId];

    layer.name = name;
    layer.element.attr('name', name);
};

/**
 * Получить объект по его идентификатору
 * @param id Идентификатор объекта
 * @returns {*}
 */
EDITOR.getObjectById = function(id) {
    for (var key in this.gcad.store) {
        if (typeof this.gcad.store[key] === 'object') {
            for (var keyId in this.gcad.store[key]) {
                if (keyId === id) {
                    return this.gcad.store[key][keyId];
                }
            }
        }
    }
};

/**
 * Перерисовать все объкты принадлежищие указанному слою
 * @param layerId Идентификатор слоя
 */
EDITOR.redrawObjectsOfLayer = function(layerId) {
    var layer = this.gcad.store.layers[layerId];

    for (var key in layer.objects) {
        for (var keyId in layer.objects[key]) {
            var object = layer.objects[key][keyId];
            this.applyStyleFromGlobal(object);
            this.redraw(object);
        }
    }
};

/*EDITOR.drawDemo = function () {
    var self = this;
    //demoPerformance();

    function demoPerformance() {

        for (var i = 0; i < 1; i++) {
            var polyline = new GCAD.Polyline();

            polyline.id = EDITOR.getUniqueId();
            polyline.layerID = self.gcad.defaultLayer.id;

            self.applyStyleFromGlobal(polyline);

            for (var j = 0; j < 10000; j++) {
                var x = Math.random() * 1200;
                var y = Math.random() * 900;

                self.handlersStore.polyline.addPointInPolyline(polyline, x, y);
            }
            self.draw(polyline);
            self.handlersStore.polyline.resetSelect(polyline);
        }
    }
};*/

/**
 * Получить хранилище store в виде json строки
 */
EDITOR.getJsonStore = function () {
    return JSON.stringify(this.gcad.store);
};

/**
 * Получить store
 * @returns {object} Объект хранилища
 */
EDITOR.getStore = function () {
    return this.gcad.store;
};

/**
 * Получить все выделенные объекты
 * @returns {object}
 */
EDITOR.getSelectedObject = function () {
    return this.gcad.selectedObjectsStore;
};

/**
 * Включить zoom
 */
EDITOR.zoomOn = function () {
    this.gcad.element.call(this.gcad.zoom).on("dblclick.zoom", null);
};

/**
 * Отключить zoom
 */
EDITOR.zoomOff = function () {
    this.gcad.element.call(this.gcad.disableZoom);
};

/**
 * Стрелки для линейки
 */
EDITOR.initDefs = function() {
    d3.select('#general').append('defs');

    // default

    d3.select('defs').append('marker')
        .attr('orient', 'auto')
        .attr('id', 'mark')
        .attr('viewBox', '-27 -12 23 18')
        .attr('markerWidth', '18')
        .attr('markerHeight', '10')
        .attr('preserveAspectRatio', "xMinYMax meet");

    d3.select('defs').append('marker')
        .attr('orient', 'auto')
        .attr('id', 'mark-left')
        .attr('viewBox', '-2 -12 23 18')
        .attr('markerWidth', '18')
        .attr('markerHeight', '10')
        .attr('preserveAspectRatio', "xMinYMax meet");

    d3.select('#mark-left').append('path')
        .attr('d', 'M0,0 L20,-4 16,0 20,4 z M0,-10 L0,10 M0,0 L27,0')
        .attr('fill', 'rgb(39, 165, 249)');

    d3.select('#mark').append('path')
        .attr('d', 'M0,0 L-20,-4 -16,0 -20,4 z M0,-10 L0,10 M0,0 L-27,0')
        .attr('fill', 'rgb(39, 165, 249)');

    // hover

    d3.select('defs').append('marker')
        .attr('orient', 'auto')
        .attr('id', 'mark-hover')
        .attr('viewBox', '-2 -12 23 18')
        .attr('markerWidth', '18')
        .attr('markerHeight', '10')
        .attr('preserveAspectRatio', "xMinYMax meet");

    d3.select('defs').append('marker-hover')
        .attr('orient', 'auto')
        .attr('id', 'mark-left-hover')
        .attr('viewBox', '-2 -12 23 18')
        .attr('markerWidth', '18')
        .attr('markerHeight', '10')
        .attr('preserveAspectRatio', "xMinYMax meet");

    // hover
    d3.select('#mark-left-hover').append('path')
        .attr('d', 'M0,0 L20,-4 16,0 20,4 z M0,-10 L0,10 M0,0 L27,0')
        .attr('fill', 'rgb(0, 0, 0)');
    // hover
    d3.select('#mark-hover').append('path')
        .attr('d', 'M0,0 L-20,-4 -16,0 -20,4 z M0,-10 L0,10 M0,0 L-27,0')
        .attr('fill', 'rgb(0, 0, 0)');
};

/**
 * Очистить все. Используется для загрузки сохраненного состояния редактора
 */
EDITOR.removeAll = function() {
    var store = EDITOR.gcad.store;
    var selectedObjectsStore = EDITOR.gcad.selectedObjectsStore;

    for (var layerId in store.layers) {
        this.removeObject(store.layers[layerId]);
    }

    EDITOR.gcad.defaultLayer = null;

    store.defaultLayer = {};
    store.layers = {};
    store.circles = {};
    store.lines = {};
    store.path = {};
    store.points = {};
    store.polylines = {};
    store.rectangles = {};
    store.rulers = {};
    store.text = {};

    selectedObjectsStore.polylines = {};
    selectedObjectsStore.rulers = {};
    selectedObjectsStore.rectangles = {};
    selectedObjectsStore.circles = {};
};

/**
 * Перерисовать все
 */
EDITOR.redrawAll = function() {
    var self = EDITOR;
    var store = EDITOR.gcad.store;

    for (var layerId in store.layers) {
        self.addLayer(store.layers[layerId]);
    }

    self.setDefaultLayer(store.defaultLayer);

    for (var circleId in store.circles) {
        self.draw(store.circles[circleId]);
    }

    for (var linesId in store.lines) {
        self.draw(store.lines[linesId]);
    }

    for (var pathId in store.path) {
        self.draw(store.path[pathId]);
    }

    for (var pointId in store.points) {
        self.draw(store.points[pointId]);
    }

    for (var polylineId in store.polylines) {
        self.draw(store.polylines[polylineId]);
    }

    for (var rectangleId in store.rectangles) {
        self.draw(store.rectangles[rectangleId]);
    }

    for (var rulerId in store.rulers) {
        self.draw(store.rulers[rulerId]);
    }

    for (var textId in store.text) {
        self.draw(store.text[textId]);
    }
};

/**
 * Загрузить состояние из json
 * @param string json строка
 */
EDITOR.load = function(string) {
    var data = JSON.parse(string);
    var store = EDITOR.gcad.store;

    this.removeAll();

    store.defaultLayer = data.defaultLayer;
    store.layers = data.layers;
    store.circles = data.circles;
    //store.lines = data.lines;
    store.path = data.path;
    //store.points = data.points;
    store.polylines = data.polylines;
    store.rectangles = data.rectangles;
    store.rulers = data.rulers;
    store.text = data.text;

    this.redrawAll();
};

/**
 * Применить стили по умолчанию. Стили берутся из файла global.js
 * @param object Объект фигуры
 */
EDITOR.applyStyleFromGlobal = function(object) {
    /**
     * Прежде чем передавать в этот метод объект нужно убедиться
     * что у него есть свойство layerID. layerID обязательно
     * должен быть!
     */
    if (!object.layerID) {
        console.warn('У объекта не задан id слоя');
    }

    var layer = this.gcad.store.layers[object.layerID];

    if (object.type === 'polyline') {
        if (layer.color) {
            object.color = layer.color;
        } else {
            object.color = GCAD.Style.polyline.color;
        }

        if (layer.opacity) {
            object.style.edge.opacity = layer.opacity;
        } else {
            object.style.edge.opacity = GCAD.Style.polyline.edge.opacity;
        }

        if (layer.strokeDasharrayOfLines) {
            object.style.edge.strokeDasharray = layer.strokeDasharrayOfLines;
        } else {
            object.style.edge.strokeDasharray = GCAD.Style.polyline.edge.strokeDasharray;
        }

        if (layer.widthOfLines) {
            object.style.edge.strokeWidth = layer.widthOfLines;
        } else {
            object.style.edge.strokeWidth = GCAD.Style.polyline.edge.strokeWidth;
        }

        object.style.edge.color = object.color;
        object.style.edge.mouseoverColor = GCAD.Style.polyline.edge.mouseoverColor;
        object.style.edge.selectedColor = GCAD.Style.polyline.edge.selectedColor;
        object.style.selectedColor = GCAD.Style.polyline.selectedColor;
    }

    if (object.type === 'spline') {
        if (layer.color) {
            object.color = layer.color;
        } else {
            object.color = GCAD.Style.spline.color;
        }

        if (layer.opacity) {
            object.opacity = layer.opacity;
        } else {
            object.opacity = GCAD.Style.spline.opacity;
        }

        if (layer.widthOfLines) {
            object.strokeWidth = layer.widthOfLines;
        } else {
            object.strokeWidth = GCAD.Style.spline.strokeWidth;
        }

        if (layer.strokeDasharrayOfLines) {
            object.strokeDasharray = layer.strokeDasharrayOfLines;
        } else {
            object.strokeDasharray = GCAD.Style.spline.strokeDasharray;
        }

        object.fill = GCAD.Style.spline.fill;
        object.mouseoverColor = GCAD.Style.spline.mouseoverColor;
    }

    if (object.type === 'path') {
        object.color = GCAD.Style.path.color;
        object.strokeWidth = GCAD.Style.path.strokeWidth;
        object.strokeDasharray = GCAD.Style.path.strokeDasharray;
    }

    if (object.type === 'rect') {
        object.color = GCAD.Style.rect.color;
        object.selectedColor = GCAD.Style.rect.selectedColor;
        object.strokeWidth = GCAD.Style.rect.strokeWidth;
        object.strokeDasharray = GCAD.Style.rect.strokeDasharray;
        object.fill = GCAD.Style.rect.fill;

        checkTheSettingsLayer(object, layer);
    }

    if (object.type === 'point') {
        object.color = GCAD.Style.point.color;
        object.selectedColor = GCAD.Style.point.selectedColor;
        object.radius = GCAD.Style.point.radius;
    }

    if (object.type === 'circle') {
        object.color = GCAD.Style.circle.color;
        object.selectedColor = GCAD.Style.circle.selectedColor;
        object.strokeWidth = GCAD.Style.circle.strokeWidth;
        object.fill = GCAD.Style.circle.fill;
        object.strokeDasharray = GCAD.Style.circle.strokeDasharray;

        checkTheSettingsLayer(object, layer);
    }

    if (object.type === 'ruler') {
        object.color = GCAD.Style.ruler.color;
        object.strokeWidth = GCAD.Style.ruler.strokeWidth;

        //checkTheSettingsLayer(object, layer);
    }

    /**
     * Если у слоя есть какие-либо заданные свойства
     * то применяем их к объекту. Объект естественно принадлежит
     * этому слою.
     * @param object
     * @param layer
     */
    function checkTheSettingsLayer(object, layer) {
        if (layer.color) {
            object.color = layer.color;
        }
        if (layer.opacity) {
            object.opacity = layer.opacity;
        }
        if (layer.strokeDasharrayOfLines) {
            object.strokeDasharray = layer.strokeDasharrayOfLines;
        }
        if (layer.widthOfLines) {
            object.strokeWidth = layer.widthOfLines;
        }
    }
};
