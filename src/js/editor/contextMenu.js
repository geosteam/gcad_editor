EDITOR.showContextMenu = function(target) {
    var self = this;
    var tagName = target.tagName;
    var owner = getOwner(target);

    var menu = {
        polylineMenu: {
            point: [
                {
                    title: 'Удалить точку',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];
                        var point = polyline.points[target.attributes['id'].value];

                        EDITOR.handlersStore.polyline.deletePoint(polyline, point);
                    }
                },
                {
                    title: 'Удалить точку с размыканием',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];
                        var point = polyline.points[target.attributes['id'].value];

                        EDITOR.handlersStore.polyline.deletePointOpening(polyline, point);
                    }
                },
                {
                    title: 'Свойства',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];
                        var point = polyline.points[target.attributes['id'].value];

                        EDITOR.runDialogForPolylinePoint(polyline, point);
                    }
                }
            ],
            lastPoint: [
                {
                    title: 'Удалить точку',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];
                        var point = polyline.points[target.attributes['id'].value];

                        EDITOR.handlersStore.polyline.deletePoint(polyline, point);
                    }
                },
                {
                    title: 'Свойства',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];
                        var point = polyline.points[target.attributes['id'].value];

                        EDITOR.runDialogForPolylinePoint(polyline, point);
                    }
                }
            ],
            line: [
                {
                    title: 'Добавить точку',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];
                        var edge = polyline.edges[target.attributes['id'].value];

                        EDITOR.handlersStore.polyline.addPoint(polyline, edge);
                    }
                },
                {
                    title: 'Замкнуть полилинию',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];
                        EDITOR.handlersStore.polyline.close(polyline);
                    }
                },
                {
                    title: 'Копировать в слой',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];

                        EDITOR.runDialogCopyInLayer(polyline.id);
                    }
                },
                {
                    title: 'Удалить сегмент',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];
                        var edge = polyline.edges[target.attributes['id'].value];

                        EDITOR.handlersStore.polyline.deleteEdge(polyline, edge);
                    }
                },
                {
                    title: 'Удалить полилинию',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];

                        EDITOR.removeObject(polyline);
                    }
                },
                {
                    title: 'Свойства',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];

                        EDITOR.runDialogForPolyline(polyline);
                    }
                }
            ],
            closingPolyline: [
                {
                    title: 'Добавить точку',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];
                        var edge = polyline.edges[target.attributes['id'].value];

                        EDITOR.handlersStore.polyline.addPoint(polyline, edge);
                    }
                },
                {
                    title: 'Разомкнуть полилинию',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];

                        EDITOR.handlersStore.polyline.open(polyline);
                    }
                },
                {
                    title: 'Копировать в слой',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];

                        EDITOR.runDialogCopyInLayer(polyline.id);
                    }
                },
                {
                    title: 'Удалить сегмент',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];
                        var edge = polyline.edges[target.attributes['id'].value];

                        EDITOR.handlersStore.polyline.deleteEdge(polyline, edge);
                    }
                },
                {
                    title: 'Удалить полилинию',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];

                        EDITOR.removeObject(polyline);
                    }
                }
            ],
            polylineWithOneSegment: [
                {
                    title: 'Добавить точку',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];
                        var edge = polyline.edges[target.attributes['id'].value];

                        EDITOR.handlersStore.polyline.addPoint(polyline, edge);
                    }
                },
                {
                    title: 'Копировать в слой',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];

                        EDITOR.runDialogCopyInLayer(polyline.id);
                    }
                },
                {
                    title: 'Удалить полилинию',
                    action: function() {
                        var polyline = EDITOR.gcad.store.polylines[target.attributes['ownerID'].value];

                        EDITOR.removeObject(polyline);
                    }
                }
            ]
        },
        rulerMenu: {
            line: [
                {
                    title: 'Удалить линейку',
                    action: function() {
                        var ruler = EDITOR.gcad.store.rulers[target.attributes['ownerID'].value];
                        EDITOR.removeObject(ruler);
                    }
                },
                /*{
                    title: 'Копировать в слой',
                    action: function() {
                        var ruler = EDITOR.gcad.store.rulers[target.attributes['ownerID'].value];
                        EDITOR.runDialogCopyInLayer([ruler.id]);
                    }
                },*/
                {
                    title: 'Свойства',
                    action: function() {
                        var ruler = EDITOR.gcad.store.rulers[target.attributes['ownerID'].value];
                        EDITOR.runDialogForRuler(ruler);
                    }
                }
            ]
        },
        splineMenu: {
            circle: [
                {
                    title: 'Добавить точку',
                    action: function() {
                        var spline = EDITOR.gcad.store.splines[target.attributes['ownerID'].value];
                        var point = spline.points[target.attributes['id'].value];

                        EDITOR.handlersStore.spline.addPointInSplineBetweenPoints(
                            spline,
                            point,
                            self.helpers.getMouseCoordinate()[0],
                            self.helpers.getMouseCoordinate()[1]
                        );
                    }
                },
                {
                    title: 'Удалить точку',
                    action: function() {
                        var spline = EDITOR.gcad.store.splines[target.attributes['ownerID'].value];
                        var point = spline.points[target.attributes['id'].value];

                        EDITOR.handlersStore.spline.deletePoint(spline.id, point.pointSpline.id);
                    }
                },
                {
                    title: 'Свойства',
                    action: function() {
                        var spline = EDITOR.gcad.store.splines[target.attributes['ownerID'].value];
                        var point = spline.points[target.attributes['id'].value];

                        EDITOR.runDialogForSplinePoint(spline, point);
                    }
                }
            ],
            path: [
                {
                    title: 'Замкнуть сплайн',
                    action: function() {
                        EDITOR.handlersStore.spline.close(target.attributes['ownerID'].value);
                    }
                },
                {
                    title: 'Свойства',
                    action: function() {
                        var spline = EDITOR.gcad.store.splines[target.attributes['ownerID'].value];

                        EDITOR.runDialogForSpline(spline);
                    }
                }
            ],
            closingSpline: [
                {
                    title: 'Разомкнуть сплайн',
                    action: function() {
                        EDITOR.handlersStore.spline.open(target.attributes['ownerID'].value);
                    }
                },
                {
                    title: 'Свойства',
                    action: function() {
                        var spline = EDITOR.gcad.store.splines[target.attributes['ownerID'].value];

                        EDITOR.runDialogForSpline(spline);
                    }
                }
            ]
        },
        rectMenu: {
            line: [
                {
                    title: 'Удалить прямоугольник',
                    action: function() {
                        var rect = EDITOR.gcad.store.rectangles[target.attributes['ownerID'].value];
                        EDITOR.removeObject(rect);
                    }
                },
                {
                    title: 'Копировать в слой',
                    action: function() {
                        var rect = EDITOR.gcad.store.rectangles[target.attributes['ownerID'].value];
                        EDITOR.runDialogCopyInLayer(rect.id);
                    }
                },
                {
                    title: 'Свойства',
                    action: function() {
                        var rect = EDITOR.gcad.store.rectangles[target.attributes['ownerID'].value];
                        if (rect.selectedPointId) {
                            EDITOR.handlersStore.rect.resetSelectPoint(rect, rect.selectedPointId);
                        }

                        EDITOR.runDialogForRect(rect);
                    }
                }
            ]
        },
        severalObjectsMenu: [
            {
                title: 'Свойства',
                action: function() {
                    var objects = [];

                    for (var key in self.gcad.selectedObjectsStore) {
                        if (key === 'numberOfSelectedObjects') {
                            continue;
                        }
                        for (var element in self.gcad.selectedObjectsStore[key]) {
                            objects.push(self.gcad.selectedObjectsStore[key][element]);
                        }
                    }

                    EDITOR.runDialogForSeveralObjects(objects);
                }
            }
        ],
        circleMenu: [
            {
                title: 'Удалить окружность',
                action: function() {
                    var circle = EDITOR.gcad.store.circles[target.attributes['ownerID'].value];
                    EDITOR.removeObject(circle);
                }
            },
            {
                title: 'Копировать в слой',
                action: function() {
                    var circle = EDITOR.gcad.store.circles[target.attributes['ownerID'].value];

                    EDITOR.runDialogCopyInLayer(circle.id);
                }
            },
            {
                title: 'Свойства',
                action: function() {
                    var circle = EDITOR.gcad.store.circles[target.attributes['ownerID'].value];
                    EDITOR.runDialogForCircle(circle);
                }
            }
        ]
    };

    var actions = {
        polyline: {
            circle: function() {
                var pointID = getAttributeElement(target, 'id');
                var polyline = self.gcad.store.polylines[getAttributeElement(target, 'ownerID')];

                if (+pointID === 0 || +pointID === polyline.points.length - 1) {
                    if (polyline.isClose) {
                        return menu.polylineMenu.point;
                    }
                    return menu.polylineMenu.lastPoint;
                }  else {
                    return menu.polylineMenu.point;
                }
            },
            edge: function() {
                var polyline = self.gcad.store.polylines[getAttributeElement(target, 'ownerID')];

                if (polyline.isClose) {
                    return menu.polylineMenu.closingPolyline;
                } else if (polyline.edges.length < 2) {
                    return menu.polylineMenu.polylineWithOneSegment;
                } else {
                    return menu.polylineMenu.line;
                }
            }
        },
        ruler: {
            line: function() {
                return menu.rulerMenu.line;
            }
        },
        rect: {
            line: function() {
                return menu.rectMenu.line;
            }
        },
        spline: {
            circle: function() {
                return menu.splineMenu.circle;
            },
            path: function() {
                var spline = EDITOR.gcad.store.splines[target.attributes['ownerID'].value];

                if (spline.isClose) {
                    return menu.splineMenu.closingSpline;
                } else {
                    return menu.splineMenu.path;
                }
            }
        },
        circle: function() {
            return menu.circleMenu;
        },
        severalObjects: function() {
            return menu.severalObjectsMenu;
        }
    };

    var numbersOfObjects = 0; // число выделенных объектов

    /**
     * Считаем количество выделенных объектов, и в случае если их несколько вызываем
     * соответствующее контекстное меню
     */
    for (var key in self.gcad.selectedObjectsStore) {
        for (var id in self.gcad.selectedObjectsStore[key]) {
            if (numbersOfObjects > 1) continue;
            numbersOfObjects++;
        }
        if (numbersOfObjects > 1) {
            if (owner === 'gcad') {
                d3.select('.d3-context-menu').style('display', 'none');
                return '';
            } else {
                return actions.severalObjects();
            }
        }
    }

    if (self.gcad.selectedObjectsStore.numberOfSelectedObjects > 1) {
        if (owner === 'gcad') {
            d3.select('.d3-context-menu').style('display', 'none');
            return '';
        } else {
            return actions.severalObjects();
        }
    }

    if (owner === 'polyline') {
        if (tagName === 'circle') {
            return actions.polyline.circle();
        }
        if (tagName === 'line') {
            return actions.polyline.edge();
        }
    }

    if (owner === 'gcad') {
        d3.select('.d3-context-menu').style('display', 'none');
        return '';
    }

    if (owner === 'ruler') {
        if (tagName === 'line') {
            return actions.ruler.line;
        }
        if (tagName === 'circle') {
            return false;
        }
    }

    if (owner === 'rect') {
        if (tagName === 'polygon') {
            return actions.rect.line;
        }

        if (tagName === 'circle') {
            return actions.rect.line;
        }
    }

    if (owner === 'circle') {
        return actions.circle;
    }

    if (owner === 'spline') {
        if (tagName === 'circle') {
            return actions.spline.circle;
        }
        if (tagName === 'path') {
            return actions.spline.path;
        }
    }

    function getAttributeElement(element, attr) {
        return element.attributes[attr].value;
    }

    function getOwner(element) {
        return element.attributes['owner'].value;
    }

};

