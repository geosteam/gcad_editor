/**
 * Сбрасываем выделение со всех нарисованных объектов
 */
EDITOR.resetGcadSelections = function() {
    var selectedObjectStore = EDITOR.gcad.selectedObjectsStore;
    var handlersStore = EDITOR.handlersStore;
    
    var selectedPolyline = selectedObjectStore.polylines;
    var selectedRulers = selectedObjectStore.rulers;
    var selectedRectangles = selectedObjectStore.rectangles;
    var selectedCircles = selectedObjectStore.circles;
    var selectedSplines = selectedObjectStore.splines;

    if (Object.keys(selectedPolyline).length > 0) {
        for (var key in selectedPolyline) {
            handlersStore.polyline.resetSelect(selectedPolyline[key]);
            delete EDITOR.gcad.selectedObjectsStore.polylines[key];
        }
    }

    if (Object.keys(selectedRulers).length > 0) {
        for (var key in selectedRulers) {
            handlersStore.ruler.resetSelect(selectedRulers[key]);
            delete EDITOR.gcad.selectedObjectsStore.rulers[key];
        }
    }

    if (Object.keys(selectedRectangles).length > 0) {
        for (var key in selectedRectangles) {
            handlersStore.rect.resetSelect(selectedRectangles[key]);
            delete EDITOR.gcad.selectedObjectsStore.rectangles[key];
        }
    }

    if (Object.keys(selectedCircles).length > 0) {
        for (var key in selectedCircles) {
            handlersStore.circle.resetSelect(selectedCircles[key]);
            delete EDITOR.gcad.selectedObjectsStore.circles[key];
        }
    }

    if (Object.keys(selectedSplines).length > 0) {
        for (var key in selectedSplines) {
            handlersStore.spline.resetSelect(selectedSplines[key]);
            delete EDITOR.gcad.selectedObjectsStore.splines[key];
        }
    }

    selectedObjectStore.numberOfSelectedObjects = 0;
};
