/**
 * Полотно кода отвечающее за обработку всех событий в редакторе.
 * Господи... Сделай так чтобы кроме меня больше никому не пришлось его читать.
 */

/**
 * Инициализация событий по умолчанию
 */
EDITOR.setHandlersDefault = function () {
    var self = this;
    var gcad = self.gcad;
    var handlers = self.handlersStore;
    var store = gcad.store;


    /**
     * Наведение мыши на элемент
     */
    gcad.element.on('mouseover', function() {
         var eventTarget = d3.event.target;

         //Полилиния
         if (eventTarget.attributes['owner'].value === 'polyline') {
             var polyline = store.polylines[eventTarget.attributes['ownerID'].value];

             if (polyline.isSelected) {
                 return false;
             }

             handlers.polyline.setMouseMoveEvent(polyline);

             polyline.points.forEach(function(item) {
                 EDITOR.pointPainter.showPoint(item);
             });
         }

        //Сплайн
        if (eventTarget.attributes['owner'].value === 'spline') {
            var spline = store.splines[eventTarget.attributes['ownerID'].value];

            if (spline.isSelected) {
                return false;
            }

            handlers.spline.setMouseMoveEvent(spline);

            for (var key in spline.points) {
                EDITOR.pointPainter.showPoint(spline.points[key].pointSpline);
            }
        }

        //Прямоугольник
        if (eventTarget.attributes['owner'].value === 'rect') {
            var rect = store.rectangles[eventTarget.attributes['ownerID'].value];

            if (rect.isSelected) {
                return false;
            }

            for (var key in rect.points) {
                if (rect.points.hasOwnProperty(key)) {
                    EDITOR.pointPainter.showPoint(rect.points[key]);
                }
            }

            handlers.rect.setMouseMoveEvent(rect);
        }

        // Линейка
        if (eventTarget.attributes['owner'].value === 'ruler') {
            var ruler = self.gcad.store.rulers[eventTarget.attributes['ownerID'].value];

            ruler.extensionLines.leftLine.element.style('stroke', GCAD.Style.ruler.mouseoverColor);
            ruler.extensionLines.rightLine.element.style('stroke', GCAD.Style.ruler.mouseoverColor);
            ruler.extensionLines.dimensionLine.element.style('stroke', GCAD.Style.ruler.mouseoverColor);
            EDITOR.pointPainter.showPoint(ruler.dimensionPoint);
        }

        //Окружность
        if (eventTarget.attributes['owner'].value === 'circle') {
            var circle = self.gcad.store.circles[eventTarget.attributes['ownerID'].value];

            for (var key in circle.points) {
                if (circle.points.hasOwnProperty(key) && circle.points[key]) {
                    EDITOR.pointPainter.showPoint(circle.points[key]);
                }
            }

            if (circle.points['upper']) { //Если есть верхняя точка, значит есть и остальные
                handlers.circle.setMouseMoveEvent(circle);
            }
        }
     });

    /**
     * Уход мыши из элемента
     */
    gcad.element.on('mouseout', function() {
        var eventTarget = d3.event.target;

        //Полилиния
        if (eventTarget.attributes['owner'].value === 'polyline') {
            var polyline = store.polylines[eventTarget.attributes['ownerID'].value];

            if (polyline.isSelected) {
                return false;
            }

            handlers.polyline.setMouseMoveEvent(polyline);

            polyline.points.forEach(function(item) {
                EDITOR.pointPainter.hidePoint(item);
            });
        }

        //Сплайн
        if (eventTarget.attributes['owner'].value === 'spline') {
            var spline = store.splines[eventTarget.attributes['ownerID'].value];

            if (spline.isSelected) {
                return false;
            }

            handlers.spline.setMouseMoveEvent(spline);

            for (var key in spline.points) {
                EDITOR.pointPainter.hidePoint(spline.points[key].pointSpline);
            }
        }

        //Прямоугольник
        if (eventTarget.attributes['owner'].value === 'rect') {
            var rect = store.rectangles[eventTarget.attributes['ownerID'].value];

            if (rect.isSelected) {
                return false;
            }

            for (var key in rect.points) {
                if (rect.points.hasOwnProperty(key)) {
                    EDITOR.pointPainter.hidePoint(rect.points[key]);
                }
            }

            handlers.rect.setMouseMoveEvent(rect);
        }

        // Линейка
        if (eventTarget.attributes['owner'].value === 'ruler') {
            var ruler = self.gcad.store.rulers[eventTarget.attributes['ownerID'].value];

            ruler.extensionLines.leftLine.element.style('stroke', ruler.color);
            ruler.extensionLines.rightLine.element.style('stroke', ruler.color);
            ruler.extensionLines.dimensionLine.element.style('stroke', ruler.color);

            if (!ruler.isSelected) {
                EDITOR.pointPainter.hidePoint(ruler.dimensionPoint);
            }
        }

        //Окружность
        if (eventTarget.attributes['owner'].value === 'circle') {
            var circle = self.gcad.store.circles[eventTarget.attributes['ownerID'].value];

            if (circle.isSelected) {
                return false;
            }

            for (var key in circle.points) {
                if (circle.points.hasOwnProperty(key) && circle.points[key]) {
                    EDITOR.pointPainter.hidePoint(circle.points[key]);
                }
            }

            if (circle.points['upper']) { //Если есть верхняя точка, значит есть и остальные
                handlers.circle.setMouseMoveEvent(circle);
            }
        }
    });

    /**
     * Клик по элементу
     */
    gcad.element.on('mousedown', function () {
        var eventTarget = d3.event.target;
        var handlers = self.handlersStore;

        //EDITOR.gcad.UndoManager.clear();

        var owner = eventTarget.attributes['owner'].value;

        if (owner === 'gcad') {
            if (d3.event.which === 2 || d3.event.which === 3) {
                return false;
            }

            var mouseDownX = d3.mouse(self.gcad.element.node())[0] / self.gcad.getScaleValue() -
                self.gcad.getTranslateValueX() / self.gcad.getScaleValue();

            var mouseDownY = d3.mouse(self.gcad.element.node())[1] / self.gcad.getScaleValue() -
                self.gcad.getTranslateValueY() / self.gcad.getScaleValue();

            self.selectionWithRectangle(mouseDownX, mouseDownY);

            return false;
        }

        var ownerId = eventTarget.attributes['ownerID'].value;
        var typeTarget = eventTarget.attributes['type'].value;
        var targetId = eventTarget.attributes['id'].value;

        if (owner === 'polyline') {
            if (typeTarget === 'point') {
                getPolylineEvent(store.polylines[ownerId].points[targetId]);
            }
            if (typeTarget === 'line') {
                getPolylineEvent(store.polylines[ownerId].edges[targetId]);
            }
        }

        if (owner === 'spline') {
            if (typeTarget === 'point') {
                getSplineEvent(store.splines[ownerId].points[targetId].pointSpline);
            }
            if (typeTarget === 'path') {
                getSplineEvent(store.splines[ownerId].path);
            }

        }

        if (eventTarget.attributes['owner'].value === 'ruler') {
            getRulerEvent();
        }

        if (eventTarget.attributes['owner'].value === 'rect') {
            getRectEvent();
        }

        if (eventTarget.attributes['owner'].value === 'circle') {
            getCircleEvent();
        }

        if (eventTarget.attributes['owner'].value === 'without-owner') {
            return false;
        }


        /**
         * Получить обработчик событий для прямогольника
         */
        function getRectEvent() {
            var rect = store.rectangles[eventTarget.attributes['ownerID'].value];

            handlers.rect.setSelect(rect); // Выделяем прямоугольник
            gcad.selectedObjectsStore.add(rect);

            //Если элемент по которому был произведен клик это circle
            //то кликнули по точке прямоугольника.
            if (eventTarget.tagName === 'circle') {
                if (d3.event.which === 3) {
                    return false;
                }

                var point = null;

                //Определяем точку по которой кликнули
                for (var keyIt in rect.points) {
                    if (rect.points[keyIt].id === eventTarget.attributes['id'].value) {
                        point = rect.points[keyIt];
                    }
                }

                //Выделяем точку по которой был произведен клик
                handlers.rect.setSelectPoint(rect, point);

                //Вызываем обработчик события для этой точки
                handlers.rect.getPointEvent(eventTarget.attributes['ownerID'].value, eventTarget.attributes['id'].value);
            }

            //Если элемент по которому был произведен клик это polygon
            //то кликнули по самому прямоугольнику.
            if (eventTarget.tagName === 'polygon') {

                if (d3.event.which === 3) {
                    return false;
                }

                //Вызываем обработчик события для прямоугольника
                handlers.rect.getLineEvent(rect.id);
            }
        }

        /**
         * Получить обработчик событий для полилинии
         */
        function getPolylineEvent(objTarget) {
            var polyline = store.polylines[objTarget.ownerID];

            gcad.selectedObjectsStore.add(polyline);

            handlers.polyline.setSelect(polyline);

            if (objTarget.element[0][0].tagName === 'circle') {
                var point = polyline.points[objTarget.id];

                if (d3.event.which === 3) {
                    handlers.polyline.setSelectPoint(polyline, point);
                    return false;
                }

                handlers.polyline.setSelectPoint(polyline, point);
                handlers.polyline.getPointEvent(objTarget.ownerID, objTarget.id);
            }

            if (objTarget.element[0][0].tagName === 'line') {
                var line = polyline.edges[objTarget.id];

                if (d3.event.which === 3) {
                    handlers.polyline.setSelectEdge(polyline, line);
                    return false;
                }

                handlers.polyline.setSelectEdge(polyline, line);
                handlers.polyline.getEdgeEvent(objTarget.ownerID);
            }
        }

        /**
         * Получить обработчик событий для сплайна
         */
        function getSplineEvent(objTarget) {
            var spline = store.splines[eventTarget.attributes['ownerID'].value];

            gcad.selectedObjectsStore.add(spline);

            handlers.spline.setSelect(spline);

            if (eventTarget.tagName === 'circle') {
                var point = spline.points[objTarget.id].pointSpline;

                if (d3.event.which === 3) {
                    handlers.spline.setSelectPoint(spline, point);
                    return false;
                }

                handlers.spline.setSelectPoint(spline, point);
                handlers.spline.getPointEvent(objTarget.ownerID, objTarget.id);
            }

            if (eventTarget.tagName === 'path') {
                if (d3.event.which === 3) {
                    return false;
                }

                handlers.spline.getPathEvent(spline.id);
            }
        }


        /**
         * Получить обработчик событий для линейки
         */
        function getRulerEvent() {
            var ruler = store.rulers[eventTarget.attributes['ownerID'].value];
            gcad.selectedObjectsStore.add(ruler);

            if (eventTarget.tagName === 'line') {
                if (d3.event.which === 3) {
                    handlers.ruler.setSelect(ruler);
                    return false;
                }

                handlers.ruler.setSelect(ruler);
            }

            if (eventTarget.tagName === 'circle') {
                if (d3.event.which === 3) {
                    handlers.ruler.setSelect(ruler);
                    return false;
                }

                handlers.ruler.setSelect(ruler);
                handlers.ruler.getRulerPointEvent(ruler);
            }
        }

        /**
         * Получить обработчик событий для окружности
         */
        function getCircleEvent() {
            var circle = store.circles[eventTarget.attributes['ownerID'].value];
            gcad.selectedObjectsStore.add(circle);

            if (eventTarget.tagName === 'circle' && eventTarget.attributes['type'].value === 'circle') {
                if (d3.event.which === 3) {
                    handlers.circle.setSelect(circle);
                    return false;
                }

                handlers.circle.setSelect(circle);
            }

            if (eventTarget.tagName === 'circle' && eventTarget.attributes['type'].value === 'point') {
                var point = null;
                handlers.circle.setSelect(circle);

                for (var keyIt in circle.points) {
                    if (circle.points[keyIt].id === eventTarget.attributes['id'].value) {
                        point = circle.points[keyIt];
                    }
                }

                if (d3.event.which === 3) {
                    handlers.circle.setSelectPoint(circle);
                    handlers.circle.setSelect(circle);
                    return false;
                }

                if (point.center === true) {
                    handlers.circle.getCenterEvent(circle.id);
                } else {
                    handlers.circle.getPointEvent(circle.id);
                }
                handlers.circle.setSelectPoint(circle, point);
            }
        }
    });
};
