/**
 * Рисовальщик svg элемента path
 */
EDITOR.pathPainter = {
    draw: function(path, layerID) {
        //Добавляем svg элемент path и задаем ему свойства объекта Path
        path.element = d3.selectAll('#' + layerID).append('path').attr({
            'd': path.lineFunction(path.arrayOfPoints),
            'stroke': path.color,
            'stroke-width': path.strokeWidth,
            'stroke-dasharray': path.strokeDasharray,
            'owner': path.owner,
            'ownerID': path.ownerID,
            'type': path.type,
            'id': path.id
        });
        path.layerID = layerID;
    },

    redraw: function(path) {
        this.remove(path);
        this.draw(path, path.layerID);
    },

    remove: function(path) {
        path.element.remove();
    },

    setColor: function(path, color) {
        path.element.style({
            stroke: color
        });

        //path.color = color;
    }
};
