/**
 * Рисуем точку
 */
EDITOR.pointPainter = {
    draw: function(point, layerID) {
        //Добавляем svg элемент circle и задаем ему свойства объекта Point
        point.element = d3.selectAll('#' + layerID).append('circle').attr({
            'r': point.radius,
            'cx': point.cx,
            'cy': point.cy,
            'id': point.id,
            'owner': point.owner,
            'ownerID': point.ownerID,
            'type': 'point'
        }).style({
            'fill': point.color,
            'stroke-width': point.strokeWidth,
            'stroke': point.stroke,
            'fill-opacity': point.opacity
        });

        point.layerID = layerID;

        if (point.hide) {
            this.hidePoint(point);
        } else {
            this.showPoint(point);
        }
    },

    redraw: function(point) {
        point.element.remove();
        this.draw(point, point.layerID);
    },

    remove: function(point) {
        point.element.remove();
    },

    showPoint: function(point) {
        point.element.style({
            'opacity': 1
        });
        point.hide = false;
    },

    hidePoint: function(point) {
        point.element.style({
            'opacity': 0
        });
        point.hide = true;
    },

    setColor: function (point, color) {
        point.color = color;
    }
};
