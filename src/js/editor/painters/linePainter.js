/**
 * Рисовальщик для линии
 */
EDITOR.linePainter = {
    draw: function(line, layerID) {
        //Добавляем svg элемент line и задаем ему свойства объекта Line
        line.element = d3.selectAll('#' + layerID).append('line').attr({
            'x1': line.coordinates.begin.x,
            'y1': line.coordinates.begin.y,
            'x2': line.coordinates.close.x,
            'y2': line.coordinates.close.y,
            'stroke-width': line.strokeWidth,
            'stroke-dasharray': line.strokeDasharray,
            'id': line.id,
            'owner': line.owner,
            'ownerID': line.ownerID,
            'type': 'line'
        }).style({
            'stroke': line.color,
            'stroke-linecap': "round",
            'opacity': line.opacity
        });

        line.layerID = layerID;
    },
    remove: function(line) {
        line.element.remove();
    },
    redraw: function(line) {
        this.remove(line);
        this.draw(line, line.layerID);
    },
    setColor: function(line, color) {
        line.color = color;
    }
};