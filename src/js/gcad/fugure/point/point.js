(function() {
    'use strict';

    function Point(cx, cy) {
        this.id = null;
        this.owner = 'without-owner';
        this.ownerID = null;
        this.type = 'point';
        this.cx = cx;
        this.cy = cy;
        this.isSelected = false;
        this.layerID = null;
        this.element = null;
        this.hide = false;
        this.radius = 0;
        this.color = null;
        this.stroke = null;
        this.strokeWidth = null;
        this.selectedColor = null;
        this.opacity = 1;
    }

    GCAD.Point = Point;
})();
