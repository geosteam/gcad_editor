EDITOR.handlersStore.setMouseoutPoint = function(point, color) {
    point.element.on('mouseout', function() {
        point.element.style('fill', color);
    });
};

EDITOR.handlersStore.setMouseoverPoint = function(point, color) {
    point.element.on('mouseover', function() {
        point.element.style('fill', color);
    });
};

