(function() {
    'use strict';

    function Path(arrayOfPoints) {
        this.type = 'path';
        this.arrayOfPoints = arrayOfPoints;
        this.element = null;
        this.owner = 'without-owner';
        this.layer = null;
        this.id = null;
        this.color = null;
        this.strokeWidth = null;
        this.strokeDasharray = null;
        this.opacity = 1;
        this.interpolate = "cardinal";
        
        var _self = this;

        this.lineFunction = d3.svg.line()
            .x(function (d) {
                return d.x;
            })
            .y(function (d) {
                return d.y;
            }).interpolate(_self.interpolate);
        
        this.setInterpolate = function(interpolate) {
            _self.lineFunction = d3.svg.line()
                .x(function (d) {
                    return d.x;
                })
                .y(function (d) {
                    return d.y;
                }).interpolate(interpolate);
        }
    }

    GCAD.Path = Path;
})();
