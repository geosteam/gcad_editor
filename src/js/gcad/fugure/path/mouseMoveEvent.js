EDITOR.handlersStore.setMouseoverPath = function(path, color) {
    path.element.on('mouseover', function() {
        path.element.style('stroke', color);
    });
};

EDITOR.handlersStore.setMouseoutPath = function(path, color) {
    path.element.on('mouseout', function() {
        path.element.style('stroke', color);
    });
};

