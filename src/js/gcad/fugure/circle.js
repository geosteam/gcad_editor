(function() {
    'use strict';

    function Circle() {
        this.type = 'circle';
        this.element = null;
        this.owner = 'circle';
        this.ownerID = this.id;
        this.isSelected = false;
        this.selectedPointId = null;
        this.layer = null;
        this.layerID = null;
        this.id = null;
        this.color = null;
        this.newColor = null;
        this.newStrokeWidth = null;
        this.selectedColor = null;
        this.strokeWidth = null;
        this.strokeDasharray = 0;
        this.fill = null;
        this.opacity = 1;

        this.radius = null;
        this.centerX = 0;
        this.centerY = 0;

        this.points = {
            upper: null,
            left: null,
            right: null,
            lower: null,
            center: null
        }
    }

    GCAD.Circle = Circle;

})();
