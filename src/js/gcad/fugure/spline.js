(function() {
    'use strict';

    /**
     * Свойство points имеет  следующий вид:
     *
     *     points = {
     *       id: {
     *         pointSpline: Объект точки,
     *         pointPath: { 'x': x, 'y': y }
     *       }
     *     }
     *
     * id равен идентификатору объекта точки
     * Зачем все это? Потому что так надо...
     */
    function Spline() {
        this.id = null;
        this.type = 'spline';
        this.points = {};
        this.lastPointId = null;
        this.path = null;
        this.isClose = false;
        this.isSelected = false;
        this.selectedPoint = null;
        this.layerID = null;
        this.color = null;
        this.opacity = 1;
        this.mouseoverColor = null;
        this.fill = 'none';

        this.style = {
            color: null,
            mouseoverColor: null,
            strokeWidth: null,
            strokeDasharray: 0,
            selectedColor: null
        }
    }

    /*Spline.prototype.getAmountOfPoints = function() {
        return this.points.length;
    };

    Spline.prototype.getAmountOfEdges = function() {
        return this.edges.length;
    };

    Spline.prototype.open = function() {
        this.edges.splice(this.edges.length - 1, 1);
        this.isClose = false;
    };*/

    Spline.prototype.getAmountOfPoints = function() {
        var counter = 0;

        for (var key in this.points) {
            counter++;
        }
        return counter;
    };

    GCAD.Spline = Spline;
})();

