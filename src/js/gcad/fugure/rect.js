(function() {
    'use strict';

    function Rect() {
        this.type = 'rect';
        this.element = null;
        this.owner = 'rect';
        this.ownerID = null;
        this.isSelected = false;
        this.selectedPointId = null;
        this.layer = null;
        this.layerID = null;
        this.id = null;
        this.color = null;
        this.newColor = null;
        this.selectedColor = null;
        this.strokeWidth = null;
        this.strokeDasharray = null;
        this.fill = null;
        this.pseudo = false;
        this.opacity = 1;

        this.points = {
            upperLeft: null,
            upperRight: null,
            upperMiddle: null,
            left: null,
            right: null,
            lowerLeft: null,
            lowerRight: null,
            lowerMiddle: null
        };

        this.coordOfAngle = {
            upperLeft: {
                x: 0,
                y: 0
            },
            upperRight: {
                x: 0,
                y: 0
            },
            lowerRight: {
                x: 0,
                y: 0
            },
            lowerLeft: {
                x: 0,
                y: 0
            }
        };
    }

    GCAD.Rect = Rect;
})();
