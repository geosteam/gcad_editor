(function() {
    'use strict';

    function Polyline() {
        this.id = null;
        this.type = 'polyline';
        this.points = [];
        this.edges = [];
        this.isClose = false;
        this.isSelected = false;
        this.closingEdge = null;
        this.selectedPoint = null;
        this.selectedEdge = null;
        this.layerID = null;
        this.color = null;
        this.opacity = 1;

        this.style = {
            edge: {
                color: null,
                mouseoverColor: null,
                strokeWidth: null,
                strokeDasharray: 0,
                selectedColor: null
            },
            selectedColor: null
        }
    }

    Polyline.prototype.getAmountOfPoints = function() {
        return this.points.length;
    };

    Polyline.prototype.getAmountOfEdges = function() {
        return this.edges.length;
    };

    Polyline.prototype.open = function() {
        this.edges.splice(this.edges.length - 1, 1);
        this.isClose = false;
    };

    GCAD.Polyline = Polyline;
})();
