EDITOR.handlersStore.setMouseoverLine = function(line, color) {
    line.element.on('mouseover', function() {
        line.element.style('stroke', color);
    });
};

EDITOR.handlersStore.setMouseoutLine = function(line, color) {
    line.element.on('mouseout', function() {
        line.element.style('stroke', color);
    });
};
