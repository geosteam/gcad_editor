(function() {
    'use strict';

    function Line() {
        this.type = 'line';
        this.element = null;
        this.owner = 'without-owner';
        this.id = null;
        this.layerID = null;
        this.isSelected = false;
        this.color = null;
        this.strokeWidth = null;
        this.strokeDasharray = null;
        this.selectedColor = null;
        this.opacity = 1;
        this.coordinates = {
            begin: {
                x: 0,
                y: 0
            },
            close: {
                x: 0,
                y: 0
            }
        };

        this.setCoordinates = function(obj) {
            this.coordinates = obj;
        };

        this.getWidth = function() {
            function widthLine(x1, y1, x2, y2) {
                return Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
            }

            return widthLine(this.coordinates.begin.x,
                this.coordinates.begin.y,
                this.coordinates.close.x,
                this.coordinates.close.y
            );
        };

        this.getAngle = function() {
            function mathAzimuth(line) {
                return Math.acos((line[1][1] - line[0][1]) / Math.sqrt(Math.pow(line[1][0] -
                                line[0][0],2) + Math.pow(line[1][1] - line[0][1],2))) *180/Math.PI;
            }

            return mathAzimuth(this);
        };
    }

    GCAD.Line = Line;
})();
