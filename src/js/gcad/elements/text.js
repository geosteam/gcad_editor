(function() {
    'use strict';

    function Text() {
        this.type = 'text';
        this.element = null;
        this.layerID = null;
        this.owner = 'without-owner';
        this.ownerID = null;
        this.id = GCAD.getUniqueId();
        this.value = 'text';
        this.color = GCAD.Style.text.color;
        this.size = GCAD.Style.text.size;
        this.position = {
            x: 0,
            y: 0
        };
        this.rotate = null;
    }

    Text.prototype.changeText = function(text) {
        this.text = text;
    };

    Text.prototype.remove = function(text) {
        text.element.remove();
    };

    GCAD.Text = Text;
})();
