(function() {
    'use strict';

    function Ruler() {
        this.type = 'ruler';
        this.id = null;
        this.ownerID = null;
        this.layerID = null;
        this.dimensionPoint = null;
        this.angle = null;
        this.position = null;
        this.isSelected = false;
        this.width = 0;
        this.extensionLines = {
            leftLine: null,
            rightLine: null,
            dimensionLine: null
        };
        this.label = null;
        this.color = null;
        this.opacity = null;
        this.strokeDasharray = null;
        this.strokeWidth = null;
        this.offset = 5;
        this.pointLeft = {
            x: 0,
            y: 0
        };
        this.pointRight = {
            x: 0,
            y: 0
        };
    }

    Ruler.prototype.addExtensionsLines = function(leftLine, rightLine) {
        this.leftLine = leftLine;
        this.rightLine = rightLine;
    };

    Ruler.prototype.addLabel = function(label) {
        this.label = label;
        this.label.owner = this.type;
        this.label.ownerID = this.id;
    };

    GCAD.Ruler = Ruler;
})();
