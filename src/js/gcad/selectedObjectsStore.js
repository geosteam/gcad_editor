(function() {
    'use strict';

    function SelectedObjectsStore() {
        this.polylines = {};
        this.rulers = {};
        this.rectangles = {};
        this.circles = {};
        this.splines = {};
    }

    SelectedObjectsStore.prototype.add = function(object) {
        if (object.type === 'polyline') {
            this.polylines[object.id] = object;

        }
        if (object.type === 'spline') {
            this.splines[object.id] = object;

        }
        if (object.type === 'ruler') {
            this.rulers[object.id] = object;

        }
        if (object.type === 'rect') {
            this.rectangles[object.id] = object;

        }
        if (object.type === 'circle') {
            this.circles[object.id] = object;

        }
    };

    GCAD.SelectedObjectsStore = SelectedObjectsStore;
})();