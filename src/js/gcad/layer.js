(function() {
    'use strict';

    function Layer(name) {
        this.id = null;
        this.element = null;
        this.name = name;
        this.visible = true;
        this.type = 'layer';
        this.container = null;

        this.opacity = null;
        this.color = null;
        this.strokeDasharrayOfLines = null;
        this.widthOfLines = null;

        this.objects = {
            polylines: {},
            rectangles: {},
            circles: {},
            rulers: {},
            splines: {}
        };
    }

    Layer.prototype.hide = function() {
        this.element.style('display', 'none');
    };

    Layer.prototype.show = function() {
        this.element.style('display', "block");
    };

    GCAD.Layer = Layer;
})();
