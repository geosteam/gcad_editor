(function() {
    'use strict';

    GCAD.Core = function() {
        this.element = null;
        this.generalLayer = null;
        this.defaultLayer = null;

        this.zoom = d3.behavior.zoom().scaleExtent([-1, 10]).on("zoom", zoomed);
        this.disableZoom = d3.behavior.zoom().on("zoom", null);

        var self = this;

        function zoomed() {
            //TODO: Если использовать обычную фун-ю, то почему то ничего не работает...
            self.generalLayer.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
        }

        this.getTranslateValueX = function() {
            return d3.transform(self.generalLayer.attr("transform")).translate[0];
        };

        this.getTranslateValueY = function() {
            return d3.transform(self.generalLayer.attr("transform")).translate[1];
        };

        this.getScaleValue = function() {
            return d3.transform(self.generalLayer.attr("transform")).scale[0];
        };
    };

    /**
     * Добавляет окно редактора (тег svg) в выбранный селектор
     * @param selector
     */
    GCAD.Core.prototype.init = function(selector) {
        this.element = d3.select(selector).append('svg');
        this.element.attr({
            'owner': 'gcad'
        });
    };
})();
