(function() {
    'use strict';

    function Store() {
        this.layers = {};
        this.defaultLayer = null;
        this.polylines = {};
        this.path = {};
        this.lines = {};
        this.points = {};
        this.text = {};
        this.rulers = {};
        this.rectangles = {};
        this.circles = {};
        this.splines = {};
    }

    Store.prototype.add = function(obj) {
        var layer = this.layers[obj.layerID];

        if (obj.type === 'layer') {
            this.layers[obj.id] = obj;
        }
        if (obj.type === 'polyline') {
            layer.objects.polylines[obj.id] = obj;
            this.polylines[obj.id] = obj;
        }
        if (obj.type === 'spline') {
            layer.objects.splines[obj.id] = obj;
            this.splines[obj.id] = obj;
        } 
        if (obj.type === 'path') {
            this.path[obj.id] = obj;
        }
        if (obj.type === 'line') {
            this.lines[obj.id] = obj;
        }
        if (obj.type === 'point') {
            this.points[obj.id] = obj;
        }
        if (obj.type === 'text') {
            this.text[obj.id] = obj;
        }
        if (obj.type === 'ruler') {
            layer.objects.rulers[obj.id] = obj;
            this.rulers[obj.id] = obj;
        }
        if (obj.type === 'rect') {
            layer.objects.rectangles[obj.id] = obj;
            this.rectangles[obj.id] = obj;
        }
        if (obj.type === 'circle') {
            layer.objects.circles[obj.id] = obj;
            this.circles[obj.id] = obj;
        }
    };

    Store.prototype.delete = function(obj) {
        if (obj.type === 'layer') {
            delete this.layers[obj.id];
        }
    };

    GCAD.Store = Store;

})();
