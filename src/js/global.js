var GCAD = GCAD || {};
var EDITOR = EDITOR || {};

EDITOR.defaultLayer = null;
EDITOR.gcad = null;
EDITOR.objectSnap = false;

/**
 * Глобальные стили (стили по умолчанию)
 */
GCAD.Style = {
    line: {
        color: '#fff',
        selectedColor: 'red',
        strokeWidth: 1,
        strokeDasharray: 0
    },
    circle: {
        color: '#fff',
        selectedColor: '#DFCF00',
        strokeWidth: 3,
        fill: 'none',
        strokeDasharray: 0,
        mouseoverColor: '#27A5F9'
    },
    point: {
        color: '#71a55d',
        selectedColor: 'red',
        mouseoverColor: '#27A5F9',
        radius: 4
    },
    rect: {
        color: '#fff',
        selectedColor: '#DFCF00',
        mouseoverColor: '#27A5F9',
        strokeWidth: 2,
        strokeDasharray: 0,
        fill: 'none'
    },
    polyline: {
        edge: {
            mouseoverColor: '#27A5F9',
            selectedColor: 'red',
            strokeWidth: 3,
            strokeDasharray: 0,
            color: '',
            opacity: 1
        },
        color: '#fff',
        selectedColor: '#DFCF00'
    },
    spline: {
        mouseoverColor: '#27A5F9',
        strokeWidth: 3,
        strokeDasharray: 0,
        color: '#fff',
        selectedColor: '#DFCF00',
        opacity: 1,
        fill: 'none'
    },
    path: {
        color: '#EA9800',
        strokeWidth: 1,
        strokeDasharray: 0
    },
    ruler: {
        color: '#fff',
        labelColor: 'red',
        labelFontSize: 10,
        strokeWidth: 2,
        selectedColor: '#DFCF00',
        mouseoverColor: '#27A5F9',
        pointColor: '#71a55d',
        pointRange: 3
    },
    text: {
        color: '#fff',
        size: 10
    }
};

GCAD.typesOfLine = {
    line: '0',
    dottedLine: '6'
};

EDITOR.handlersStore = {
    polyline: {},
    ruler: {},
    text: {},
    rect: {},
    circle: {},
    spline: {}
};

EDITOR.helpers = {};
